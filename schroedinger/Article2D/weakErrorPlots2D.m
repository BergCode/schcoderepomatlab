%% Run initial info and query definitions in weakErrorSchemeEstimate2D.m
%% Load the data if necessary
load('weakErrorEstimate2D.mat')
load('weakErrorReference2D.mat')
%% Multilevel Monte Carlo estimation (may need to trim to same number of samples)
% numMeans = 10^3;
% confLevel = 0.95;
% perc = abs(norminv((1-confLevel)/2,0,1));
% 
% % Storage of mean approximation
% batchSize = size(estimateBatch,1);
% numMeansInMean = batchSize/numMeans;
% indexFun = @(i) (i-1)*numMeansInMean+(1:numMeansInMean);
% weakMeanMat = zeros(numMeans,numN,numUsedschemes,numWeaks);
% 
% % Basic level
% for i = 1:numMeans
%     weakMeanMat(i,1,:,:) = mean(estimateBatch(indexFun(i),1,:,:));
% end
% % Additional differences
% for n = 2:numN
%     for i = 1:numMeans
%         weakMeanMat(i,n,:,:) = weakMeanMat(i,n-1,:,:) + mean(...
%             estimateBatch(indexFun(i),n,:,:)...
%             -estimateBatch(indexFun(i),n-1,:,:)...
%             );
%     end
% end
% 
% % Reference mean estimate
% refBatchSize = size(refBatch,1);
% numRefMeansInMean = refBatchSize/numMeans;
% indexFunRef = @(i) (i-1)*numRefMeansInMean+(1:numRefMeansInMean);
% weakRefMat = zeros(numMeans,numWeaks);
% for i = 1:numMeans
%     weakRefMat(i,:) = squeeze(weakMeanMat(i,end,end,:))' + mean(...
%         squeeze(estimateBatch(indexFun(i),end,end,:))...
%         -refBatch(indexFunRef(i),:)...
%         );
% end

%% Simple Monte Carlo estimation
numMeans = 10^3;
confLevel = 0.95;
batchSize = size(estimateBatch,1);
perc = abs(norminv((1-confLevel)/2,0,1));

% Weak error mean estimate
numMeansInMean = batchSize/numMeans;
weakMeanMat = zeros(numMeans,numN,numUsedschemes,numWeaks);
for i = 1:numMeans
    weakMeanMat(i,:,:,:) = mean(estimateBatch((i-1)*numMeansInMean+(1:numMeansInMean),:,:,:));
end

% Reference mean estimate
numRefMeansInMean = size(refBatch,1)/numMeans;
weakRefMat = zeros(numMeans,numWeaks);
for i = 1:numMeans
    weakRefMat(i,:) = mean(refBatch((i-1)*numRefMeansInMean+(1:numRefMeansInMean),:));
end

%%
weakMeanEst = mean(weakMeanMat);
weakStdEst = std(weakMeanMat);

weakCIEst = zeros(2,numN,numUsedschemes,numWeaks);
weakCIEst(1,:,:,:) = weakMeanEst - perc*weakStdEst/sqrt(numMeans);
weakCIEst(2,:,:,:) = weakMeanEst + perc*weakStdEst/sqrt(numMeans);

perc = abs(norminv((1-confLevel)/2,0,1));
disp('Scheme estimate, min/max mean/CI width ratio')
min(weakMeanEst(:)./(perc*weakStdEst(:)/sqrt(numMeans)))
max(weakMeanEst(:)./(perc*weakStdEst(:)/sqrt(numMeans)))

meanEst = mean(weakRefMat);
stdEst = std(weakRefMat);


disp('Reference estimates, min/max mean/CI width ratio')
min(meanEst(:)./(perc*stdEst(:)/sqrt(numRefMeansInMean)))
max(meanEst(:)./(perc*stdEst(:)/sqrt(numRefMeansInMean)))

CIEst = zeros(2,numWeaks);
CIEst(1,:) = meanEst - perc*stdEst/sqrt(numRefMeansInMean);
CIEst(2,:) = meanEst + perc*stdEst/sqrt(numRefMeansInMean);


%% Plot convergence
supportLineSlope = {-1 
    -1
    -1 
    -1 
    -1
    -1
    -1
    -1
    -1
    -1
    -1
    -1
    -1
    -1
    -1};
supportLineTranslation = {10^-3
    10^-3
    10^-3
    10^-3
    10^-3
    10^-3
    10^-9
    10^-9
    10^-9
    10^-2.5
    10^-2.5
    10^-2
    10^-3
    10^-4
    10^-2};

for j = 1:numWeaks
    % Estimate degrees of freedom
    % (difference of two normally distributed random variables)
    degreesOfFreedom = 1./((stdEst(j)^4 + weakStdEst(:,:,:,j).^4)/(numMeans-1)./(stdEst(j)^2+weakStdEst(:,:,:,j).^2).^2);
    perc = squeeze(abs(tinv((1-confLevel)/2,degreesOfFreedom)));

    tempMean = squeeze(abs(weakMeanEst(:,:,:,j) - meanEst(j)));
    tempStd = squeeze(sqrt(weakStdEst(:,:,:,j).^2 + stdEst(j)^2));

    numSuppLines = length(supportLineSlope{j});
    legendInArg = cell(numSuppLines,1);
    logInArg = cell(3*numSuppLines,1);
    for i = 1:numSuppLines
        supportLine = (N.^supportLineSlope{j}(i))/(N(1)^supportLineSlope{j}(i))*supportLineTranslation{j}(i);
        logInArg(((i-1)*3+1):((i-1)*3+3)) = {N,supportLine,'--k'};
        legendInArg{i} = ['CN^{' num2str(supportLineSlope{j}(i)) '}'];
    end

    figure(j)
    clf
    hold on
    for i = 1:numUsedSchemes
        schemeNo = schemeIndexMat(i,2);
        % For presentation purposes
%         temp = errorbar(N,tempMean(:,i),perc(:,i).*tempStd(:,i)/sqrt(batchSize),...
%             refModel.schemes.lineInfo{schemeNo},'color',refModel.schemes.lineRGB{schemeNo},...
%             'LineWidth',4.5,'MarkerSize',25);
        
        % For article
        temp = errorbar(N,tempMean(:,i),perc(:,i).*tempStd(:,i)/sqrt(batchSize),...
            refModel.schemes.lineInfo{schemeNo},'color',refModel.schemes.lineRGB{schemeNo},...
            'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'));

        % If only one scheme, use this row:
%         temp = errorbar(N,tempMean,perc.*tempStd/sqrt(batchSize),...
%             refModel.schemes.lineInfo{schemeNo},'color',refModel.schemes.lineRGB{schemeNo},...
%             'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'));
        temp.CapSize = 15;
    end

    set(gca,'xscale','log');
    set(gca,'yscale','log');
    loglog(logInArg{:})
    legend(refModel.schemes.shortNames{schemesUsed},legendInArg{:},'Location','SouthWest')
    hold off

    xlabel('N')
    ylabel(weakErrorQueryNames{j},'Interpreter','latex')
    set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
%     if j > 9
        pause(2)
        printToPDF(gcf,weakErrorQueryFileNames{j})
%     end
end