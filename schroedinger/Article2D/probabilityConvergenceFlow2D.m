function probabilityConvergenceFlow2D(batchSize)
addpath('Functions')
%% Set initial info and function
initSeed = 1;
% batchSize = 200;
confLevel = 0.95;
% Time and area
L1 = 4*pi; 
L2 = 4*pi; 
XInt = [-L1,L1,-L2,L2];
T = 0.25; TInt = [0,T];

% Numerical precision, number of points
% refN = 2^16;
refN = 2^20;
refh = (TInt(2)-TInt(1))/refN;
% N = 2.^(10:14); % Time
N = 2.^(10:16); % Time
h = (TInt(2)-TInt(1))./N;
numN = length(N);
M = [2^6 2^6]; % Space
dx = (XInt(2)-XInt(1))/M(1);
dy = (XInt(4)-XInt(3))/M(2);

u0Fun = @(x,y) exp(-2*(x.^2+y.^2));
per = true;
simpsonAppr = false;
intLen = [XInt(2)-XInt(1),XInt(4)-XInt(3)];

sigma = 1;

k1 = 2*pi/(XInt(2)-XInt(1))*[0:M(1)/2-1, 0, -M(1)/2+1:-1];
k2 = 2*pi/(XInt(4)-XInt(3))*[0:M(2)/2-1, 0, -M(2)/2+1:-1];
[k1Grid,k2Grid] = meshgrid(k1,k2);
k = cat(3,k1Grid,k2Grid);
kSq = k1Grid.^2 + k2Grid.^2;

x = XInt(1) + dx*(0:M(1)-1);
y = XInt(3) + dy*(0:M(2)-1);

[x1Grid,x2Grid] = meshgrid(x,y);
u0FunVal = u0Fun(x1Grid,x2Grid);

refSchemes = makePSSchroedSchemes2D(kSq,refh,sigma);
numAvailableSchemes = length(refSchemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];
%% Query storage
storageFilename1 = 'probConvSamplesH1C2D.mat';
storageFilename2 = 'probConvSamplesL2C2D.mat';
% H1 & L2 storage
if isfile(storageFilename1) && isfile(storageFilename2)
    % Load information of the currently saved samples
    matObj = matfile(storageFilename1);
    details = whos(matObj);
    % If the size is smaller than the batch size, resize storage
    % This assumes that no new N, schemes or queries have been added
    numStoredElements = details.size(1);
    if(batchSize > numStoredElements)
        load(storageFilename1);
        
        temp = zeros(batchSize,numN,numUsedSchemes);
        temp(1:numStoredElements,:,:) = maxH1DiffBatch;
        maxH1DiffBatch = temp;
        
        load(storageFilename2);
        temp = zeros(batchSize,numN,numUsedSchemes);
        temp(1:numStoredElements,:,:) = maxL2DiffBatch;
        maxL2DiffBatch = temp;
    end
else
    maxH1DiffBatch = zeros(batchSize,numN,numUsedSchemes);
    maxL2DiffBatch = maxH1DiffBatch;
    numStoredElements = 0;
end
%% Perform calculations
if batchSize > numStoredElements
    parfor m = (numStoredElements+1):batchSize
        rng(m,'twister')
        % If we've already calculated this sample, skip it
        criteriaH1 = maxH1DiffBatch(m,:,:) == 0;
        criteriaL2 = maxL2DiffBatch(m,:,:) == 0;
        if any(criteriaH1(:)) || any(criteriaL2(:))
            % Load the broadcast variables to internal
            internalRefSchemes = refSchemes;
            internalSchemeIndexMat = schemeIndexMat;
            internalN = N;
            internalh = h;

            % Produce the reference Brownian motion
            refW = randn(refN,2)*sqrt(refh/2);
            % Initialize memory
            tempH1Batch = zeros(length(N),numUsedSchemes);
            tempL2Batch = zeros(length(N),numUsedSchemes);
            % Produce the solution storage and coarse Brownian motions
            refSol = fft2(u0FunVal);
            coarseSol = cell(numN,numUsedSchemes);
            schemes = cell(numN,1);
            coarseW = cell(numN,1);
            scalingFactor = zeros(numN,1);
            for n = 1:numN
                currN = internalN(n);
                currh = internalh(n);

                % Calculate the coarser Brownian motion increments
                scalingFactor(n) = refN/currN;
                coarseW{n} = zeros(currN,2);
                currIndex = 0;
                for i = 1:currN
                    indexList = (currIndex+1):(currIndex+scalingFactor(n)/2);
                    coarseW{n}(i,1) = sum(sum(refW(indexList,:)));
                    indexList = (currIndex+scalingFactor(n)/2+1):(currIndex+scalingFactor(n));
                    coarseW{n}(i,2) = sum(sum(refW(indexList,:)));
                    currIndex = currIndex + scalingFactor(n);
                end
                % Solution initial values
                for j = 1:numUsedSchemes
                    coarseSol{n,j} = refSol;
                end
                schemes{n} = makePSSchroedSchemes2D(kSq,currh,sigma);
            end

            % Simulate at reference precision, but step coarse solutions when the
            % correct time has been passed. Only calculate difference when coarsest
            % calculation is made
            dWIndex = ones(numN,1);
            for i = 1:refN
                refSol = internalRefSchemes.fun{7}(refSol,refW(i,:));
                for n = 1:numN
                    % Step coarse solution if enough steps passed
                    if mod(i,scalingFactor(n)) == 0
                        for j = 1:numUsedSchemes
                            currScheme = internalSchemeIndexMat(j,2);
                            coarseSol{n,j} = schemes{n}.fun{currScheme}(coarseSol{n,j},coarseW{n}(dWIndex(n),:));
                            
                            % Calculate maximum L2 and H1 difference
                            currDiff = refSol-coarseSol{n,j};
                            tempH1Batch(n,j) = max(sqrt(H1norm2D(currDiff,[dx dy],k,per,simpsonAppr,intLen)),tempH1Batch(n,j));
                            tempL2Batch(n,j) = max(sqrt(L2norm2D(currDiff,[dx dy],per,simpsonAppr,intLen)),tempL2Batch(n,j));
                        end
                        dWIndex(n) = dWIndex(n) + 1;
                    end
                end
                fprintf(['-------'...
                    '\n Sample: ', num2str(m), ...
                    '.\n Percentage until max time: ', num2str(i/refN), ...
                    '.\n-------\n'])
            end
            maxH1DiffBatch(m,:,:) = tempH1Batch;
            maxL2DiffBatch(m,:,:) = tempL2Batch;
        end
    end
    %%
    save(storageFilename1,'maxH1DiffBatch','-v7.3')
    save(storageFilename2,'maxL2DiffBatch','-v7.3')
end
% %% Process into probabilities
% % Use one contant vector per scheme
% cPotVec = [0 0.5 1 2.75 3.25 3.75];
% cVectorFlow = {10.^cPotVec
%     10.^cPotVec
%     10.^cPotVec};
% numC = length(cVectorFlow{1});
% deltaVectorFlow = [0.9 1 1.1];
% numDelta = length(deltaVectorFlow);
% 
% % Size of time steps
% timeStepMatrix = T./N;
% 
% % Storage
% probConvStorageH1 = zeros(numC,numDelta,numN,numUsedSchemes);
% probConvStorageL2 = zeros(numC,numDelta,numN,numUsedSchemes);
% 
% for i = 1:numC
%     for j = 1:numDelta
%         deltaFlow = deltaVectorFlow(j);
%         for n = 1:numN
%             for scheme = 1:numUsedSchemes
%                 % Use different for Euler type schemes
%                 probConvStorageH1(i,j,n,scheme) = ...
%                     mean(squeeze(maxH1DiffBatch(:,n,scheme)) > ...
%                     cVectorFlow{scheme}(i)*((T/N(n)).^deltaFlow) );
%                 probConvStorageL2(i,j,n,scheme) = ...
%                     mean(squeeze(maxL2DiffBatch(:,n,scheme)) > ...
%                     cVectorFlow{scheme}(i)*((T/N(n)).^deltaFlow) );
%             end
%         end
%     end
% end
% 
% %% Initialize title information
% rightTitleFlow = cell(numC,numUsedSchemes);
% topTitleFlow = cell(numDelta,1);
% 
% for j = 1:numC
%     for scheme = 1:numUsedSchemes
%         rightTitleFlow{j,scheme} = ['$C = $ $10^{' num2str(log10(cVectorFlow{scheme}(j))) '}$'];
%     end
% end
% for j = 1:numDelta
%     topTitleFlow{j} = ['$\delta = ' num2str(deltaVectorFlow(j)) '$'];
% end
% 
% 
% %% Mean square convergence confirmation
% % figure
% % loglog(N,squeeze(mean(maxH1DiffBatch)),N,N.^-(1/2),'--',N,N.^-1,'--')
% % figure
% % loglog(N,squeeze(mean(maxL2DiffBatch)),N,N.^-(1/2),'--',N,N.^-1,'--')
% %% One plot per family of similar schemes (special case)
% close all
% % EExp (scheme 5) and LTSpl (scheme 7)
% schNo = [5 7];
% vertProbPlotMult(probConvStorageH1(1:3,:,:,2:3),N,topTitleFlow,rightTitleFlow(1:3,1),...
%     refSchemes.lineInfo(schNo),refSchemes.lineRGB(schNo))
% pause(1)
% printToPDF(gcf,'probConvH1Flow2D')
% % CN (scheme 4)
% schNo = 4;
% vertProbPlotMult(probConvStorageH1(4:6,:,:,1),N,topTitleFlow,rightTitleFlow(4:6,1),...
%     refSchemes.lineInfo(schNo),refSchemes.lineRGB(schNo))
% pause(1)
% printToPDF(gcf,'probConvH1Eul2D')
% % All three together
% schNo = [4 5 7];
% vertProbPlotMult(probConvStorageH1,N,topTitleFlow,rightTitleFlow,...
%     refSchemes.lineInfo(schNo),refSchemes.lineRGB(schNo))
% pause(1)
% printToPDF(gcf,'probConvH1ThreeSchemes2D')
% 
% %% Testing boxplot presentation
% close all
% currScheme = 3;
% xString = num2str(log2(N'));
% xString = [repmat('2^',numN,1),xString];
% 
% maxEnd = max(maxH1DiffBatch(:,end,currScheme));
% 
% pow1 = 1.1;
% probVec1 = T./N.^pow1;
% C1 = 0.999*maxEnd/probVec1(end);
% leg1 = [num2str(round(C1,2)) '\Delta t^{' num2str(pow1) '}'];
% 
% 
% pow2 = 0.9;
% probVec2 = T./N.^pow2;
% C2 = 0.999*maxEnd/probVec2(end);
% leg2 = [num2str(round(C2,2)) '\Delta t^{' num2str(pow2) '}'];
% 
% pow3 = 1;
% probVec3 = T./N.^pow3;
% C3 = 1.7;
% leg3 = [num2str(C3) '\Delta t^{' num2str(pow3) '}'];
% 
% figure
% boxplot(maxH1DiffBatch(:,:,currScheme),xString)
% % Find handle for median line and set visibility off
% set(findobj(gca,'Tag','Median'),'Visible','off');
% hold on
% % Plot observed mean values
% plot(1:numN,mean(maxH1DiffBatch(:,:,currScheme)), 'k-*')
% % Plot probability lines
% plot(1:numN,probVec1*C1,'r-^')
% plot(1:numN,probVec2*C2,'g-v')
% plot(1:numN,probVec3*C3,'b-o')
% hold off
% set(gca,'yscale','log')
% legend('Mean error',leg1,leg2,leg3,'Interpreter','latex')
% 
% 
% %% Estimate C(epsilon)
% close all
% currScheme = 3;
% batchSize = size(maxH1DiffBatch,1);
% epsilonVec = linspace(0,1,batchSize);
% 
% powVec = 0.9:0.1:1.1;
% numPow = length(powVec);
% CVec = cell(numPow,numN);
% 
% for i = 1:numPow
%     figure
%     hold on
%     for n = 1:numN
%         % Estimate C(epsilon)
%         sortedDiff = sort(maxH1DiffBatch(:,n,currScheme));
%         dtPowVec = (T/N(n)).^powVec;
%         CVec{i,n} = zeros(batchSize,1);
%         for j = 1:batchSize
%             CVec{i,n}(j) =  sortedDiff(j)/dtPowVec(i);
%         end
%         % Plot
%         plot(epsilonVec,CVec{i,n},'LineWidth',2.5)
%     end
%     hold off
%     set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
%     xlabel('$\varepsilon$','Interpreter','latex')
%     ylabel('$C(\varepsilon)$','Interpreter','latex')
%     title(['$\delta = ' num2str(powVec(i)) '$'],'Interpreter','latex')
%     set(gcf,'units','normalized','outerposition',[0 0 1 1])
%     pause(1)
%     printToPDF(gcf,['ProbConv2DCEpsilon' num2str(i)])
% end
end