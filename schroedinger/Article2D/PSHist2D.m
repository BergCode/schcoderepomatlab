addpath('Functions')
%% Set initial info and function
initSeed = 1;
batchSize = 1000;
% Time and area
L1 = 4*pi; 
L2 = 4*pi; 
XInt = [-L1,L1,-L2,L2];
T = 0.25; TInt = [0,T];

% Numerical precision, number of points
N = 2^10; % Time
h = (TInt(2)-TInt(1))/N;
M = [2^8 2^8]; % Space
dx = (XInt(2)-XInt(1))/M(1);
dy = (XInt(4)-XInt(3))/M(2);

u0Fun = @(x,y) exp(-2*(x.^2+y.^2));
per = true;
% simpsonAppr determines L2 norm approximation method
simpsonAppr = false;
intLen = [XInt(2)-XInt(1), XInt(4)-XInt(3)];

sigma = 1;

k1 = 2*pi/(XInt(2)-XInt(1))*[0:M(1)/2-1, 0, -M(1)/2+1:-1];
k2 = 2*pi/(XInt(4)-XInt(3))*[0:M(2)/2-1, 0, -M(2)/2+1:-1];
[k1Grid,k2Grid] = meshgrid(k1,k2);
kSq = k1Grid.^2 + k2Grid.^2;

x = XInt(1) + dx*(0:M(1)-1);
y = XInt(3) + dy*(0:M(2)-1);

[x1Grid,x2Grid] = meshgrid(x,y);
u0FunVal = fft2(u0Fun(x1Grid,x2Grid));
u0L2Val = L2norm2D(u0FunVal,[dx dy],per,simpsonAppr,intLen);

schemes = makePSSchroedSchemes2D(kSq,h,sigma);
numAvailableSchemes = length(schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Query storage
maxL2DriftBatch = zeros(batchSize,numUsedSchemes);

%% Perform calculations
parfor m = 1:batchSize
    rng(m,'twister')
    % Load the broadcast variables to internal
    schemes;
    schemeIndexMat;
    
    W = randn(N,2)*sqrt(h/2);
    
    maxL2Drift = zeros(1,numUsedSchemes);
    for j = 1:numUsedSchemes
        currU = u0FunVal;
        currScheme = schemeIndexMat(j,2);
        for i = 1:N
            dW = W(i,:);
            %% Scheme and query calculations
            currU = schemes.fun{currScheme}(currU,dW);
            
            currL2Drift = abs(L2norm2D(currU,[dx dy],per,simpsonAppr,intLen) - u0L2Val);
            if currL2Drift > maxL2Drift(j)
                maxL2Drift(j) = currL2Drift;
            end
            [m j i/N]
        end
    end
    maxL2DriftBatch(m,:) = maxL2Drift;
end
save('PSHistL2Drift2D','maxL2DriftBatch2D')
%% Vertical histogram plot
% Set y axis info and plot the information in form of histrograms
histWidh = 0.774;
schemePlotSpec = [0.1305,histWidh/numUsedSchemes,0.11,0.815];

logDiff = log10(maxL2DriftBatch);

yMax = ceil(max(logDiff(:)));
yMin = floor(min(logDiff(:)));

if yMin < -17
    yMin = -17;
end
if yMax > 2
    yMax = 2;
end

% Set the y ticks and labels
yAxisVector = yMin:0.2:yMax;

% sideTitle = '$max_{n\in\{1,2,\ldots,N\}}log(| ~ ||u_n||_{L^2}^2-||u_0||_{L^2}^2 ~ |)$';
sideTitle = '$log_{10}$ of max. $L^2$-norm drift';
vertHistPlot(logDiff,schemes.shortNames(schemeIndexMat(:,2)),yAxisVector,sideTitle,schemePlotSpec,'Full')
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
% printToText('L2HistDriftBatch2D.txt',maxL2DriftBatch)
printToPDF(gcf,'PSHist2D')