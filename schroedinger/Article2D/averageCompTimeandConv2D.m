addpath('Functions')
%% Set initial info and function
initSeed = 1;
batchSize = 200;
confLevel = 0.95;
% Time and area
L1 = 4*pi; 
L2 = 4*pi; 
XInt = [-L1,L1,-L2,L2];
T = 0.25; TInt = [0,T];

% Numerical precision, number of points
refN = 2^21;
refh = (TInt(2)-TInt(1))/refN;
N = 2.^(10:18); % Time
h = (TInt(2)-TInt(1))./N;
numN = length(N);
M = [2^6 2^6]; % Space
dx = (XInt(2)-XInt(1))/M(1);
dy = (XInt(4)-XInt(3))/M(2);

u0Fun = @(x,y) exp(-2*(x.^2+y.^2));
per = true;
simpsonAppr = false;
intLen = [XInt(2)-XInt(1),XInt(4)-XInt(3)];

sigma = 1;

k1 = 2*pi/(XInt(2)-XInt(1))*[0:M(1)/2-1, 0, -M(1)/2+1:-1];
k2 = 2*pi/(XInt(4)-XInt(3))*[0:M(2)/2-1, 0, -M(2)/2+1:-1];
[k1Grid,k2Grid] = meshgrid(k1,k2);
k = cat(3,k1Grid,k2Grid);
kSq = k1Grid.^2 + k2Grid.^2;

x = XInt(1) + dx*(0:M(1)-1);
y = XInt(3) + dy*(0:M(2)-1);

[x1Grid,x2Grid] = meshgrid(x,y);
u0FunVal = u0Fun(x1Grid,x2Grid);

refSchemes = makePSSchroedSchemes2D(kSq,refh,sigma);
numAvailableSchemes = length(refSchemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];
%% Query storage
H1DiffBatch = zeros(batchSize,length(N),numUsedSchemes);
timeBatch = zeros(batchSize,length(N),numUsedSchemes);

%% Perform calculations
parfor m = 1:batchSize
    rng(m,'twister')
    % Load the broadcast variables to internal
    internalRefSchemes = refSchemes;
    internalSchemeIndexMat = schemeIndexMat;
    internalN = N;
    internalh = h;
    
    % Calculate reference solution
    refW = randn(refN,2)*sqrt(refh/2);
    currU = fft2(u0FunVal);
    for i = 1:refN
        dW = refW(i,:);
        % Using Lie-Trotter splitting scheme as reference solution
        currU = internalRefSchemes.fun{7}(currU,dW);
    end
    refSol = currU;
    
    tempH1Batch = zeros(length(N),numUsedSchemes);
    tempTimeBatch = zeros(length(N),numUsedSchemes);
    for n = 1:length(N)
        % Retrieve information regarding current number of steps
        currN = internalN(n);
        currh = internalh(n);
        scalingFactor = refN/currN;
        schemes = makePSSchroedSchemes2D(kSq,currh,sigma);
        
        % Calculate the coarser Brownian motion increments
        coarseDW = zeros(2,currN);
        currIndex = 0;
        for i = 1:currN
            indexList = (currIndex+1):(currIndex+scalingFactor/2);
            coarseDW(1,i) = sum(sum(refW(indexList,:)));
            indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
            coarseDW(2,i) = sum(sum(refW(indexList,:)));
            currIndex = currIndex + scalingFactor;
        end
        
        H1Difference = zeros(1,numUsedSchemes);
        compTime = zeros(1,numUsedSchemes);
        for j = 1:numUsedSchemes
            currU = fft2(u0FunVal);
            currScheme = internalSchemeIndexMat(j,2);
            dW = zeros(2,1);
            
			startTime = tic;
            for i = 1:currN
                dW = coarseDW(:,i);
                %% Scheme and query calculations
                currU = schemes.fun{currScheme}(currU,dW);
            end
			compTime(j) = toc(startTime);
            H1Difference(j) = H1norm2D(refSol-currU,[dx dy],k,per,simpsonAppr,intLen);
            
            fprintf(['-------'...
                '\n Sample: ', num2str(m), ...
                '\n N: ', num2str(currN), ...
                '\n N index: ', num2str(n), ...
                '\n Scheme: ', num2str(j), ...
                '.\n-------\n']);
        end
        tempH1Batch(n,:) = H1Difference;
		tempTimeBatch(n,:) = compTime;
    end
    H1DiffBatch(m,:,:) = tempH1Batch;
    timeBatch(m,:,:) = tempTimeBatch;
end
save('meanSqConvH1Diff2D','H1DiffBatch')
save('meanSqConvTime2D','timeBatch')

%% Plot convergence
% Statistics
meanH1Vals = squeeze(mean(H1DiffBatch));
meanTimeVals = squeeze(mean(timeBatch));
stdH1Vals = squeeze(std(H1DiffBatch));
stdTimeVals = squeeze(std(timeBatch));
% Used for CI based on normal distribution
perc = abs(norminv((1-confLevel)/2,0,1));

% Add support lines
% numSupportLines = 2;
% translConst = [1/10^3 1/10^5];
%     
% logInArg = cell(0);
% legendInArg = cell(numSupportLines,1);
% for i = 1:numSupportLines
%     logInArg(end+1:end+3) = {N,translConst(i)*N.^-i,'--k'};
%     legendInArg{i} = sprintf('C_%dN^{-%d}',i,i);
% end


logInArg = cell(0);
legendInArg = cell(1,1);
supportLine = (N.^-2)/(N(1)^-2)*meanH1Vals(1,end)*10;
logInArg(end+1:end+3) = {N,supportLine,'--k'};
legendInArg{1} = sprintf('C_%dN^{-%d}',2,2);


% supportLine = (N.^-1)/(N(1)^-1)*meanL2Vals(1,end)*10000;
% logInArg(end+1:end+3) = {N,supportLine,'--k'};
% legendInArg{2} = sprintf('C_%dN^{-%d}',1,1);

figure(1)
clf
hold on
for i = 1:numUsedSchemes
    schemeNo = schemeIndexMat(i,2);
    errorbar(N,meanH1Vals(:,i),perc*stdH1Vals(:,i)/sqrt(batchSize),...
        refSchemes.lineInfo{schemeNo},'color',refSchemes.lineRGB{schemeNo},...
        'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'));
end

set(gca,'xscale','log');
set(gca,'yscale','log');
loglog(logInArg{:})
legend(refSchemes.shortNames{schemesUsed},legendInArg{:},'Location','SouthWest')
hold off

xlabel('N')
ylabel('$E[||u_N - u^{ref}_{N^{ref}}||_{H^1}^2]$','Interpreter','latex')
set(gca,'FontSize',35)
set(gcf,'units','normalized','outerposition',[0 0 1 1])

pause(1)
printToPDF(gcf,'convTime2d')

%% Computational time plot

figure(2)
clf
hold on
for i = 1:numUsedSchemes
    schemeNo = schemeIndexMat(i,2);
    plot(meanTimeVals(:,i),meanH1Vals(:,i),...
        refSchemes.lineInfo{schemeNo},'color',refSchemes.lineRGB{schemeNo},...
        'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'))
end

set(gca,'xscale','log');
set(gca,'yscale','log');
legend(refSchemes.shortNames(schemesUsed),'Location','SouthWest')
hold off

xlabel('Aver. comp. time (sec)','Interpreter','latex')
ylabel('$E[||u_N - u^{ref}_{N^{ref}}||_{H^1}^2]$','Interpreter','latex')
set(gca,'FontSize',35)
set(gcf,'units','normalized','outerposition',[0 0 1 1])

pause(1)
printToPDF(gcf,'timePrecCalc2D')

%% Combine these plots into one
% Retrieve figure info
figure(1)
fig1LineInfo = get(gca,'children');
figure(2)
fig2LineInfo = get(gca,'children');

ylimArg = [10^(-5)*min(meanH1Vals(:)) 10*max(meanH1Vals(:))];

% Create new figure and subplots
figure(3)
clf
set(gcf,'units','normalized','outerposition',[0 0 1 1])

sp1 = subplot(1,2,1);
% Insert info
copyobj(fig1LineInfo,sp1);
ylim(ylimArg)
set(gca,'xscale','log');
set(gca,'yscale','log');
set(gca,'FontSize',fontAndMarkerSize('Font','Full'))
legend(refSchemes.shortNames{schemesUsed},legendInArg{:},'Location','SouthWest')
xlabel('$N$','Interpreter','latex')
ylabel('$E[||u_N - u^{ref}_{N^{ref}}||_{H^1}^2]$','Interpreter','latex')
    
sp2 = subplot(1,2,2);
% Insert info
copyobj(fig2LineInfo,sp2);
ylim(ylimArg)
set(gca,'xscale','log');
set(gca,'yscale','log');
set(gca,'FontSize',fontAndMarkerSize('Font','Full'))
legend(refSchemes.shortNames(schemesUsed),'Location','SouthWest')
xlabel('Aver. comp. times (in sec)','Interpreter','latex')
ylabel('$E[||u_N - u^{ref}_{N^{ref}}||_{H^1}^2]$','Interpreter','latex')
    
    
figure(3)
pause(1)
printToPDF(gcf,'timeConvAndPrec2D')