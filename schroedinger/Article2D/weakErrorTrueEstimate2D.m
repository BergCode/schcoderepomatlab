function weakErrorTrueEstimate2D(batchSize)
addpath('Functions')
%% Set initial info and function
initSeed = 1;
% batchSize = 10^4;
% Time and area
L1 = 4*pi; 
L2 = 4*pi; 
XInt = [-L1,L1,-L2,L2];
T = 1/4; TInt = [0,T];

% Numerical precision, number of points
% refN = 2^12; % Time
% refM = 2^8; % Space
% Old settings, see weakError.m
refN = 2^13; % Time
% refM = 2^7; % Space
refM = [2^4 2^4]; % Space

u0Fun = @(x,y) exp(-2*(x.^2+y.^2));
per = true;

% noiseConstant = 20;
noiseConstant = 1/4;
sigma = 1;

refModel = initModelInfo2D(refN,TInt,refM,XInt,sigma,per);
[x1Grid,x2Grid] = meshgrid(refModel.x,refModel.y);
[k1Grid,k2Grid] = meshgrid(refModel.k1,refModel.k2);
k = cat(3,k1Grid,k2Grid);
dx = [refModel.dx refModel.dy];

u0FunVal = fft2(u0Fun(x1Grid,x2Grid));

%% Query definitions
% The different weak errors we will compare, currU and refU will be in Fourier space
weakErrorQuery = cell(0,0);
weakErrorQueryNames = cell(0,0);
weakErrorQueryFileNames = cell(0,0);

% L2-norm of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm2D(fft2(real(ifft2(currU))),dx,per,false,[2*L1, 2*L2]);
weakErrorQueryNames{end+1} = '$e_1(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DRealPartL2';

% L2-norm of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) L2norm2D(fft2(exp(-(ifft2(currU)).^2)),dx,per,false,[2*L1, 2*L2]);
weakErrorQueryNames{end+1} = '$e_2(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DGaussL2';

% L2-norm of sin of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm2D(fft2(sin(real(ifft2(currU)))),dx,per,false,[2*L1, 2*L2]);
weakErrorQueryNames{end+1} = '$e_3(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DSinRealPartL2';

% Energy
weakErrorQuery{end+1} = @(currU,dx,k) energySchroed2D(currU,dx,k,sigma,[2*L1, 2*L2]);
weakErrorQueryNames{end+1} = '$e_4(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DEnergy';

% H1 norm
weakErrorQuery{end+1} = @(currU,dx,k) H1norm2D(currU,dx,k,per,false,[2*L1, 2*L2]);
weakErrorQueryNames{end+1} = '$e_5(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DH1';

% Odd stuff
weakErrorQuery{end+1} = @(currU,dx,k) abs(cos(abs(currU(5))));
weakErrorQueryNames{end+1} = '$e_6(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DOdd';

% Max of cosine filter
weakErrorQuery{end+1} = @(currU,dx,k) max(max(cos(abs(ifft2(currU)))));
weakErrorQueryNames{end+1} = '$e_7(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DCosMax';

% Max of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) max(max(abs(exp(-absSq(ifft2(currU))))));
weakErrorQueryNames{end+1} = '$e_8(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DGaussMax';

% Max of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) max(max(1./(1+(absSq(ifft2(currU))))));
weakErrorQueryNames{end+1} = '$e_9(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DDivSqMax';

% Real space
% Min of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) min(min(1./(1+(absSq(ifft2(currU))))));
weakErrorQueryNames{end+1} = '$e_{10}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DDivSqMin';

% Gaussian filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) exp(-max(max(absSq(ifft2(currU)))));
weakErrorQueryNames{end+1} = '$e_{11}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DGaussOfMax';

% Square filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) max(max(absSq(ifft2(currU))));
weakErrorQueryNames{end+1} = '$e_{12}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DSqOfMax';

% Fourier space
% Min of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) min(min(1./(1+(absSq(currU)))));
weakErrorQueryNames{end+1} = '$e_{13}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DDivSqMinFour';

% Gaussian filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) exp(-max(max(absSq(currU))));
weakErrorQueryNames{end+1} = '$e_{14}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DGaussOfMaxFour';

% Square filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) max(max(absSq(currU)));
weakErrorQueryNames{end+1} = '$e_{15}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErr2DSqOfMaxFour';

numWeaks = length(weakErrorQuery);
%% Query storage
storageFilename = 'weakErrorReference2D.mat';
% If query storage exists, load it. Otherwise, create it
if isfile(storageFilename)
    % Load information of the currently saved samples
    matObj = matfile(storageFilename);
    details = whos(matObj);
    % If the size is smaller than the batch size, resize storage
    % This assumes that no new N, schemes or queries have been added
    numStoredElements = details.size(1);
    if(batchSize > numStoredElements)
        load(storageFilename);
        temp = zeros(batchSize,numWeaks);
        temp(1:numStoredElements,:) = refBatch;
        refBatch = temp;
    end
else
    refBatch = zeros(batchSize,numWeaks);
    numStoredElements = 0;
end

%% Perform calculations, only if batchSize is larger than the currently stored values

if batchSize > numStoredElements
    parfor m = (numStoredElements+1):batchSize
        rng(m,'twister')
        % Load the broadcast variables to internal
        internalWeakErrorQuery = weakErrorQuery;
        internalRefModel = refModel;

        % Calculate reference solution
        refW = noiseConstant*randn(refN,2)*sqrt(internalRefModel.h/2);
        currU = u0FunVal;
        for i = 1:refN
            dW = refW(i,:);
            % Using Lie-Trotter splitting scheme as reference solution
            currU = internalRefModel.schemes.fun{7}(currU,dW);
        end
        tempRefBatch = zeros(numWeaks,1);
        for i = 1:numWeaks
             tempRefBatch(i) = internalWeakErrorQuery{i}(currU,dx,k);
        end
        refBatch(m,:) = tempRefBatch;
        m
    end

    % Save the data
    save(storageFilename,'refBatch','-v7.3')
end