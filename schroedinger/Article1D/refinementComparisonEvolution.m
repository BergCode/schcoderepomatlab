% OBS! Script only uses Lie-Trotter splitting scheme
%% Set initial info and function
addpath('Data')
initSeed = 4;
batchSize = 40;
% Time and area
L = 5*pi; XInt = [-L,L];
T = 10^(-2); TInt = [0,T];
% T = 1; TInt = [0,T];
% simpsonAppr determines H1 norm approximation method
simpsonAppr = false;
intervalLen = XInt(2)-XInt(1);

% Numerical precision, number of points
% N = 2^9; % Time
N = 2^10; % Time
% M = 2^9; % Space
M = 2^13; % Space
fineM = 2*M; % Space
per = true;
sigma = 1;

% Reference solution
coarseSpaceScalingFactor = 2^5;
fineSpaceScalingFactor = coarseSpaceScalingFactor/2;
refN = 2^5*N; % Space
refM = coarseSpaceScalingFactor*M; % Space

u0Fun = @(x) exp(-4*x.^2);

%% Define models and refinements 
% Used models, from coarsest to finest (only last with higher N)
models = cell(0,0);
models{end+1} = initModelInfo(N,TInt,M,XInt,sigma,per);
models{end+1} = initModelInfo(N,TInt,fineM,XInt,sigma,per);
models{end+1} = initModelInfo(refN,TInt,refM,XInt,sigma,per);
numModels = length(models);

% Which refimenents are used
refinements = {'Linear','Interpft','Pchip','Spline','nFme'};
numRef = length(refinements);

% One process which will not be refined, per model, and one process per
% refinement on the coarsest
numProc = numModels + numRef;
u0FunValues = cell(numModels + numRef,1);

%% Link each model to each process and calculate the initial value
procModelNo = ones(numProc,1);
% Second process uses the finer model
procModelNo(2) = 2;
% Last process uses the reference model
procModelNo(end) = 3;

% Note that refinement will automatically increase the procModelNo by 1

% Initial values and norming
for i = 1:numProc
    u0FunValues{i} = fft(u0Fun(models{procModelNo(i)}.x));
    u0FunValues{i} = u0FunValues{i} / sqrt(L2norm(u0FunValues{i},models{procModelNo(i)}.dx,per,simpsonAppr,intervalLen));
end
%% Queries and query storage
query = cell(0,0);
% Reduce the vector u in Fourier space to length M, and scale properly
reduceFreq = @(M,u)(M/length(u))*[u(1:M/2) u((end-M/2+1):end)];
% Increase the vector u in Fourier space to length M, and scale properly
increaseFreq = @(M,uLen,u)(M/uLen)*[u(1:uLen/2) zeros(1,M-uLen) u((uLen/2+1):end)];

% L2 norm
query{end+1} = @(u,refU,model) L2norm(u,model.dx,per,simpsonAppr,intervalLen);
% L2 norm of error, via reduction of frequencies
query{end+1} = @(u,refU,model) L2norm(u-reduceFreq(length(u),refU),model.dx,per,simpsonAppr,intervalLen);
% H1 norm of error, via reduction of frequencies
query{end+1} = @(u,refU,model) H1norm(u-reduceFreq(length(u),refU),model.dx,model.k,per,simpsonAppr,intervalLen);
% L2 norm of error, via increase of frequencies
query{end+1} = @(u,refU,model) L2norm(increaseFreq(length(refU),length(u),u)-refU,models{end}.dx,per,simpsonAppr,intervalLen);
% H1 norm of error, via increase of frequencies
query{end+1} = @(u,refU,model) H1norm(increaseFreq(length(refU),length(u),u)-refU,models{end}.dx,models{end}.k,per,simpsonAppr,intervalLen);

numQueries = length(query);

% Exclude the reference solution
queryStorage = zeros(batchSize,numQueries,numProc-1,N+1);

%% Perform calculations
parfor m = 1:batchSize
    rng(m,'twister')
    % Load the broadcast variables to internal
    models;
    u0FunValues;
    query;
    refinements;
    % Load the broadcast variables which are subject to change during calc.
    internalProcModelNo = procModelNo;
    
    % Simulate Brownian motion
    refW = randn(2,refN)*sqrt(models{end}.h/2);
    % Calculate the coarser Brownian motion increments
    scalingFactor = refN/N;
    coarseW = zeros(2,N);
    currIndex = 0;
    for i = 1:N
        indexList = (currIndex+1):(currIndex+scalingFactor/2);
        coarseW(1,i) = sum(sum(refW(:,indexList)));
        indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
        coarseW(2,i) = sum(sum(refW(:,indexList)));
        currIndex = currIndex + scalingFactor;
    end
    
    % Initial values
    currU = u0FunValues;

    % Internal query storage
    tempQueryStorage = zeros(numQueries,numProc-1,N+1);
    
    % Calculate first query values
    for j = 1:numProc-1
        for q = 1:numQueries
            tempQueryStorage(q,j,1) = query{q}(currU{j},currU{end},models{internalProcModelNo(j)});
        end
    end

    % Not yet refined
    refined = false;
    for i = 1:N
        %% Scheme and query calculations
        % Step the reference solution, which is the last process
        for j = 1:scalingFactor
            refdW = refW(:,scalingFactor*(i-1)+j);
            currU{end} = models{internalProcModelNo(end)}.schemes.fun{7}(currU{end},refdW);
        end
        
        coarsedW = coarseW(:,i);
        % Step processes and calculate queries
        if ~refined
            for j = 1:numProc-1-numRef
                currU{j} = models{internalProcModelNo(j)}.schemes.fun{7}(currU{j},coarsedW);
                for q = 1:numQueries
                    tempQueryStorage(q,j,i+1) = query{q}(currU{j},currU{end},models{internalProcModelNo(j)});
                end
            end
            % Copy first process
            for j = numProc-numRef:numProc-1
                currU{j} = currU{1};
                for q = 1:numQueries
                    tempQueryStorage(q,j,i+1) = tempQueryStorage(q,1,i+1);
                end
            end
        else
            for j = 1:numProc-1
                currU{j} = models{internalProcModelNo(j)}.schemes.fun{7}(currU{j},coarsedW);
                for q = 1:numQueries
                    tempQueryStorage(q,j,i+1) = query{q}(currU{j},currU{end},models{internalProcModelNo(j)});
                end
            end
        end
        
        % Check if time for refinement, refine one step finer
        if ~refined && i == N/2
            refIndex = 1;
            for j = numProc-numRef:numProc-1
                coModel = internalProcModelNo(j);
                fiModel = coModel + 1;
                internalProcModelNo(j) = fiModel;
                currU{j} = fft(refineU(ifft(currU{j}),per,refinements{refIndex},models{coModel}.x,models{fiModel}.x));
                refIndex = refIndex + 1;
            end
            refined = true;
        end
        
        fprintf(['Sample: ', num2str(m), ...
            '.\n Percentage until max time: ', num2str(i/N), ...
            '.\n-------\n'])
    end
    queryStorage(m,:,:,:) = tempQueryStorage;
end

save('refinementEvolutionQueries','queryStorage','-v7.3')

%%
tVector = linspace(0,T,N+1);
refineLine = {'-','-','-+','-x','-^','-v','-o'};
legendInfo = [{'Coarse','Fine'},refinements];

ylabels = cell(0,0);
ylabels{end+1} = '$| ~ ||\hat{u}||_{L^2}^2-||u(0)||_{L^2}^2 ~ |$';
ylabels{end+1} = 'E$\left[||\hat{u}-u^{ref}||_{L^2}^2\right]$';
ylabels{end+1} = 'E$\left[||\hat{u}-u^{ref}||_{H^1}^2\right]$';
ylabels{end+1} = 'E$\left[||\hat{u}-u^{ref}||_{L^2}^2\right]$';
ylabels{end+1} = 'E$\left[||\hat{u}-u^{ref}||_{H^1}^2\right]$';


%% Half page width plot - L2 drift
figure(1)
tempVal = queryStorage(1,1,:,:);
tempL2 = squeeze(abs(tempVal-queryStorage(1,1,:,1)))';
plot(tVector,tempL2,'LineWidth',2.5)

ylabel(ylabels{1},'Interpreter','latex')
xlabel('$t$','Interpreter','latex')
set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
legend(legendInfo,'Location','northwest');
set(gca,'yscale','log');
set(gca,'YTick', 10.^(-16:4:-4))
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationL2Drift')
%% Half page width plot - L2 norm of error
figure(2)
tempErr = squeeze(mean(queryStorage(:,2,:,:)))';
plot(tVector,tempErr,'LineWidth',2.5)

ylabel(ylabels{2},'Interpreter','latex')
xlabel('$t$','Interpreter','latex')
set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
legend(legendInfo,'Location','northwest');
ylim([0.9*min(tempErr(N/4:end,1)) 2*max(max(tempErr(N/4:end,:)))])
set(gca,'yscale','log');
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationL2OfErrorDrift')

%% Half page width plot - H1 norm of error
figure(3)
tempErr = squeeze(mean(queryStorage(:,3,:,:)))';
plot(tVector,tempErr,'LineWidth',2.5)

ylabel(ylabels{3},'Interpreter','latex')
xlabel('$t$','Interpreter','latex')
set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
legend(legendInfo,'Location','northwest');
ylim([0.1*min(tempErr(N/4:end,1)) 2*max(max(tempErr(N/4:end,:)))])
set(gca,'yscale','log');
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationH1OfErrorDrift')

%% Compare end precision
tempMean = squeeze(mean(queryStorage(:,:,:,end)));
% First query measures drift from normed initial L2 value
tempMean(1,:) = abs(tempMean(1,:) - queryStorage(1,1,end,1));

for q = 1:numQueries
    [querySort, queryOrder] = sort(tempMean(q,:));
    fprintf(['\nQuery ' num2str(q) ' at T in order of smallest to largest:\n'])
    fprintf(char(legendInfo(queryOrder))')
    fprintf('\n')
end

%% ANOVA analysis of L2 conservation
temp = squeeze(queryStorage(:,1,:,end));
fprintf('\n')
fprintf('ANOVA-analysis of all processes:\n')
[pAll,tbl,stats] = anova1(temp);
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

fprintf('\n')
fprintf('ANOVA-analysis of all processes, excluding Linear and Interpft:\n')
[pAll,tbl,stats] = anova1(temp(:,[1 2 5:7]));
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

fprintf('\n')
fprintf('ANOVA-analysis of all processes, excluding Linear, Interpft and Pchip:\n')
[pAll,tbl,stats] = anova1(temp(:,[1 2 6 7]));
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

fprintf('\n')
fprintf('ANOVA-analysis of Coarse, Fine, and nFme processes:\n')
[pAll,tbl,stats] = anova1(temp(:,[1 2 7]));
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

%% ANOVA analysis of H1 error
temp = squeeze(queryStorage(:,3,:,end));
fprintf('\n')
fprintf('ANOVA-analysis of all processes:\n')
[pAll,tbl,stats] = anova1(temp);
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

fprintf('\n')
fprintf('ANOVA-analysis of all processes, excluding Linear and Interpft:\n')
[pAll,tbl,stats] = anova1(temp(:,[1 2 5:7]));
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

fprintf('\n')
fprintf('ANOVA-analysis of all processes, excluding Linear, Interpft and Pchip:\n')
[pAll,tbl,stats] = anova1(temp(:,[1 2 6 7]));
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

fprintf('\n')
fprintf('ANOVA-analysis of Coarse, Fine, and nFme processes:\n')
[pAll,tbl,stats] = anova1(temp(:,[1 2 7]));
fprintf(['P-value equal mean: ' num2str(pAll)])
fprintf('\n')

%% Vertical histogram plot of H1 values
temp = log10(squeeze(queryStorage(:,3,:,end)));
histWidh = 0.774;
schemePlotSpec = [0.1305,histWidh/size(temp,2),0.11,0.815];
procNames = [{'Coarse'} {'Fine'} refinements];
yMax = ceil(max(temp(:)));
yMin = floor(min(temp(:)));
yAxis = linspace(yMin,yMax,10);

sideTitle = '$log_{10}$ of $H^1$-error at $T$';

vertHistPlot(temp,procNames,yAxis,sideTitle,schemePlotSpec,'Full')
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationH1')