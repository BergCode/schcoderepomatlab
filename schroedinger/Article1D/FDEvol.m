addpath('Functions')
%% Set initial info and function
initSeed = 1;
% Time and area
L = 30; XInt = [-L,L];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^10; % Time
M = 2^9; % Space
sigma = 1;

u0Fun = @(x) exp(-2*x.^2);
per = false;

model = initModelInfo(N,TInt,M,XInt,sigma,per);

u0FunVal = u0Fun(model.x);

numAvailableSchemes = length(model.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Query storage
% Query fidelity
timeFidelity = 2^5;
if N > timeFidelity
    scalingFactor = N / timeFidelity;
    timeVec = 0:scalingFactor:N;
else
    scalingFactor = 1;
    timeVec = 0:N;
end

normStorage = cell(numUsedSchemes,1);
for i = 1:numUsedSchemes
    normStorage{i} = zeros(length(timeVec),1);
    normStorage{i}(1) = L2norm(u0FunVal,model.dx,per);
end

%% Perform calculations
rng(initSeed,'twister')
W = randn(N,2)*sqrt(model.h/2);
for j = 1:numUsedSchemes
    currU = u0FunVal;
    currScheme = schemeIndexMat(j,2);
    queryIndex = 2;
    for i = 1:N
        dW = W(i,:);
        %% Scheme and query calculations
        currU = model.schemes.fun{currScheme}(currU,dW);
        
        if mod(i,scalingFactor) == 0
            normStorage{j}(queryIndex) = L2norm(currU,model.dx,per);
            queryIndex = queryIndex + 1;
        end
    end
end
save('FDEvolStorage','normStorage')

%% L2 norm evolution plot
% EExp scheme currently colored and marked with -pm
schemeLegendMarks = {'k','k','-pm','k','k','k'};
title = '$||u_n||_{L^2}^2$';

figure
plotQueryEvol(model,normStorage,TInt,schemeIndexMat,schemeLegendMarks,title)
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'FDEvol')