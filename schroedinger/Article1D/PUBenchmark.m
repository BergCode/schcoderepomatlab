batchSize = 100;

T = 1;
TInt = [0 T];
N = 2^3;

L = 10;
XInt = [-L,L];
numM = 11;
MVector = 2^14*2.^(1:numM);
% 2^22 vector size is the limit for parfor GPU memory
% 2^25 vector size working for single GPU memory

per = true;
sigma = 1;

u0Fun = @(x) exp(-3*x.^2);


%% Simulating dW without preallocation and no parfor
numBenchs = 4;
timeStorage = zeros(batchSize,numM,numBenchs);

for j = 1:numM
    M = MVector(j);
    model = initModelInfo(N,TInt,M,XInt,sigma,per);
    u0FunVal = fft(u0Fun(model.x));
    for m = 1:batchSize


        % CPU time test - No overhead
        currU = u0FunVal;
        kSq = model.k.^2;
        h = model.h;
        tic
        for i = 1:N
            dW = sqrt(h/2)*randn(2,1);
            % Full linear step
            tempRealSpace = ifft(exp(-sum(dW)*1i*kSq).*currU);
            % Full nonlinear step
            currU = fft(exp(h*1i*absSq(tempRealSpace).^sigma).*tempRealSpace);
        end
        timeStorage(m,j,1) = toc;


        % CPU time test - Model struct overhead
        currU = u0FunVal;
        h = model.h;
        tic
        for i = 1:N
            dW = sqrt(h)*randn(2,1);
            currU = model.schemes.fun{7}(currU,dW);
        end
        timeStorage(m,j,2) = toc;

        % GPU time test - No overhead
        currU = gpuArray(u0FunVal);
        kSq = gpuArray(model.k.^2);
        h = gpuArray(model.h);
        tic
        for i = 1:N
            dW = sqrt(h)*randn(2,1);
            % Full linear step
            tempRealSpace = ifft(exp(-sum(dW)*1i*kSq).*currU);
            % Full nonlinear step
            currU = fft(exp(h*1i*absSq(tempRealSpace).^sigma).*tempRealSpace);
        end
        timeStorage(m,j,3) = toc;
        
        
        % GPU time test - Model struct overhead
        currU = gpuArray(u0FunVal);
        h = gpuArray(model.h);
        tic
        for i = 1:N
            dW = sqrt(h)*randn(2,1);
            currU = model.schemes.fun{7}(currU,dW);
        end
        timeStorage(m,j,4) = toc;
        
        
        disp(['Done with sample ' num2str(m) ' and size ' num2str(j) ' of ' num2str(numM)])
    end
end


%% Simulating using parfor
parforTimeStorage = zeros(numBenchs,1);

% CPU time test - No overhead
tic
parfor m = 1:batchSize
    internalXInt = XInt;
    internalTInt = TInt;
    internalMVector = MVector;
    M = internalMVector(end-3);
    h = (internalTInt(2)-internalTInt(1))/N;
    
    dx = (internalXInt(2)-internalXInt(1))/M;
    x = internalXInt(1) + dx*(0:M-1);
    k = 2*pi/(internalXInt(2)-internalXInt(1))*[0:M/2-1, 0, -M/2+1:-1];
    kSq = k.^2;

    currU = fft(u0Fun(x));
    for i = 1:N
        dW = sqrt(h)*randn(2,1);
        % Full linear step
        tempRealSpace = ifft(exp(-sum(dW)*1i*kSq).*currU);
        % Full nonlinear step
        currU = fft(exp(h*1i*absSq(tempRealSpace).^sigma).*tempRealSpace);
        disp(num2str(i/N))
    end
    disp(['Done with sample ' num2str(m) ' of CPU no overhead.'])
end
parforTimeStorage(1) = toc;


% CPU time test - Model struct overhead
tic
parfor m = 1:batchSize
    internalMVector = MVector;
    M = internalMVector(end-3);
    model = initModelInfo(N,TInt,M,XInt,sigma,per);

    % CPU time test - Model struct overhead
    currU = fft(u0Fun(model.x));
    for i = 1:N
        dW = sqrt(model.h)*randn(2,1);
        currU = model.schemes.fun{7}(currU,dW);
        disp(num2str(i/N))
    end
    disp(['Done with sample ' num2str(m) ' of CPU with overhead.'])
end
parforTimeStorage(2) = toc;



% GPU time test - No overhead
tic
parfor m = 1:batchSize
    internalXInt = XInt;
    internalTInt = TInt;
    internalMVector = MVector;
    M = internalMVector(end-3);
    h = (internalTInt(2)-internalTInt(1))/N;

    dx = (internalXInt(2)-internalXInt(1))/M;
    x = internalXInt(1) + dx*(0:M-1);
    k = 2*pi/(internalXInt(2)-internalXInt(1))*[0:M/2-1, 0, -M/2+1:-1];

    % GPU time test - No overhead
    currU = gpuArray(fft(u0Fun(x)));
    kSq = gpuArray(k.^2);
    h = gpuArray(h);
    for i = 1:N
        dW = sqrt(h)*randn(2,1);
        % Full linear step
        tempRealSpace = ifft(exp(-sum(dW)*1i*kSq).*currU);
        % Full nonlinear step
        currU = fft(exp(h*1i*absSq(tempRealSpace).^sigma).*tempRealSpace);
        disp(num2str(i/N))
    end
    disp(['Done with sample ' num2str(m) ' of GPU no overhead.'])
end
parforTimeStorage(3) = toc;


% GPU time test - Model struct overhead
tic
parfor m = 1:batchSize
    internalMVector = MVector;
    M = internalMVector(end-3);
    model = initModelInfo(N,TInt,M,XInt,sigma,per);

    % GPU time test - Model struct overhead
    currU = gpuArray(fft(u0Fun(model.x)));
    h = gpuArray(model.h);
    for i = 1:N
        dW = sqrt(h)*randn(2,1);
        currU = model.schemes.fun{7}(currU,dW);
        disp(num2str(i/N))
    end
    disp(['Done with sample ' num2str(m) ' of GPU with overhead.'])
end
parforTimeStorage(4) = toc;





%% Plot the means
meanTimeVal = squeeze(mean(timeStorage));

confidenceLevel = 0.95;
stdTimeVal = squeeze(std(timeStorage));
CIWidth = stdTimeVal*abs(norminv(1-confidenceLevel));

% loglog(MVector,meanTimeVal)
close all
figure(1)
hold on
for i = 1:numBenchs
    switch i
        case 1
            line = '-^';
        case 2
            line = '-v';
        case 3
            line = '-s';
        case 4
            line = '-o';
    end
    errorbar(MVector,meanTimeVal(:,i),CIWidth(:,i),line,'MarkerSize',fontAndMarkerSize('Marker','Full'),'LineWidth',2.5)
end
hold off
legend('CPU - No model overhead','CPU - Model overhead','GPU - No model overhead','GPU - Model overhead','Location','NorthWest')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
xlabel('$M$','Interpreter','latex')
ylabel('Aver. comp. times (in sec)','Interpreter','latex')
set(gcf,'units','normalized','outerposition',[0 0 1 1])
set(gca,'FontSize',fontAndMarkerSize('Font','Full'))
pause(1)
printToPDF(gcf,'GPUBenchmark')