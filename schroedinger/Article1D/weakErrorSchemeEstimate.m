function weakErrorSchemeEstimate(batchSize)
addpath('Functions')
%% Set initial info and function
initSeed = 1;
% batchSize = 10^4;
% Time and area
L = 5; XInt = [-L,L];
T = 1/4; TInt = [0,T];

% Numerical precision, number of points
N = 2.^(3:10); % Time
numN = length(N);
M = 2^5; % Space
refN = 2^13; % Time (used for true mean)
refM = 2^5; % Space (used for true mean)
% refM = 2^7; % Space (used for true mean)

u0Fun = @(x) exp(-3*x.^2);
per = true;

% noiseConstant = 20;
noiseConstant = 1/4;
sigma = 1;

refModel = initModelInfo(refN,TInt,refM,XInt,sigma,per);
refH = refModel.h;
numAvailableSchemes = length(refModel.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];
%% Query definitions
% The different weak errors we will compare, currU and refU will be in Fourier space
weakErrorQuery = cell(0,0);
weakErrorQueryNames = cell(0,0);
weakErrorQueryFileNames = cell(0,0);

% L2-norm of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(real(ifft(currU)),dx,per);
weakErrorQueryNames{end+1} = '$e_1(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrRealPartL2';

% L2-norm of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(exp(-(ifft(currU)).^2),dx,per);
weakErrorQueryNames{end+1} = '$e_2(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussL2';

% L2-norm of sin of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(sin(real(ifft(currU))),dx,per);
weakErrorQueryNames{end+1} = '$e_3(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSinRealPartL2';

% Energy
weakErrorQuery{end+1} = @(currU,dx,k) energySchroed(currU,dx,k,per,sigma);
weakErrorQueryNames{end+1} = '$e_4(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrEnergy';

% H1 norm
weakErrorQuery{end+1} = @(currU,dx,k) H1norm(currU,dx,k,per);
weakErrorQueryNames{end+1} = '$e_5(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrH1';

% Odd stuff
weakErrorQuery{end+1} = @(currU,dx,k) abs(cos(abs(currU(5))));
weakErrorQueryNames{end+1} = '$e_6(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrOdd';

% Max of cosine filter
weakErrorQuery{end+1} = @(currU,dx,k) max(cos(abs(ifft(currU))));
weakErrorQueryNames{end+1} = '$e_7(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrCosMax';

% Max of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) max(abs(exp(-absSq(ifft(currU)))));
weakErrorQueryNames{end+1} = '$e_8(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussMax';

% Max of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) max(1./(1+(absSq(ifft(currU)))));
weakErrorQueryNames{end+1} = '$e_9(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrDivSqMax';

% Real space
% Min of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) min(1./(1+(absSq(ifft(currU)))));
weakErrorQueryNames{end+1} = '$e_{10}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrDivSqMin';

% Gaussian filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) exp(-max(absSq(ifft(currU))));
weakErrorQueryNames{end+1} = '$e_{11}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussOfMax';

% Square filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) max(absSq(ifft(currU)));
weakErrorQueryNames{end+1} = '$e_{12}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSqOfMax';

% Fourier space
% Min of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) min(1./(1+(absSq(currU))));
weakErrorQueryNames{end+1} = '$e_{13}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrDivSqMinFour';

% Gaussian filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) exp(-max(absSq(currU)));
weakErrorQueryNames{end+1} = '$e_{14}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussOfMaxFour';

% Square filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) max(absSq(currU));
weakErrorQueryNames{end+1} = '$e_{15}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSqOfMaxFour';

numWeaks = length(weakErrorQuery);

%% Gaussian of L4-norm
%weakErrorQuery{end+1} = @(currU,dx,k) exp(-5*L2norm(absSq(currU),dx,per));
%weakErrorQueryNames{end+1} = '$e_{15}(N)$';
%weakErrorQueryFileNames{end+1} = 'weakErrSqOfMaxFour';

numWeaks = length(weakErrorQuery);

%% Query storage
storageFilename = 'weakErrorEstimate.mat';
% If query storage exists, load it. Otherwise, create it
if isfile(storageFilename)
    % Load information of the currently saved samples
    matObj = matfile(storageFilename);
    details = whos(matObj);
    % If the size is smaller than the batch size, resize storage
    % This assumes that no new N, schemes or queries have been added
    numStoredElements = details.size(1);
    if(batchSize > numStoredElements)
        load(storageFilename);
        temp = zeros(batchSize,length(N),numUsedSchemes,numWeaks);
        temp(1:numStoredElements,:,:,:) = estimateBatch;
        estimateBatch = temp;
    end
else
    estimateBatch = zeros(batchSize,length(N),numUsedSchemes,numWeaks);
    numStoredElements = 0;
end

%% Perform calculations, only if batchSize is larger than the currently stored values
if batchSize > numStoredElements
    parfor m = (numStoredElements+1):batchSize
        rng(m,'twister')
        % Load the broadcast variables to internal
        internalSchemeIndexMat = schemeIndexMat;
        internalN = N;
        internalWeakErrorQuery = weakErrorQuery;

        % Reference brownian motion
        refW = noiseConstant*randn(refN,2)*sqrt(refH/2);

        tempEstBatch = zeros(length(N),numUsedSchemes,numWeaks);
        for n = 1:length(N)
            % Retrieve information regarding current number of steps
            currN = internalN(n);
            scalingFactor = refN/currN;
            currModel = initModelInfo(currN,TInt,M,XInt,sigma,per);

            % Calculate the coarser Brownian motion increments
            coarseDW = zeros(2,currN);
            currIndex = 0;
            for i = 1:currN
                indexList = (currIndex+1):(currIndex+scalingFactor/2);
                coarseDW(1,i) = sum(sum(refW(indexList,:)));
                indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
                coarseDW(2,i) = sum(sum(refW(indexList,:)));
                currIndex = currIndex + scalingFactor;
            end

            for j = 1:numUsedSchemes
                currU = fft(u0Fun(currModel.x));
                currScheme = internalSchemeIndexMat(j,2);
                dW = zeros(2,1);

                for i = 1:currN
                    dW = coarseDW(:,i);
                    %% Scheme and query calculations
                    currU = currModel.schemes.fun{currScheme}(currU,dW);
                end
                for i = 1:numWeaks
                    tempEstBatch(n,j,i) = internalWeakErrorQuery{i}(currU,currModel.dx,currModel.k);
                end

            end
        end
        estimateBatch(m,:,:,:) = tempEstBatch;
        m
    end

    % Save the data
    save(storageFilename,'estimateBatch','-v7.3')
end