function weakErrorTrueEstimate(batchSize)
addpath('Functions')
%% Set initial info and function
initSeed = 1;
% batchSize = 10^4;
% Time and area
L = 5; XInt = [-L,L];
T = 1/4; TInt = [0,T];

% Numerical precision, number of points
% refN = 2^12; % Time
% refM = 2^8; % Space
% Old settings, see weakError.m
refN = 2^13; % Time
% refM = 2^7; % Space
refM = 2^5; % Space

u0Fun = @(x) exp(-3*x.^2);
per = true;

% noiseConstant = 20;
noiseConstant = 1/4;
sigma = 1;

refModel = initModelInfo(refN,TInt,refM,XInt,sigma,per);

%% Query definitions
% The different weak errors we will compare, currU and refU will be in Fourier space
weakErrorQuery = cell(0,0);
weakErrorQueryNames = cell(0,0);
weakErrorQueryFileNames = cell(0,0);

% L2-norm of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(real(ifft(currU)),dx,per);
weakErrorQueryNames{end+1} = '$e_1(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrRealPartL2';

% L2-norm of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(exp(-(ifft(currU)).^2),dx,per);
weakErrorQueryNames{end+1} = '$e_2(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussL2';

% L2-norm of sin of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(sin(real(ifft(currU))),dx,per);
weakErrorQueryNames{end+1} = '$e_3(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSinRealPartL2';

% Energy
weakErrorQuery{end+1} = @(currU,dx,k) energySchroed(currU,dx,k,per,sigma);
weakErrorQueryNames{end+1} = '$e_4(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrEnergy';

% H1 norm
weakErrorQuery{end+1} = @(currU,dx,k) H1norm(currU,dx,k,per);
weakErrorQueryNames{end+1} = '$e_5(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrH1';

% Odd stuff
weakErrorQuery{end+1} = @(currU,dx,k) abs(cos(abs(currU(5))));
weakErrorQueryNames{end+1} = '$e_6(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrOdd';

% Max of cosine filter
weakErrorQuery{end+1} = @(currU,dx,k) max(cos(abs(ifft(currU))));
weakErrorQueryNames{end+1} = '$e_7(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrCosMax';

% Max of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) max(abs(exp(-absSq(ifft(currU)))));
weakErrorQueryNames{end+1} = '$e_8(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussMax';

% Max of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) max(1./(1+(absSq(ifft(currU)))));
weakErrorQueryNames{end+1} = '$e_9(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrDivSqMax';

% Real space
% Min of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) min(1./(1+(absSq(ifft(currU)))));
weakErrorQueryNames{end+1} = '$e_{10}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrDivSqMin';

% Gaussian filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) exp(-max(absSq(ifft(currU))));
weakErrorQueryNames{end+1} = '$e_{11}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussOfMax';

% Square filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) max(absSq(ifft(currU)));
weakErrorQueryNames{end+1} = '$e_{12}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSqOfMax';

% Fourier space
% Min of division by square filter
weakErrorQuery{end+1} = @(currU,dx,k) min(1./(1+(absSq(currU))));
weakErrorQueryNames{end+1} = '$e_{13}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrDivSqMinFour';

% Gaussian filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) exp(-max(absSq(currU)));
weakErrorQueryNames{end+1} = '$e_{14}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussOfMaxFour';

% Square filter on maximum value
weakErrorQuery{end+1} = @(currU,dx,k) max(absSq(currU));
weakErrorQueryNames{end+1} = '$e_{15}(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSqOfMaxFour';

numWeaks = length(weakErrorQuery);
%% Query storage
storageFilename = 'weakErrorReference.mat';
% If query storage exists, load it. Otherwise, create it
if isfile(storageFilename)
    % Load information of the currently saved samples
    matObj = matfile(storageFilename);
    details = whos(matObj);
    % If the size is smaller than the batch size, resize storage
    % This assumes that no new N, schemes or queries have been added
    numStoredElements = details.size(1);
    if(batchSize > numStoredElements)
        load(storageFilename);
        temp = zeros(batchSize,numWeaks);
        temp(1:numStoredElements,:) = refBatch;
        refBatch = temp;
    end
else
    refBatch = zeros(batchSize,numWeaks);
    numStoredElements = 0;
end

%% Perform calculations, only if batchSize is larger than the currently stored values

if batchSize > numStoredElements
    parfor m = (numStoredElements+1):batchSize
        rng(m,'twister')
        % Load the broadcast variables to internal
        internalWeakErrorQuery = weakErrorQuery;
        internalRefModel = refModel;

        % Calculate reference solution
        refW = noiseConstant*randn(refN,2)*sqrt(internalRefModel.h/2);
        currU = fft(u0Fun(internalRefModel.x));
        for i = 1:refN
            dW = refW(i,:);
            % Using Lie-Trotter splitting scheme as reference solution
            currU = internalRefModel.schemes.fun{7}(currU,dW);
        end
        tempRefBatch = zeros(numWeaks,1);
        for i = 1:numWeaks
             tempRefBatch(i) = internalWeakErrorQuery{i}(currU,internalRefModel.dx,internalRefModel.k);
        end
        refBatch(m,:) = tempRefBatch;
        m
    end

    % Save the data
    save(storageFilename,'refBatch','-v7.3')
end

%% Calculate the CI - Monte Carlo estimation
% numMeans = 10^1;
% numMeansInMean = batchSize/numMeans;
% refMeanMat = zeros(numMeans,numWeaks);
% 
% perc = abs(norminv((1-confLevel)/2,0,1));
% 
% for i = 1:numMeans
%     for j = 1:numWeaks
%         refMeanMat(i,j) = mean(refBatch((i-1)*numMeansInMean+(1:numMeansInMean),j));
%     end
% end
% 
% meanEst = mean(refMeanMat);
% stdEst = std(refMeanMat);
% 
% CIEst = zeros(numWeaks,2);
% CIEst(:,1) = meanEst - perc*stdEst/sqrt(numMeans);
% CIEst(:,2) = meanEst + perc*stdEst/sqrt(numMeans);
% 
% CIEst
% perc*stdEst/sqrt(numMeans)
% 
% meanEst./(perc*stdEst/sqrt(numMeans))