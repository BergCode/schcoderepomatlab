function vertProbPlotMult(storage,NVec,topTitle,rightTitle,varargin)
% vertProbPlotMult  - Vertical histograms with titles.
% Syntax: vertHistPlot(storage,schemeShortNames,yAxisVector,leftTitle,axesInfo)
%
% Input:
% storage           - 
% schemeShortNames  - A cell vector of length m containing strings.
% yAxisVector       - A vector containing the y-axis labels.
% leftTitle         - A string containing the left side title.
% varargin          - If storage has dim 4, accept line styles
%
% Non-standard dependencies: None.
% See also: PSHist.m for example usage.
    numRight = size(storage,1);
    numTop = size(storage,2);
    numN = size(storage,3);
    numSchemes = size(storage,4);
    
    % ERROR if wrong dimension
    if numTop ~= length(topTitle) || numRight ~= length(rightTitle)
        error(['The numer of titles does not match the number of data.'...
            '\n Number of column values: ' num2str(numTop)...
            '\n Number of top titles: ' num2str(length(topTitle))...
            '\n Number of row values: ' num2str(numRight)...
            '\n Number of right titles: %s'], num2str(length(rightTitle)))
    end
    if numN ~= length(NVec)
        error('Length of NVec and storage dimension 3 not equal.')
    end
    if numSchemes ~= length(varargin{1})
        error('Length of line styles and storage dimension 4 not equal.')
    end
    
    figure
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    % Set top titles by adapting to the subplot positions
    for n = 1:numTop
        posInfo = get(subplot(numRight,numTop,n),'position');
        annotation('textbox',[posInfo(1)+0.2*posInfo(3) posInfo(2)+1.3*posInfo(4) 0 0],...
            'String',topTitle{n},...
            'EdgeColor','none',...
            'Interpreter','latex',...
            'FontSize',fontAndMarkerSize('Font','Full'))
    end
    
    % Set right titles by adapting to the subplot positions
    for j = 1:numRight
        posInfo = get(subplot(numRight,numTop,j*numTop),'position');
        annotation('textbox',[posInfo(1)+1.02*posInfo(3) posInfo(2)+0.65*posInfo(4) 0.05 0],...
            'String',rightTitle{j},...
            'EdgeColor','none',...
            'Interpreter','latex',...
            'FontSize',fontAndMarkerSize('Font','Full'))
    end
    
    for j = 1:numRight
        for n = 1:numTop
            subplot(numRight,numTop,(j-1)*numTop+n);
            % Plot probabilities
            if nargin == 4
                plot(NVec,squeeze(storage(j,n,:)),'LineWidth',2.5);
            elseif nargin == 6
                hold on
                for i = 1:numSchemes
                    plot(NVec,squeeze(storage(j,n,:,i)),varargin{1}{i},'color',varargin{2}{i},'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Full'));
                end
                hold off
            else
                error('Wrong number of input arguments.')
            end
            ylim([-.01 1.1])
            xlim([min(NVec) max(NVec)])
            
            set(gca, 'XScale', 'log')
            set(gca,'FontSize',fontAndMarkerSize('Font','Full'))
            
            % If leftmost row add probability label, otherwise remove ticks
            if(n == 1)
                ylabel('$p$','Interpreter','latex')%,'Rotation',0)
%                 ytickangle(90)
            else
                set(gca,'YTickLabel',[])
            end
            % If bottom row add N label, otherwise remove ticks
            if(j == numRight)
                xlabel('$N$','Interpreter','latex')
            else
                set(gca,'XTickLabel',[])
            end
        end
    end
    
end