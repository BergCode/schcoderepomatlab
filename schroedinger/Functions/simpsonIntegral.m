function [integral] = simpsonIntegral(dx,f)
% simpsonIntegral - The Simpson integral approximation of f.
% Syntax: [integral] = simpsonIntegral(dx,f)
%
% Input:
% dx    - Space step size
% f     - A vector of f(x), where x is a vector such that x(i)-x(i-1)=dx
%
% Output:
% integral  - A scalar of the Simpson integral approximation
%
% Non-standard dependencies: None.
% See also: Any accompanying script for example usage.

% Simpson integral approximation uses an odd number of points
if mod(length(f),2) == 0
    % Simpson 3/8 rule at the end
    g = f(1:(end-3));
    
	integral = simpsonIntegral(dx,g);
    integral = integral + (3*dx/8)*(f(end-3) + 3*f(end-2) + 3*f(end-1) + f(end));
else
	integral = (f(1) + f(end) + ...
        2*sum(f(3:2:(end-2))) + ...
        4*sum(f(2:2:(end-1))))*dx/3;
end
end

