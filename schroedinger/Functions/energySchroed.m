function ret = energySchroed(u,dx,k,per,sigma)
% energyShroed - A Simpson integral approximation of the energy for the
% Schroedinger equation
% Syntax: ret = energyShroed(u,dx,k,per,sigma)
%
% Input:
% u     - A vector of length M containing the function u in Fourier space.
% dx    - The space step size.
% k     - A vector of length M containing the Fouier modes.
% per   - A boolean value declaring whether u periodic or not.
% sigma - A non-linearity paramter
%
% Output:
% ret - 1/2 int abs(d/dx u)^2 - 1/(sigma + 1) abs(u)^(2 sigma + 2) dx
%
% Non-standard dependencies: L2norm.m.
% See also: L2norm.m
    ret = 1/2*L2norm(ifft(1i*k.*u),dx,per) - ...
    1/(2*sigma+2)*L2norm(ifft(u).^(sigma+1),dx,per);
end