function ret = energySchroed2D(u,dx,k,sigma,sideLen)
% energySchroed2D - An approximation of the energy for the
% Schroedinger equation in 2D
% Syntax: ret = energyShroed2D(u,dx,k,per,sigma,sideLen)
%
% Input:
% u         - A square matrix containing the function u in Fourier space
% dx        - Vector containing the 2 spatial step sizes
% k         - An MxMx2 matrix containing the Fouier modes
% sigma     - A non-linearity paramter
% sideLen   - A vector containing the length of the sides of the area
%
% Output:
% ret - 1/2 int abs(d/dx u)^2 - 1/(sigma + 1) abs(u)^(2 sigma + 2) dx
%
% Non-standard dependencies: L2norm.m.
% See also: L2norm.m
    tempReal = ifft2(u);
    ret = 1/2*L2norm2D(sum(k,3).*u,dx,true,false,sideLen) -...
        1/(2*sigma+2)*L2norm2D(fft2(tempReal.^(sigma+1)),dx,true,false,sideLen);
end