function printToText(fileName,vector)

    if ~exist('Data', 'dir')
        mkdir('Data');
    end
    % The precision of later comparisons will be limited by this
    dlmwrite(fileName,vector,'precision',17);
    movefile(fileName,'Data')
end