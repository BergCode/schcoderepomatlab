function [integral] = simpsonIntegral2D(dx,f)
% simpsonIntegral2D - The Simpson integral approximation of f in 2D.
% Syntax: [integral] = simpsonIntegral2D(dx,f)
%
% Input:
% dx    - Vector containing the 2 spatial step sizes
% f     - A square matrix of f(x,y)
%
% Output:
% integral  - A scalar of the Simpson integral approximation
%
% Non-standard dependencies: None.
% See also: Any accompanying script for example usage.


% Simpson integral approximation uses an odd number of points
if mod(length(f),2) == 0
    % Trapezoidal rule at the end
    g = f(1:end-1,1:end-1);
	integral = simpsonIntegral(dx,g);
    
    integral = integral + dx/4*(...
        sum(f(end-1,1:end-1)) + ...
        sum(f(1:end-2,end-1)) + ...
        sum(f(end,:)) + ...
        sum(f(:,end-1)));
else
    M = length(f);
    weight = 4*ones(M,M);
    weight(2:2:end-1,:) = 2*weight(2:2:end-1,:);
    weight(:,2:2:end-1) = 2*weight(:,2:2:end-1);
    weight(1,:) = weight(1,:)/2;
    weight(:,1) = weight(:,1)/2;
    weight(end,:) = weight(end,:)/2;
    weight(:,end) = weight(:,end)/2;
    
	integral = weight.*f*dx(1)*dx(2)/9;
    integral = sum(integral(:));
end


end