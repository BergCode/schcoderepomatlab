function ret = schroedEnergy(u,dx,k,sigma,per)
% schroedEnergy - A Simpson integral approximation of the energy.
% Syntax: ret = schroedEnergy(u,dx,k,sigma,per)
%
% Input:
% u     - A vector of length M containing the function u in Fourier space.
% dx    - The space step size.
% k     - A vector of length M containing the Fouier modes.
% sigma - The value for the non-linearity
% per   - A boolean value declaring whether u periodic or not.
%
% Output:
% ret - 1/2 int abs(d/dx u)^2 dx + 1/(2 sigma+2) abs(u)^(2 sigma+2)dx
%
% Non-standard dependencies: L2norm.m.
% See also: L2norm.m
    ret = 1/2*L2norm(ifft(1i*k.*u),dx,per) + 1/(2*sigma+2)*L2norm(ifft(u).^(sigma+1),dx,per);
end