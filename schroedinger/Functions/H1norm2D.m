function ret = H1norm2D(u,dx,k,per,varargin)
% H1norm2D - An approximation of the H1 norm.
% Syntax: ret = H1norm2D(u,dx,k1,k2,per)
%
% Input:
% u         - A square matrix containing the function u in Fourier space
% dx        - Vector containing the 2 spatial step sizes
% k         - An MxMx2 matrix containing the Fouier modes
% per       - A boolean value declaring whether u periodic or not
% varargin  - A pair of values, only applicable if periodic
%             1. A boolean flag for whether the Simpsons integral 
%             approximation should be used, or whether the Parseval theorem
%             info should be used (false for Parseval)
%             2. Array containing the two interval lengths
%
% Output:
% ret - int abs(u)^2 + abs(d/dx u)^2 dx
%
% Non-standard dependencies: L2norm2D.m.
% See also: L2norm.m

    % Check that u is a matrix of dimension 2
    if ~ismatrix(u)
        error('u not matrix of size 2, required for 2D H1 norm approximation')
    end
    
    % Simpsons integral by default
    if nargin > 4 && ~varargin{1}
        M = size(u);
        L = varargin{2};
        ret = sum(sum((1 + sum(k.^2,3)).*absSq(u)))/prod(M.^2)*prod(L);
    else
        ret = L2norm2D(ifft2(u),dx,per) + ...
            L2norm2D(ifft2(1i*k(:,:,1).*u),dx,per) + ...
            L2norm2D(ifft2(1i*k(:,:,2).*u),dx,per);
    end
end