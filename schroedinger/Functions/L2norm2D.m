function ret = L2norm2D(u,dx,per,varargin)
% L2norm2D - An approximation of the L2 norm, or the squared absolute integral.
% Syntax: ret = L2norm2D(u,dx,per,varargin)
%
% Input:
% u         - A square matrix containing the function u in physical space
% dx        - Vector containing the 2 spatial step sizes
% per       - A boolean value declaring whether u periodic or not
% varargin  - A pair of values, only applicable if periodic
%             1. A boolean flag for whether the Simpsons integral 
%             approximation should be used, or whether the Parseval theorem
%             info should be used (false for Parseval)
%             Assumes that u is in Fourier space and periodic if false.
%             2. Interval length
%
% Output:
% ret - int abs(u)^2 dx
%
% Non-standard dependencies: absSq.m, simpsonIntegral2D.m
% See also: PSHist.m for example usage.
%           L2norm.m
%           H1norm.m

    % Check that u is a matrix of dimension 2
    if ~ismatrix(u)
        error('u not matrix of size 2, required for 2D L2 norm approximation')
    end

    if per
        
        % Simpsons integral by default
        if nargin > 3 && ~varargin{1}
            M = size(u);
            L = varargin{2};
            ret = sum(sum(absSq(u)))/prod(M.^2)*prod(L);
        else
            % Append first column / row
            M = length(u);
            temp = zeros(M+1,M+1);
            temp(1:end-1,1:end-1) = u;
            temp(end,:) = temp(1,:);
            temp(:,end) = temp(:,1);

            ret = simpsonIntegral2D(dx,absSq(temp));
        end
    else
        ret = simpsonIntegral2D(dx,absSq(u));
    end
end