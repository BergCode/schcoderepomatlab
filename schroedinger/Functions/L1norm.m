function ret = L1norm(u,dx,per)
% L1norm - An approximation of the L1 norm, or the absolute integral.
% Syntax: ret = L1norm(u,dx,per)
%
% Input:
% u     - A vector containing the function u in physical space
% dx    - The space step size.
% per   - A boolean value declaring whether u periodic or not.
%
% Output:
% ret - int abs(u) dx
%
% Non-standard dependencies: simpsonIntegral.m
% See also: PSHist.m for example usage.
%           H1norm.m

    % Check that u is a vector
    if size(u,1) ~= 1 && size(u,2) ~= 1
        error('u not vector, L1 norm')
    end
    
    if per
        % Append first value
        if size(u,1) == 1
            ret = simpsonIntegral(dx,abs([u u(1)]));
        else
            ret = simpsonIntegral(dx,abs([u;u(1)]));
        end
    else
        ret = simpsonIntegral(dx,abs(u));
    end
end