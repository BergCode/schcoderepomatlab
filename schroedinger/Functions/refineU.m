function refU = refineU(u,periodic,refType,varargin)
% refineU   - Refine u using linear interpolation. Assuming u periodic.
% Syntax: ret = refineU(u,periodic,refType,varargin)
% varargin either nothing or x and xRef
%
% Input:
% u         - A vector of size (1,M) containing the function u
% periodic  - A boolean which decides if u is interpreted as periodic
% refType   - A boolean which decides which interpolation is used.
%               'Linear'    - Simple piecewise linear interpolation
%               'Interpft'   - Interpolation using Fourier modes
%               'nFme'   - Interpolation using Fourier modes
%               'pchip' - Piecewise Cubic Hermite Interpolating Polynomial
%                       Good for interpolation of non-oscillatory problems
%               'spline'    - Cubic spline data interpolation
%                           Good for interpolation of oscillatory problems
% x         - Vector containing space points for u(x)
% xRef      - Vector containing space points for refU(xRef)
% 
%
% Output:
% ret   - A vector containining the refined u, of length 2*M if periodic or
%         of length 2*M-1 if not
%
% Non-standard dependencies: None.
% See also: ~.
    if size(u,1)~= 1 && size(u,2)~= 1
        error('Error: u must be a vector of size (1,M) or (M,1).')
    end
    
    M = length(u);
    
    switch refType
        case 'Linear'
            if periodic
                refU = [u ; (u + [u(2:end) u(1)])/2];
                refU = refU(:).';
            else
                refU = zeros(2*M-1,1);
                refU(1:2:end) = u;
                refU(2:2:end-1) = (u(1:(end-1)) + u(2:end))/2;
            end
        case 'Interpft'
            if periodic
                refU = interpft([u , u(1)],2*M+1);
                refU = refU(1:end-1);
            else
                refU = interpft(u,2*M-1);
            end
        case 'Pchip'
            x = varargin{1};
            xRef = varargin{2};
            refU = pchip(x,u,xRef);
        case 'Spline'
            x = varargin{1};
            xRef = varargin{2};
            refU = spline(x,u,xRef);
        case 'nFme'
            % Adding higher Fourier frequencies with 0 value
            temp = 2*fft(u);
            refU = ifft([temp(1:(M/2)),zeros(1,M),temp((M/2+1):M)]);
        otherwise
            fprintf('Unknown refinement')
    end
end