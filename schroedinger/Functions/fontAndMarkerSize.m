function [ret] = fontAndMarkerSize(fontOrMarker,fullOrHalf)
% Specify font and marker sizes for the plot outputs
% Defaults to marker and half page size plot


askingForFontSize = strcmp(fontOrMarker,'Font');
askingForFull = strcmp(fullOrHalf,'Full');

if askingForFontSize
    if askingForFull
        ret = 35;
    else
        ret = 50;
    end
else
    if askingForFull
        ret = 14;
    else
        ret = 20;
    end
end

end

