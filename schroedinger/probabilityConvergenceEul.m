addpath('Functions')
%% Set initial info and function
initSeed = 1;
batchSize = 200;
confLevel = 0.95;
% Time and area
L = 10; XInt = [-L,L];
T = 1/4; TInt = [0,T];

% Numerical precision, number of points
% refN = 2^22;
refN = 2^22;
refh = (TInt(2)-TInt(1))/refN;
% N = 2.^(5:20); % Time
N = 2.^(5:20); % Time
h = (TInt(2)-TInt(1))./N;
numN = length(N);
% M = 2^6; % Space
M = 2^5; % Space
dx = (XInt(2)-XInt(1))/M;

u0Fun = @(x) exp(-3*x.^2);
per = true;

sigma = 1;

k = 2*pi/(XInt(2)-XInt(1))*[0:M/2-1, 0, -M/2+1:-1];
kSq = k.^2;

x = XInt(1) + dx*(0:M-1);
u0FunVal = u0Fun(x);

refSchemes = makePSSchroedSchemes(kSq,refh,sigma);
numAvailableSchemes = length(refSchemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
% schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
% schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];
%% Query storage
% H1 & L2 storage
if isfile('probConvSamplesH1.mat') && isfile('probConvSamplesL2.mat')
    load('probConvSamplesH1.mat');
    load('probConvSamplesL2.mat');
    % If the size is smaller than the batch size, resize storage
    % This assumes that no new N, schemes or queries have been added
    numStoredElements = size(maxH1DiffBatch,1);
    if(numStoredElements < batchSize)
        temp = zeros(batchSize,length(N),numUsedSchemes);
        temp(1:numStoredElements,:,:) = maxH1DiffBatch;
        maxH1DiffBatch = temp;
        
        temp = zeros(batchSize,length(N),numUsedSchemes);
        temp(1:numStoredElements,:,:) = maxL2DiffBatch;
        maxL2DiffBatch = temp;
    end
else
    maxH1DiffBatch = zeros(batchSize,length(N),numUsedSchemes);
    maxL2DiffBatch = maxH1DiffBatch;
end
%% Perform calculations
rng(initSeed,'twister')
parfor m = 1:batchSize
    % If we've already calculated this sample, skip it
    criteriaH1 = maxH1DiffBatch(m,:,:) == 0;
    criteriaL2 = maxL2DiffBatch(m,:,:) == 0;
    if any(criteriaH1(:)) || any(criteriaL2(:))
        % Load the broadcast variables to internal
        internalRefSchemes = refSchemes;
        internalSchemeIndexMat = schemeIndexMat;
        internalN = N;
        internalh = h;

        % Produce the reference Brownian motion
        refW = randn(refN,2)*sqrt(refh/2);
        % Initialize memory
        tempH1Batch = zeros(length(N),numUsedSchemes);
        tempL2Batch = zeros(length(N),numUsedSchemes);
        % Produce the solution storage and coarse Brownian motions
        refSol = fft(u0FunVal);
        coarseSol = cell(numN,numUsedSchemes);
        schemes = cell(numN,1);
        coarseW = cell(numN,1);
        scalingFactor = zeros(numN,1);
        for n = 1:numN
            currN = internalN(n);
            currh = internalh(n);

            % Calculate the coarser Brownian motion increments
            scalingFactor(n) = refN/currN;
            coarseW{n} = zeros(currN,2);
            currIndex = 0;
            for i = 1:currN
                indexList = (currIndex+1):(currIndex+scalingFactor(n)/2);
                coarseW{n}(i,1) = sum(sum(refW(indexList,:)));
                indexList = (currIndex+scalingFactor(n)/2+1):(currIndex+scalingFactor(n));
                coarseW{n}(i,2) = sum(sum(refW(indexList,:)));
                currIndex = currIndex + scalingFactor(n);
            end
            %% Check that coarse Brownian motions are correct (which they are)
%                 close all
%                 figure(1)
%                 hold on
%                 plot(linspace(0,T,refN+1),[0 ; cumsum(sum(refW,2))])
%                 pause(2)
%                 for i = 1:numN
%                     plot(linspace(0,T,N(i)+1),[0 ; cumsum(sum(coarseW{i},2))])
%                     pause(2)
%                 end
%                 hold off
            %%
            % Solution initial values
            for j = 1:numUsedSchemes
                coarseSol{n,j} = refSol;
            end
            schemes{n} = makePSSchroedSchemes(kSq,currh,sigma);
        end

        % Simulate at reference precision, but step coarse solutions when the
        % correct time has been passed. Only calculate difference when coarsest
        % calculation is made
        dWIndex = ones(numN,1);
        for i = 1:refN
            refSol = internalRefSchemes.fun{7}(refSol,refW(i,:));
            for n = 1:numN
                % Step coarse solution if enough steps passed
                if mod(i,scalingFactor(n)) == 0
                    for j = 1:numUsedSchemes
                        currScheme = internalSchemeIndexMat(j,2);
                        coarseSol{n,j} = schemes{n}.fun{currScheme}(coarseSol{n,j},coarseW{n}(dWIndex(n),:));
                    end
                    dWIndex(n) = dWIndex(n) + 1;
                end
            end

            % If we fulfill the coarsest, assuming N increasing, then we
            % calculate the difference.
            % If Correct, should have mod == 0 for all scalingFactors
            if  mod(i,scalingFactor(1)) == 0
                for n = 1:numN
                    for j = 1:numUsedSchemes
                        tempH1Batch(n,j) = max(sqrt(H1norm(refSol-coarseSol{n,j},dx,k,per)),tempH1Batch(n,j));
                        tempL2Batch(n,j) = max(sqrt(L2norm(ifft(refSol)-ifft(coarseSol{n,j}),dx,per)),tempL2Batch(n,j));
                    end
                end
                fprintf(['-------'...
                    '\n Sample: ', num2str(m), ...
                    '.\n Percentage until max time: ', num2str(i/refN), ...
                    '.\n-------\n'])
            end
        end
        maxH1DiffBatch(m,:,:) = tempH1Batch;
        maxL2DiffBatch(m,:,:) = tempL2Batch;
    end
end
%%
save('probConvSamplesH1Eul.mat','maxH1DiffBatch','-v7.3')
save('probConvSamplesL2Eul.mat','maxL2DiffBatch','-v7.3')
%% Process into probabilities
% Use one contant vector per scheme
cVectorEul = 10.^[2.5 3 3.5];
numC = length(cVectorEul);
deltaVectorEul = [0.9 1 1.1];
numDelta = length(deltaVectorEul);

% Size of time steps
timeStepMatrix = T./N;

% Storage
probConvStorageH1 = zeros(numC,numDelta,numN,numUsedSchemes);
probConvStorageL2 = zeros(numC,numDelta,numN,numUsedSchemes);

for i = 1:numC
    for j = 1:numDelta
        deltaFlow = deltaVectorFlow(j);
        deltaEul = deltaVectorEul(j);
        for n = 1:numN
            for scheme = 1:numUsedSchemes
                % Use different for Euler type schemes
                c = cVectorEul(i);
                probConvStorageH1(i,j,n,scheme) = sum(squeeze(maxH1DiffBatch(:,n,scheme)) > c*((T/N(n)).^deltaEul) )/batchSize;
                probConvStorageL2(i,j,n,scheme) = sum(squeeze(maxL2DiffBatch(:,n,scheme)) > c*((T/N(n)).^deltaEul) )/batchSize;
            end
        end
    end
end

%% Initialize title information
rightTitleEul = cell(numC,1);
topTitleEul = cell(numDelta,1);

for j = 1:numC
    rightTitleEul{j} = ['$C = $ $10^{' num2str(log10(cVectorEul(j))) '}$'];
end
for j = 1:numDelta
    topTitleEul{j} = ['$\delta = ' num2str(deltaVectorEul(j)) '$'];
end


%% Mean square convergence confirmation
% figure
% loglog(N,squeeze(mean(maxH1DiffBatch)),N,N.^-(1/2),'--',N,N.^-1,'--')
% figure
% loglog(N,squeeze(mean(maxL2DiffBatch)),N,N.^-(1/2),'--',N,N.^-1,'--')
%% One plot per family of similar schemes (special case)
close all
vertProbPlotMult(probConvStorageH1,N,topTitleEul,rightTitleEul,{'-s','-*'})
pause(0.1)
printToPDF(gcf,'probConvH1EulType')
vertProbPlotMult(probConvStorageL2,N,topTitleEul,rightTitleEul,{'-s','-*'})
pause(0.1)
