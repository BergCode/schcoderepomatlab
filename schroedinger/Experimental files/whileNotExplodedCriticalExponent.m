%% Set initial info and function
addpath('Data')
addpath('Functions')
initSeed = 4;
batchSize = 200;
% Time and area
L = 5*pi; XInt = [-L,L];
T = 10^(-6); TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
% N = 2^5; % Time
% M = 2^4; % Space
N = 2^17; % Time
M = 2^16; % Space
refM = 2*M; % Space
NVec = [N, 2*N, 4*N, 8*N];
sigmaVec = [3.9, 4, 4.1];
per = true;

u0Fun = @(x) 10*exp(-10*x.^2);

% Calculate initial H1 value and 
dx = (XInt(2)-XInt(1))/M;
x = XInt(1) + dx*(0:M-1);
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2-1, 0, -M/2+1:-1];
initH1 = H1norm(fft(u0Fun(x)),dx,k,per);

% If the H1 ratio between u_n+1 and u_n exceeds this, consider it exploded 
blowUpRatio = 1.2;
% Alternatively, if it exceeds 25 times the initial H1 value (~10'000)
blowUpLimit = 25*initH1;
% If the H1 norm exceeds this value, perform spatial refinement
treshhold = 10*initH1;
%% Initialize storage
numSigma = length(sigmaVec);
numN = length(NVec);
% Two methods of blow up detection
% Four different refinement levels / methods
blowupTimes = zeros(batchSize,2,4,numN,numSigma);

%% Perform calculations
rng(initSeed,'twister')
parfor m = 1:batchSize
    internalBlowupTimes = zeros(2,4,numN,numSigma);
    
    maxN = max(NVec);
    dW = randn(2,maxN)*sqrt(T/maxN);
    
    % Vary N and sigma
    for n = 1:length(NVec)
        for j = 1:length(sigmaVec)
            currN = NVec(n);
            sigma = sigmaVec(j);
            % If we are not using the finest time discretization, calculate the
            % coarser Brownian motion increments
            if currN < maxN
                scalingFactor = maxN/currN;
                coarseDW = zeros(2,currN);
                currIndex = 0;
                for i = 1:currN
                    indexList = (currIndex+1):(currIndex+scalingFactor/2);
                    coarseDW(1,i) = sum(sum(dW(:,indexList)));
                    indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
                    coarseDW(2,i) = sum(sum(dW(:,indexList)));
                    currIndex = currIndex + scalingFactor;
                end
            else
                coarseDW = dW;
            end
            modelInfo = initModelInfo(currN,TInt,M,XInt,sigma,per);
            fineModelInfo = initModelInfo(currN,TInt,2*M,XInt,sigma,per);

%             % Load variables into GPU memory
%             currU = gpuArray(fft(u0Fun(modelInfo.x)));
%             currUFine = gpuArray(fft(u0Fun(fineModelInfo.x)));
%             coarseDW = gpuArray(coarseDW);
            currU = cell(4,1); % Coarse, fine, linear, nFme
            currU{1} = fft(u0Fun(modelInfo.x));
            currU{2} = fft(u0Fun(fineModelInfo.x));
            % Norm storage
            currH1Val = zeros(4,1);
            currH1Val(1) = H1norm(currU{1},modelInfo.dx,modelInfo.k,per);
            currH1Val(2) = H1norm(currU{2},fineModelInfo.dx,fineModelInfo.k,per);
            oldH1Val = currH1Val;

            % Simulate spatially coarse fine, and refined processes
            % Refine spatially once, if H1 > treshhold at some point
            refined = false;
            
            % Two criteria per process
            notExploded = true(4,2);
                notExploded(3,:) = false(2,1);
                notExploded(4,:) = false(2,1);
                
            i = 1;
            t = 0;
            while any(notExploded(:)) && i <= currN
                t = t + modelInfo.h;
                currDW = coarseDW(:,i);
                % Coarse process
                if(any(notExploded(1,:)))
                    % Progress
                    currU{1} = modelInfo.schemes.fun{7}(currU{1},currDW);
                    % Calculate norm
                    oldH1Val(1) = currH1Val(1);
                    currH1Val(1) = H1norm(currU{1},modelInfo.dx,modelInfo.k,per);
                    % Check if it has exploded
                    notExploded(1,1) = ~ (currH1Val(1)/oldH1Val(1) > blowUpRatio);
                    notExploded(1,2) = ~ (currH1Val(1) > blowUpLimit);
                    % Record time in case it has not been recorded
                    if(internalBlowupTimes(1,1,n,j) == 0 && ~notExploded(1,1))
                        internalBlowupTimes(1,1,n,j) = t;
                    end
                    if(internalBlowupTimes(2,1,n,j) == 0 && ~notExploded(1,2))
                        internalBlowupTimes(2,1,n,j) = t;
                    end
                end
                % Fine process
                if(any(notExploded(2,:)))
                    % Progress
                    currU{2} = fineModelInfo.schemes.fun{7}(currU{2},currDW);
                    % Calculate norm
                    oldH1Val(2) = currH1Val(2);
                    currH1Val(2) = H1norm(currU{2},fineModelInfo.dx,fineModelInfo.k,per);
                    % Check if it has exploded
                    notExploded(2,1) = ~ (currH1Val(2)/oldH1Val(2) > blowUpRatio);
                    notExploded(2,2) = ~ (currH1Val(2) > blowUpLimit);
                    % Record time in case it has not been recorded
                    if(internalBlowupTimes(1,2,n,j) == 0 && ~notExploded(2,1))
                        internalBlowupTimes(1,2,n,j) = t;
                    end
                    if(internalBlowupTimes(2,2,n,j) == 0 && ~notExploded(2,2))
                        internalBlowupTimes(2,2,n,j) = t;
                    end
                end

                if refined
                    % Linearely interpolated process
                    if(any(notExploded(3,:)))
                        % Progress
                        currU{3} = fineModelInfo.schemes.fun{7}(currU{3},currDW);
                        % Calculate norm
                        oldH1Val(3) = currH1Val(3);
                        currH1Val(3) = H1norm(currU{3},fineModelInfo.dx,fineModelInfo.k,per);
                        % Check if it has exploded
                        notExploded(3,1) = ~ (currH1Val(3)/oldH1Val(3) > blowUpRatio);
                        notExploded(3,2) = ~ (currH1Val(3) > blowUpLimit);
                        % Record time in case it has not been recorded
                        if(internalBlowupTimes(1,3,n,j) == 0 && ~notExploded(3,1))
                            internalBlowupTimes(1,3,n,j) = t;
                        end
                        if(internalBlowupTimes(2,3,n,j) == 0 && ~notExploded(3,2))
                            internalBlowupTimes(2,3,n,j) = t;
                        end
                    end
                    % Fft interpolated process
                    if(any(notExploded(4,:)))
                        % Progress
                        currU{4} = fineModelInfo.schemes.fun{7}(currU{4},currDW);
                        % Calculate norm
                        oldH1Val(4) = currH1Val(4);
                        currH1Val(4) = H1norm(currU{4},fineModelInfo.dx,fineModelInfo.k,per);
                        % Check if it has exploded
                        notExploded(4,1) = ~ (currH1Val(4)/oldH1Val(4) > blowUpRatio);
                        notExploded(4,2) = ~ (currH1Val(4) > blowUpLimit);
                        % Record time in case it has not been recorded
                        if(internalBlowupTimes(1,4,n,j) == 0 && ~notExploded(4,1))
                            internalBlowupTimes(1,4,n,j) = t;
                        end
                        if(internalBlowupTimes(2,4,n,j) == 0 && ~notExploded(4,2))
                            internalBlowupTimes(2,4,n,j) = t;
                        end
                    end
                end

                % Perform spatial refinement, if necessary
                if currH1Val(1) > treshhold && ~refined
                    % Refine
                    tempCurrU = ifft(currU{1});
                    currU{3} = fft(refineU(tempCurrU,per,'Linear',modelInfo.x,fineModelInfo.x));
                    currU{4} = fft(refineU(tempCurrU,per,'nFme',modelInfo.x,fineModelInfo.x));

                    tempCurrU = [];
                    
                    % Store H1 norm
                    currH1Val(3) = currH1Val(1);
                    currH1Val(4) = currH1Val(1);
                    refined = true;
                    
                    % Set as not yet exploded
                    notExploded(3,:) = true(2,1);
                    notExploded(4,:) = true(2,1);
                end

                fprintf(['\n-------'...
                    '\n Sample: ', num2str(m), ...
                    '\n N: ', num2str(currN), ...
                    '\n Sigma: ', num2str(sigma), ...
                    '.\n Percentage until max time: ', num2str(i/currN), ...
                    '.\n Current not blown up: ', num2str(sum(notExploded(:)))]);
                fprintf('.\n-------\n');
                i = i+1;
            end
        end
    end
    blowupTimes(m,:,:,:,:) = internalBlowupTimes;
    parforSave(['Data\blowupTime' num2str(m) '.mat'],internalBlowupTimes)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and process text files - H1 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for m = 1:batchSize
%     for n = 1:numN
%         for sigma = 1:numSigma
%             H1samples{m,n,sigma} = importdata(['TEST_H1_sample_' num2str(m) '_N_' num2str(n) '_sigma_' num2str(sigma) '.txt']);
%         end
%     end
% end
% 
% tVec = cell(numN,1);
% for i = 1:numN
%     tVec{i} = linspace(0,T,NVec(i)+1);
% end
% %% Observe what samples blow up at what time
% ratioBlowUpTimes = zeros(batchSize,numN,numSigma,4);
% limitBlowUpTimes = zeros(batchSize,numN,numSigma,4);
% for k = 1:batchSize
%     for n = 1:numN
%         for j = 1:numSigma
%             tempRatio = H1samples{k,n,j}(2:end,:)./H1samples{k,n,j}(1:end-1,:);
%             for i = 1:size(tempRatio,2)
%                 % Ratio blow up criteria
%                 index = find(tempRatio(:,i) > blowUpRatio,1);
%                 if index
%                     ratioBlowUpTimes(k,n,j,i) = tVec{n}(index);
%                 else
%                     % If the blowup hasn't happened before T, just set 2T
%                     % as a placeholder
%                     ratioBlowUpTimes(k,n,j,i) = 2*T;
%                 end
%                 
%                 % Limit blow up critera
%                 index = find(H1samples{k,n,j}(:,i) > blowUpLimit,1);
%                 if index
%                     limitBlowUpTimes(k,n,j,i) = tVec{n}(index);
%                 else
%                     % If the blowup hasn't happened before T, just set 2T
%                     % as a placeholder
%                     limitBlowUpTimes(k,n,j,i) = 2*T;
%                 end
%             end
%         end
%     end
% end
% % The elements which are still zero are the samples where there were no
% % refinement made. Add a placeholder here as well
% ratioBlowUpTimes(ratioBlowUpTimes==0) = 2*T;
% limitBlowUpTimes(limitBlowUpTimes==0) = 2*T;
% 
% numBins = 25;
% timeAxis = linspace(0,T,numBins);
% histID = {'Co','Fi','Lin','Fft'};
% timeID = {'0', 'T/2', 'T'};
% NID = cell(numN,1);
% sigmaID = cell(numSigma,1);
% for n = 1:numN
%     NID{n} = ['$N = 2^{' num2str(log2(NVec(n))) '}$'];
% end
% for j = 1:numSigma
%     sigmaID{j} = ['$\sigma = ' num2str(sigmaVec(j)) '$'];
% end
% 
% %% Plot and print multi-histograms
% vertHistPlotMult(ratioBlowUpTimes,histID,timeAxis,NID,sigmaID)
% printToPDF(gcf,'critExpHistRatio')
% vertHistPlotMult(limitBlowUpTimes,histID,timeAxis,NID,sigmaID)
% printToPDF(gcf,'critExpHistLimit')
% %% Count some processes
% % The number of processes which do not blow up according to the ratio
% % Coarse
% sum(ratioBlowUpTimes(:,:,:,1)==2*T,1)
% % Fine
% sum(ratioBlowUpTimes(:,:,:,2)==2*T,1)
% % Lin
% sum(ratioBlowUpTimes(:,:,:,3)==2*T,1)
% % Fft
% sum(ratioBlowUpTimes(:,:,:,4)==2*T,1)
% % The number of processes which meet the ratio critera before the limit
% sum(ratioBlowUpTimes(:) < limitBlowUpTimes(:))
% % Check how fine vs linear interpolation blow up times vary
% max(abs(ratioBlowUpTimes(:,:,:,2)-ratioBlowUpTimes(:,:,:,3))/T)
% % Check how fine vs Fft interpolation blow up times vary
% max(abs(ratioBlowUpTimes(:,:,:,2)-ratioBlowUpTimes(:,:,:,4))/T)