addpath('Functions')
%% Set initial info and function
initSeed = 1;
batchSize = 10^4;
confLevel = 0.95;
% Time and area
L = 10; XInt = [-L,L];
T = 1/4; TInt = [0,T];

% Numerical precision, number of points
refN = 2^15; % Time
refM = 2^7; % Space

u0Fun = @(x) exp(-3*x.^2);
per = true;
C = 50;

sigma = 1;

refModel = initModelInfoScaling(refN,TInt,refM,XInt,sigma,per,C);
% refModel = initModelInfo(refN,TInt,refM,XInt,sigma,per);

%% Query definitions
% The different weak errors we will compare, currU and refU will be in Fourier space
weakErrorQuery = cell(0,0);
weakErrorQueryNames = cell(0,0);
weakErrorQueryFileNames = cell(0,0);
% L2-norm of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(real(ifft(currU)),dx,per);
weakErrorQueryNames{end+1} = '$e_1(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrRealPartL2';

% L2-norm of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(exp(-(ifft(currU)).^2),dx,per);
weakErrorQueryNames{end+1} = '$e_2(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussL2';

% L2-norm of sin of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(sin(real(ifft(currU))),dx,per);
weakErrorQueryNames{end+1} = '$e_3(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSinRealPartL2';

% Energy
weakErrorQuery{end+1} = @(currU,dx,k) energyShroed(currU,dx,k,per,sigma);
weakErrorQueryNames{end+1} = '$e_4(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrEnergy';

% H1 norm
weakErrorQuery{end+1} = @(currU,dx,k) H1norm(currU,dx,k,per);
weakErrorQueryNames{end+1} = '$e_5(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrH1';

% Odd stuff
weakErrorQuery{end+1} = @(currU,dx,k) abs(cos(abs(currU(5))));
weakErrorQueryNames{end+1} = '$e_6(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrOdd';

numWeaks = length(weakErrorQuery);
%% Query storage
% If query storage exists, load it. Otherwise, create it
if isfile('weakErrorReferenceLINEAR.mat')
    load('weakErrorReferenceLINEAR.mat');
    % If the size is smaller than the batch size, resize storage
    % This assumes that no new N, schemes or queries have been added
    numStoredElements = size(refBatch,1);
    if(numStoredElements < batchSize)
        temp = zeros(batchSize,numWeaks);
        temp(1:numStoredElements,:) = refBatch;
        refBatch = temp;
    end
else
    refBatch = zeros(batchSize,numWeaks);
end

%% Perform calculations
rng(initSeed,'twister')

% No simulation necessary for the linear equation, only one calculation
% necessary
parfor m = 1:batchSize
    % Only perform the simulation no estimate has been made earlier
    runSim = all(refBatch(m,:) == 0);
    if runSim
        % Load the broadcast variables to internal
        internalWeakErrorQuery = weakErrorQuery;
        internalRefModel = refModel;

        % Calculate reference solution
        refW = randn(refN,2)*sqrt(internalRefModel.h/2);
        currU = fft(u0Fun(internalRefModel.x));
        for i = 1:refN
            dW = refW(i,:);
            % Using Lie-Trotter splitting scheme as reference solution
            currU = internalRefModel.schemes.fun{7}(currU,dW);
        end
        tempRefBatch = zeros(numWeaks,1);
        for i = 1:numWeaks
             tempRefBatch(i) = internalWeakErrorQuery{i}(currU,internalRefModel.dx,internalRefModel.k);
        end
        refBatch(m,:) = tempRefBatch;
        m
    end
end

%% Save the data
save('weakErrorReferenceLINEAR.mat','refBatch','-v7.3')

%% Calculate the CI - Monte Carlo estimation
numMeans = 10^2;
numMeansInMean = batchSize/numMeans;
refMeanMat = zeros(numMeans,numWeaks);

perc = abs(norminv((1-confLevel)/2,0,1));

for i = 1:numMeans
    for j = 1:numWeaks
        refMeanMat(i,j) = mean(refBatch((i-1)*numMeansInMean+(1:numMeansInMean),j));
    end
end

meanEst = mean(refMeanMat);
stdEst = std(refMeanMat);

CIEst = zeros(numWeaks,2);
CIEst(:,1) = meanEst - perc*stdEst/sqrt(numMeans);
CIEst(:,2) = meanEst + perc*stdEst/sqrt(numMeans);

CIEst
perc*stdEst/sqrt(numMeans)