% Check convergence of integral methods for different functions
numIntegralMethods = 2*2;
% Normal convergence, or L2-equivalent?
L2Eq = true;

f = cell(0,0);
g = cell(0,0); % Derivative
f{end+1} = @(x) cos(x);
    g{end+1} = @(x) -sin(x);
f{end+1} = @(x) exp(-x.^2);
    g{end+1} = @(x) -2*x.*exp(-x.^2);
% f{end+1} = @(x) exp(x); % Not periodic (req. for H1)
%     g{end+1} = @(x) exp(x);
f{end+1} = @(x) 1./(x.^4+1);
    g{end+1} = @(x) -4*x.^3./(x.^4+1).^2;
% f{end+1} = @(x) exp(1./(abs(x)+1)); % Function not in H1
%     g{end+1} = @(x) -x.^(abs(x)+1)*sqrt(exp(1))./(abs(x).*(abs(x)+1).^2);
% f{end+1} = @(x) exp(x+1i*x); % Not periodic (req. for H1)
%     g{end+1} = @(x) (1+1i)*exp(x+1i*x);
f{end+1} = @(x) absSq(exp(-x.^2));
    g{end+1} = @(x) -4*exp(-2*x.^2).*x;
f{end+1} = @(x) 1./(2+sin(x).^2);
    g{end+1} = @(x) -2*sin(x).*cos(x)./(2+sin(x).^2).^2;
numFun = length(f);


MVector = 2.^(4:11);
numM = length(MVector);

a = -5;
b = 5;

xVecs = cell(numM,1);
diffs = zeros(numIntegralMethods,numM,numFun);
for m = 1:numM
    M = MVector(m);
    dx = (b-a)/M;
    x = a + dx*(0:M-1);
    k = 2*pi/(b-a)*[0:M/2-1, 0, -M/2+1:-1];
    
    for i = 1:numFun
        y = f{i}(x);
        u = fft(y);
        if L2Eq
            trapVal = trapezoidalIntegral(dx,absSq(y));
            simpVal = simpsonIntegral(dx,absSq(y));
            refVal = integral(@(x) absSq(f{i}(x)),a,b);
        else
            trapVal = trapezoidalIntegral(dx,y);
            simpVal = simpsonIntegral(dx,y);
            refVal = integral(f{i},a,b);
        end
        
        trapH1 = trapezoidalIntegral(dx,absSq(y)) + trapezoidalIntegral(dx,absSq(ifft(1i*k.*u)));
        simpH1 = simpsonIntegral(dx,absSq(y)) + simpsonIntegral(dx,absSq(ifft(1i*k.*u)));
        refH1 = integral(@(x) absSq(f{i}(x)) + absSq(g{i}(x)),a,b);
        
        % Simple function or L2
        diffs(1,m,i) = abs(simpVal-refVal);
        diffs(2,m,i) = abs(trapVal-refVal);
        % H1 norm
        diffs(3,m,i) = abs(simpH1-refH1);
        diffs(4,m,i) = abs(trapH1-refH1);
    end
end
%%
for i = 1:numIntegralMethods
    figure(i)
    loglog(MVector,squeeze(diffs(i,:,:)))
    legend(num2str((1:numFun)'))
end