addpath('Functions')
%% Set initial info and function
initSeed = 3;
% Time and area
L = 10; XInt = [-L,L];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^10; % Time
M = 2^13; % Space
sigma = 1;

u0Fun = @(x) exp(-3*x.^2);
per = true;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
u0FunVal = u0Fun(modelInfo.x);

%% Perform calculations
rng(initSeed,'twister')
W = randn(N,2)*sqrt(modelInfo.h/2);

currU = fft(u0FunVal);
currUDet = currU;

frameRate = 40;

figure(1)
set(gcf,'units','normalized','outerposition',[0 0 1 1])
framesRealSpace(N) = struct('cdata',[],'colormap',[]);
movieTitleReal = 'Plots/presentationReal.avi';
writerObjReal = VideoWriter(movieTitleReal);
writerObjReal.FrameRate = frameRate;
% open(writerObjReal);

figure(2)
set(gcf,'units','normalized','outerposition',[0 0 1 1])
framesFourSpace(N) = struct('cdata',[],'colormap',[]);
movieTitleFour = 'Plots/presentationFourier.avi';
writerObjFour = VideoWriter(movieTitleFour);
writerObjFour.FrameRate = frameRate;
% open(writerObjFour);

figure(3)
set(gcf,'units','normalized','outerposition',[0 0 1 1])
framesRealSpaceDet(N) = struct('cdata',[],'colormap',[]);
movieTitleRealDet = 'Plots/presentationRealDet.avi';
writerObjRealDet = VideoWriter(movieTitleRealDet);
writerObjRealDet.FrameRate = frameRate;
% open(writerObjReal);

figure(4)
set(gcf,'units','normalized','outerposition',[0 0 1 1])
framesFourSpaceDet(N) = struct('cdata',[],'colormap',[]);
movieTitleFourDet = 'Plots/presentationFourierDet.avi';
writerObjFourDet = VideoWriter(movieTitleFourDet);
writerObjFourDet.FrameRate = frameRate;
% open(writerObjFour);

for i = 1:N
    dW = W(i,:);
    dt = modelInfo.h/2*ones(2,1);
    % LTSpl stepping
    currU = modelInfo.schemes.fun{7}(currU,dW);
    currUDet = modelInfo.schemes.fun{7}(currUDet,dt);
    tempCurrU = ifft(currU);
    tempCurrUDet = ifft(currUDet);
    
    figure(1)
    plotProfile(modelInfo.x,tempCurrU,false,false)
    ylim([-1 1])
    title(['t = ' num2str(i/N)])
    xlabel('$x$', 'Interpreter','latex')
    ylabel('$u(x,t)$', 'Interpreter','latex')
    set(gca,'FontSize',35)
    drawnow
    pause(0.05)
    framesRealSpace(i) = getframe(gcf);
%     pause(0.05)
%     writeVideo(writerObjReal, framesRealSpace(i));
    
    figure(2)
    plotProfile(modelInfo.k,currU/max(abs(currU)),true,true)
    ylim([-1 1])
    xlim([-15 15])
    title(['t = ' num2str(i/N)])
    xlabel('$k$', 'Interpreter','latex')
    ylabel('Normed $u^k(t)$', 'Interpreter','latex')
    set(gca,'FontSize',35)
    drawnow
    pause(0.05)
    framesFourSpace(i) = getframe(gcf);
%     pause(0.05)
%     writeVideo(writerObjFour, framesFourierSpace(i));

    figure(3)
    plotProfile(modelInfo.x,tempCurrUDet,false,false)
    ylim([-1 1])
    title(['t = ' num2str(i/N)])
    xlabel('$x$', 'Interpreter','latex')
    ylabel('$u(x,t)$', 'Interpreter','latex')
    set(gca,'FontSize',35)
    drawnow
    pause(0.05)
    framesRealSpaceDet(i) = getframe(gcf);
%     pause(0.05)
%     writeVideo(writerObjRealDet, framesRealSpace(i));

    figure(4)
    plotProfile(modelInfo.k,currUDet/max(abs(currUDet)),true,true)
    ylim([-1 1])
    xlim([-15 15])
    title(['t = ' num2str(i/N)])
    xlabel('$k$', 'Interpreter','latex')
    ylabel('Normed $u^k(t)$', 'Interpreter','latex')
    set(gca,'FontSize',35)
    drawnow
    pause(0.05)
    framesFourSpaceDet(i) = getframe(gcf);
%     pause(0.05)
%     writeVideo(writerObjFourDet, framesFourierSpace(i));
end

%% Write to video
% Stochastic realspace
open(writerObjReal);
% write the frames to the video
for i=1:N
    % convert the image to a frame
    frame = framesRealSpace(i) ;    
    writeVideo(writerObjReal, frame);
end
% close the writer object
close(writerObjReal);

% Stochastic Fourier space
open(writerObjFour);
% write the frames to the video
for i=1:N
    % convert the image to a frame
    frame = framesFourSpace(i) ;    
    writeVideo(writerObjFour, frame);
end
% close the writer object
close(writerObjFour);

% Deterministic realspace
open(writerObjRealDet);
% write the frames to the video
for i=1:N
    % convert the image to a frame
    frame = framesRealSpaceDet(i) ;    
    writeVideo(writerObjRealDet, frame);
end
% close the writer object
close(writerObjRealDet);

% Deterministic Fourier space
open(writerObjFourDet);
% write the frames to the video
for i=1:N
    % convert the image to a frame
    frame = framesFourSpaceDet(i) ;    
    writeVideo(writerObjFourDet, frame);
end
% close the writer object
close(writerObjFourDet);