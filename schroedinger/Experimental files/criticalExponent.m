%% Set initial info and function
addpath('Data')
initSeed = 4;
batchSize = 8;
% Time and area
L = 5*pi; XInt = [-L,L];
T = 10^(-7); TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^16; % Time
M = 2^14; % Space
refM = 2*M; % Space
% N = 2^17; % Time
% M = 2^15; % Space
% refM = 2*M; % Space
NVec = [N, 2*N, 4*N];
sigmaVec = [3.9, 4, 4.1];
per = true;

u0Fun = @(x) 10*exp(-10*x.^2);
treshhold = 10000;
% Make two thresholds ratios
%% Query storage
numSigma = length(sigmaVec);
numN = length(NVec);
H1Storage = cell(batchSize,numN,numSigma);

%% Perform calculations
rng(initSeed,'twister')
parfor m = 1:batchSize
    maxN = max(NVec);
    max(sigmaVec);
    dW = randn(2,maxN)*sqrt(T/maxN);
    NH1Vector = cell(numN,numSigma);
    % Vary N and sigma
    for n = 1:numN
        for j = 1:numSigma
            N = NVec(n);
            sigma = sigmaVec(j);
            % If we are not using the finest time discretization, calculate the
            % coarser Brownian motion increments
            if N < maxN
                scalingFactor = maxN/N;
                coarseDW = zeros(2,N);
                currIndex = 0;
                for i = 1:N
                    indexList = (currIndex+1):(currIndex+scalingFactor/2);
                    coarseDW(1,i) = sum(sum(dW(:,indexList)));
                    indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
                    coarseDW(2,i) = sum(sum(dW(:,indexList)));
                    currIndex = currIndex + scalingFactor;
                end
            else
                coarseDW = dW;
            end
            modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
            fineModelInfo = initModelInfo(N,TInt,2*M,XInt,sigma,per);

            currU = gpuArray(fft(u0Fun(modelInfo.x)));
            currUFine = gpuArray(fft(u0Fun(fineModelInfo.x)));
                % Initialization necessary for parfor
                linIntU = currU;
                fftIntU = currU;
            % Norm storage
            H1Val = zeros(N+1,1);
                H1Val(1) = gather(H1norm(currU,modelInfo.dx,modelInfo.k,per));
            H1FineVal = zeros(N+1,1);
                H1FineVal(1) = gather(H1norm(currUFine,fineModelInfo.dx,fineModelInfo.k,per));

            % Simulate spatially coarse fine, and refined processes
            % Refine spatially once, if H1 > treshhold at some point
            refined = false;
            for i = 1:N
                currDW = coarseDW(:,i);
                % Progress and calculate norms
                currU = modelInfo.schemes.fun{7}(currU,currDW);
                currUFine = fineModelInfo.schemes.fun{7}(currUFine,currDW);
                H1Val(i+1) = gather(H1norm(currU,modelInfo.dx,modelInfo.k,per));
                H1FineVal(i+1) = gather(H1norm(currUFine,fineModelInfo.dx,fineModelInfo.k,per));

                if refined
                    % Progress
                    linIntU = fineModelInfo.schemes.fun{7}(linIntU,currDW);
                    fftIntU = fineModelInfo.schemes.fun{7}(fftIntU,currDW);
                    % Calculate norms
                    H1LinVal(i+1) = gather(H1norm(linIntU,fineModelInfo.dx,fineModelInfo.k,per));
                    H1FftVal(i+1) = gather(H1norm(fftIntU,fineModelInfo.dx,fineModelInfo.k,per));
                end

                % Perform spatial refinement, if necessary
                if H1Val(i+1) > treshhold && ~refined
                    tempCurrU = ifft(currU);
                    linIntU = fft(refineU(tempCurrU,per,'Linear',modelInfo.x,fineModelInfo.x));
                    fftIntU = fft(refineU(tempCurrU,per,'Fft',modelInfo.x,fineModelInfo.x));

                    H1LinVal = H1Val;
                    H1FftVal = H1Val;
                    refined = true;
                end

                fprintf(['-------'...
                    '\n Sample: ', num2str(m), ...
                    '\n N: ', num2str(N), ...
                    '\n Sigma: ', num2str(sigma), ...
                    '.\n Percentage until max time: ', num2str(i/N), ...
                    '.\n Current H1 value: ',...
                    '\n', num2str(H1Val(i+1)),...
                    '\n', num2str(H1FineVal(i+1))]);
                if refined
                    fprintf(['\n', num2str(H1LinVal(i+1)),...
                    '\n', num2str(H1FftVal(i+1))]);
                end
                     fprintf('.\n-------\n');
            end
            % Save the norm values
            if ~refined
                H1Vector = zeros(N+1,2);
                H1Vector(:,1) = H1Val;
                H1Vector(:,2) = H1FineVal;
            else
                H1Vector = zeros(N+1,4);
                H1Vector(:,1) = H1Val;
                H1Vector(:,2) = H1FineVal;
                H1Vector(:,3) = H1LinVal;
                H1Vector(:,4) = H1FftVal;
            end
            NH1Vector{n,j} = H1Vector;
        end
    end
    H1Storage(m,:,:) = NH1Vector;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and process text files - H1 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tVec = cell(numN,1);
for i = 1:numN
    tVec{i} = linspace(0,T,NVec(i)+1);
end

for k = 1:batchSize
    figure(k)
    currSubplot = 1;
    for n = 1:numN
        for j = 1:numSigma
            subplot(numN,numSigma,currSubplot)
            currSubplot = currSubplot + 1;
            plot(tVec{n},H1Storage{k,n,j})
            title(['$N = ' num2str(NVec(n)) '$. $\sigma = ' num2str(sigmaVec(j)) '$.'], 'interpreter','latex')
            set(gca,'yscale','log');
        end
    end
    set(gcf, 'Position', get(0, 'Screensize'));
    pause(2)
end
%% Observe what samples blow up at what time
blowUpRatio = 1.4;
blowUpTimes = zeros(batchSize,numN,numSigma,4);
for k = 1:batchSize
    for n = 1:numN
        for j = 1:numSigma
            tempRatio = H1Storage{k,n,j}(2:end,:)./H1Storage{k,n,j}(1:end-1,:);
            for i = 1:size(tempRatio,2)
                index = find(tempRatio(:,i) > blowUpRatio,1);
                if index
                    blowUpTimes(k,n,j,i) = tVec{n}(index);
                else
                    blowUpTimes(k,n,j,i) = T;
                end
            end
        end
    end
end

numBins = 25;
timeAxis = linspace(0,T,numBins);
histID = {'Co','Fi','Lin','Fft'};
timeID = {'0', 'T/2', 'T'};
NID = cell(numN,1);
sigmaID = cell(numSigma,1);
for n = 1:numN
    NID{n} = ['$N = 2^{' num2str(log2(NVec(n))) '}$'];
end
for j = 1:numSigma
    sigmaID{j} = ['$\sigma = ' num2str(sigmaVec(j)) '$'];
end

vertHistPlotMult(blowUpTimes,histID,timeAxis,NID,sigmaID)
% printToPDF(gcf,'criticalExponentHighPrec')