The files in this folder will be severely less:
Organized - At most, they will have some subfolders describing different types of experiments
Updated - When an experiment is done, any trailing experimental files will be abandoned
Stable - This follows from the previous point and from the lack of polish each file will have
Relevant - They may test some very small aspect which will not be revealed by the rest of the content

Odds are, you won't gain much from looking at these.