%% Set initial info and function
initSeed = 4;
batchSize = 100;
% Time and area
L = 5*pi; XInt = [-L,L];
% T = 10^(-3); TInt = [0,T];
T = 1; TInt = [0,T];

% Numerical precision, number of points
% N = 2^19; % Time
% M = 2^18; % Space
N = 2^12; % Time
M = 2^17; % Space
refM = 2*M; % Space
per = true;
sigma = 4;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
refModelInfo = initModelInfo(N,TInt,refM,XInt,sigma,per);
%%
% u0Fun = @(x) 1./(2+sin(x).^2);
u0Fun = @(x) 1.4*exp(-4*x.^2);

% Coarse
u0FunVal = u0Fun(modelInfo.x);
% Norming the initial value
u0L2Val = L2norm(u0FunVal,modelInfo.dx,per);
u0FunVal = u0FunVal/sqrt(u0L2Val);
u0L2Val = L2norm(u0FunVal,modelInfo.dx,per);
u0H1Val = H1norm(fft(u0FunVal),modelInfo.dx,modelInfo.k,per);

% Fine
fineU0FunVal = u0Fun(refModelInfo.x);
% Norming the initial value
refU0L2Val = L2norm(fineU0FunVal,refModelInfo.dx,per);
fineU0FunVal = fineU0FunVal/sqrt(refU0L2Val);
refU0L2Val = L2norm(fineU0FunVal,refModelInfo.dx,per);
refU0H1Val = H1norm(fft(fineU0FunVal),refModelInfo.dx,refModelInfo.k,per);


numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MEul
% schemesUsed(4) = true; % CN
% schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Query storage
% File names


%% Perform calculations
rng(initSeed,'twister')
test = tic;
parfor m = 1:batchSize
    worker = getCurrentWorker;
    
    internalSchemeIndexMat = schemeIndexMat;
    internalModelInfo = modelInfo;
    internalRefModelInfo = refModelInfo;
    for j = 1:numUsedSchemes
        coarseU = fft(u0FunVal);
        fineU = fft(fineU0FunVal);
        
        % Necessary for parfor code reading
        linIntU = fineU;
        custIntU = fineU;
        
        currScheme = internalSchemeIndexMat(j,2);
        t = 0;

        W = randn(2,N)*sqrt(internalModelInfo.h);
        refined = false;
        
        % Storage vectors
        L2Vec = zeros(4,N+1);
            L2Vec(1,1) = u0L2Val;
            L2Vec(2,1) = refU0L2Val;
            L2Vec(3,1) = u0L2Val;
            L2Vec(4,1) = u0L2Val;
            
        H1Vec = zeros(4,N+1);
            H1Vec(1,1) = u0H1Val;
            H1Vec(2,1) = refU0H1Val;
            H1Vec(3,1) = u0H1Val;
            H1Vec(4,1) = u0H1Val;
            
        for i = 1:N
            dW = W(:,i);
            %% Scheme and query calculations
            if ~refined
                % The fine and coarse
                coarseU = internalModelInfo.schemes.fun{currScheme}(coarseU,dW);
                fineU = internalRefModelInfo.schemes.fun{currScheme}(fineU,dW);
                
                % Store values
                tempCoarse = ifft(coarseU);
                tempFine = ifft(fineU);
                    % L2
                    L2Vec(1,i+1) = L2norm(tempCoarse,internalModelInfo.dx,per);
                    L2Vec(2,i+1) = L2norm(tempFine,internalRefModelInfo.dx,per);
                    L2Vec(3,i+1) = L2Vec(1,i+1);
                    L2Vec(4,i+1) = L2Vec(1,i+1);
                    % H1
                    H1Vec(1,i+1) = H1norm(coarseU,internalModelInfo.dx,internalModelInfo.k,per);
                    H1Vec(2,i+1) = H1norm(fineU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
                    H1Vec(3,i+1) = H1Vec(1,i+1);
                    H1Vec(4,i+1) = H1Vec(1,i+1);
            else
                % The fine and coarse
                coarseU = internalModelInfo.schemes.fun{currScheme}(coarseU,dW);
                fineU = internalRefModelInfo.schemes.fun{currScheme}(fineU,dW);
                % The refined
                linIntU = internalRefModelInfo.schemes.fun{currScheme}(linIntU,dW);
                custIntU = internalRefModelInfo.schemes.fun{currScheme}(custIntU,dW);
                
                % Store values
                tempCoarse = ifft(coarseU);
                tempFine = ifft(fineU);
                tempLin = ifft(linIntU);
                tempCust = ifft(custIntU);
                    % L2
                    L2Vec(1,i+1) = L2norm(tempCoarse,internalModelInfo.dx,per);
                    L2Vec(2,i+1) = L2norm(tempFine,internalRefModelInfo.dx,per);
                    L2Vec(3,i+1) = L2norm(tempLin,internalRefModelInfo.dx,per);
                    L2Vec(4,i+1) = L2norm(tempCust,internalRefModelInfo.dx,per);
                    % H1
                    H1Vec(1,i+1) = H1norm(coarseU,internalModelInfo.dx,internalModelInfo.k,per);
                    H1Vec(2,i+1) = H1norm(fineU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
                    H1Vec(3,i+1) = H1norm(linIntU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
                    H1Vec(4,i+1) = H1norm(custIntU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
            end
            
            % Refine using linear and custom interpolation
            if i == N/2 && ~refined
                linIntU = fft(refineU(tempCoarse,per,'Linear',internalModelInfo.x,internalRefModelInfo.x));
                custIntU = fft(refineU(tempCoarse,per,'Custom',internalModelInfo.x,internalRefModelInfo.x));
                refined = true;
            end
            
%             H1Val = H1norm(coarseU,internalModelInfo.dx,internalModelInfo.k,per);

            t = t + modelInfo.h;
            fprintf(['-------'...
                '\n Sample: ', num2str(m), ...
                '.\n Scheme: ', num2str(j), ...
                '.\n Percentage until max time: ', num2str(t/T), ...
                '.\n Current H1 value: ',...
                '\n', num2str(H1Vec(1,i+1)),...
                '\n', num2str(H1Vec(2,i+1)),...
                '\n', num2str(H1Vec(3,i+1)),...
                '\n', num2str(H1Vec(4,i+1)),...
                '.\n-------\n'])
        end
    end
    printToText(['L2_' num2str(m) '.txt'],L2Vec);
    printToText(['H1_' num2str(m) '.txt'],H1Vec);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and process text files - H1 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract max H1 value and H1 differences
H1Max = zeros(batchSize,4);
H1Diff = zeros(batchSize,6);
for i = 1:batchSize
    fileName = ['H1_' num2str(i) '.txt'];
    temp = importdata(fileName);
    
    currDiff = 1;
    for j = 1:4
        H1Max(i,j) = max(temp(j,:));
        for k = 2:4
            % No need for diagonal or mirrored differences
            if k > j
                H1Diff(i,currDiff) = max(log10(abs(temp(j,:) - temp(k,:))));
                
                currDiff = currDiff + 1;
            end
        end
    end
end

%% Vertical histogram plot - Max H1 value
% Histogram legend
histLeg = {'Coarse','Fine','Lin. int','Cust. int'};

% Set y axis info and plot the information in form of histrograms
numHist = 4;
histWidh = 0.774;
schemePlotSpec = [0.1305,histWidh/numHist,0.11,0.815];

yMax = max(H1Max(:));
yMin = min(H1Max(:));

% Set the y ticks and labels
numBins = 30;
yAxisVector = linspace(yMin,yMax,numBins);

sideTitle = '$max_{n\in\{1,2,\ldots,N\}}||u_n||_{H^1}^2$';
vertHistPlot(H1Max,histLeg,yAxisVector,sideTitle,schemePlotSpec)
set(gcf, 'Position', get(0, 'Screensize'));
pause(1)

%% Vertical histogram plot - Max H1 difference
% Histogram legend
histLeg = {'Co-Fi','Co-Li','Co-Cu','Fi-Li','Fi-Cu','Li-Cu'};

% Set y axis info and plot the information in form of histrograms
numHist = 6;
histWidh = 0.774;
schemePlotSpec = [0.1305,histWidh/numHist,0.11,0.815];

yMax = ceil(max(H1Diff(:)));
yMin = floor(min(H1Diff(:)));

if yMin < -17
    yMin = -17;
end
if yMax > 2
    yMax = 2;
end

% Set the y ticks and labels
numBins = 30;
yAxisVector = linspace(yMin,yMax,numBins);

sideTitle = '$max_{n\in\{1,2,\ldots,N\}}\left(log(| ~ ||u_n^i||_{H^1}^2-||u_n^j||_{H^1}^2 ~ |)\right)$';
vertHistPlot(H1Diff,histLeg,yAxisVector,sideTitle,schemePlotSpec)
set(gcf, 'Position', get(0, 'Screensize'));
pause(1)
% printToPDF(gcf,'H1Explosion')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and process text files - L2 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract max L2 differences
L2MaxDiff = zeros(batchSize,4);
for i = 1:batchSize
    fileName = ['L2_' num2str(i) '.txt'];
    temp = importdata(fileName);
    
    for j = 1:4
        L2MaxDiff(i,j) = max(log10(abs(temp(j,:)-temp(j,1))));
    end
end

%% Vertical histogram plot - Max L2 drift
% Histogram legend
histLeg = {'Coarse','Fine','Lin. int','Cust. int'};

% Set y axis info and plot the information in form of histrograms
numHist = 4;
histWidh = 0.774;
schemePlotSpec = [0.1305,histWidh/numHist,0.11,0.815];

yMax = max(L2MaxDiff(:));
yMin = min(L2MaxDiff(:));

if yMin < -17
    yMin = -17;
end
if yMax > 2
    yMax = 2;
end

% Set the y ticks and labels
numBins = 30;
yAxisVector = linspace(yMin,yMax,numBins);

sideTitle = '$max_{n\in\{1,2,\ldots,N\}}\left(log(| ~ ||u_n||_{L^2}^2-||u_0||_{L^2}^2 ~ |)\right)$';
vertHistPlot(L2MaxDiff,histLeg,yAxisVector,sideTitle,schemePlotSpec)
set(gcf, 'Position', get(0, 'Screensize'));
pause(1)

% printToPDF(gcf,'H1Explosion')