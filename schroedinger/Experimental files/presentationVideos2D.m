addpath('Functions')
%% Set initial info and function
initSeed = 3;
% Time and area
L1 = 5*pi; 
L2 = 5*pi; 
XInt = [-L1,L1,-L2,L2];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^10; % Time
M = 2^8; % Space
sigma = 1;

h = T/N;
u0Fun = @(x,y) exp(-3*(x.^2+y.^2)) + exp(-3*((x-pi).^2+(y+pi).^2));
per = true;

k1 = 2*pi/(XInt(2)-XInt(1))*[0:M/2-1, 0, -M/2+1:-1];
k2 = 2*pi/(XInt(4)-XInt(3))*[0:M/2-1, 0, -M/2+1:-1];
[k1Grid,k2Grid] = meshgrid(k1,k2);
kSq = k1Grid.^2 + k2Grid.^2;

dx = (XInt(2)-XInt(1))/M;
dy = (XInt(4)-XInt(3))/M;
x = XInt(1) + dx*(0:M-1);
y = XInt(3) + dy*(0:M-1);

[x1Grid,x2Grid] = meshgrid(x,y);
u0FunVal = u0Fun(x1Grid,x2Grid);

schemes = makePSSchroedSchemes2D(kSq,h,sigma);

%% Perform calculations
rng(initSeed,'twister')
W = randn(N,2)*sqrt(h/2);

currU = fft2(u0FunVal);

figure(1)
set(gcf,'units','normalized','outerposition',[0 0 1 1])
framesRealSpace(N) = struct('cdata',[],'colormap',[]);
movieTitleReal2D = 'Plots/presentation2D.avi';
writerObjReal2D = VideoWriter(movieTitleReal2D);
writerObjReal2D.FrameRate = 40;

for i = 1:N
    dW = W(i,:);
    % LTSpl stepping
    currU = schemes.fun{7}(currU,dW);
    tempCurrU = ifft2(currU);
    
    figure(1)
    surf(x,y,abs(tempCurrU))
    zlim([0 1.5])
    title(['t = ' num2str(i/N)])
    xlabel('$x$', 'Interpreter','latex')
    ylabel('$y$', 'Interpreter','latex')
    zlabel('$|u(x,y,t)|$','Interpreter','latex')
    set(gca,'FontSize',35)
    drawnow
    pause(0.05)
    framesRealSpace(i) = getframe(gcf);
    pause(0.05)
end

%%
open(writerObjReal2D);
% write the frames to the video
for i = 1:N
    % convert the image to a frame
    frame = framesRealSpace(i) ;    
    writeVideo(writerObjReal2D,frame);
end
% close the writer object
close(writerObjReal2D);