batchSize = 10^4;
batchNum = 10^4;
confLevel = 0.95;
alpha = 1-confLevel;
perc = abs(norminv((1-confLevel)/2,0,1));

totSim = exprnd(5,batchSize,batchNum);

simpleMeanEst = mean(totSim(:));
simpleStdEst = std(totSim(:));

monteMats = mean(totSim);
monteMeanEst = mean(monteMats);
monteStdEst = std(monteMats);

simpleCI = simpleMeanEst + [-1 1].*perc*simpleStdEst/sqrt(batchSize*batchNum)
monteCI = monteMeanEst + [-1 1].*perc*monteStdEst/sqrt(batchNum)


bootStrapTotSim = totSim(:);
R = 10^4;
bootStrapEst = zeros(R,1);

for i = 1:R
    bootStrapEst(i) = mean(datasample(bootStrapTotSim,batchSize));
end

lowerPercentile = prctile(bootStrapEst,100*alpha/2);
upperPercentile = prctile(bootStrapEst,100*(1-alpha/2));
bootMean = mean(bootStrapEst);

bootStrapCI = [lowerPercentile upperPercentile]

bootStrapAltCI = [2*bootMean-upperPercentile 2*bootMean-lowerPercentile]