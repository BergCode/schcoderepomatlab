initSeed = 4;
batchSize = 100;
% Time and area
L = 5*pi; XInt = [-L,L];
T = 10^(-3); TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^19; % Time
M = 2^18; % Space
refM = 2*M; % Space
per = true;
sigma = 4;

t = linspace(0,T,N+1);

% Initial value and norming of IV
% u0FunVal = u0Fun(modelInfo.x);
% u0Fun = @(x) exp(-4*x.^2);
% u0FunVal = u0Fun(modelInfo.x);
% u0L2Val = L2norm(u0FunVal,modelInfo.dx,per);
% u0FunVal = u0FunVal/sqrt(u0L2Val);

H1 = cell(4,1);
L2 = cell(4,1);

H1{1} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\H1_2.txt');
H1{2} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\H1_4.txt');
H1{3} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\H1_8.txt');
H1{4} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\H1_16.txt');


L2{1} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\L2_2.txt');
L2{2} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\L2_4.txt');
L2{3} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\L2_8.txt');
L2{4} = importdata('C:\Users\anbe0211\Desktop\Doktorand\Doktorand - Data\blowUpRefinement\N19M18Normed\L2_16.txt');

%%
temp = zeros(4,4,size(L2{1},2));
for i = 1:4
    temp(i,:,:) = L2{i};
end
% True L2 value is 1, due to norming
L2MeanDiff = squeeze(mean(abs(temp-1)));
%%
figure(1)
plot(t,L2MeanDiff,'LineWidth',2.5)
ylabel('$| ~ ||\hat{u}_M||_{L^2}^2-||u(0)||_{L^2}^2 ~ |$','Interpreter','latex')
xlabel('t')
set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
set(gca,'yscale','log')
legend('Unrefined','Fine','Linear','Fft','Location','southwest')
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationMeanL2DriftComparison')

%%
figure(2)
clf
hold on
for i = 1:4
    % Coarse
    plot(t,H1{i}(1,:),'k','LineWidth',2.5)
    % Fine
    plot(t,H1{i}(2,:),'k','LineWidth',2.5)
    % Linear
    plot(t,H1{i}(3,:),'r','LineWidth',2.5)
    % Fft
    plot(t,H1{i}(4,:),'k','LineWidth',2.5)
end
hold off

ylabel('$||\hat{u}_M||_{H^1}^2$','Interpreter','latex')
xlabel('t')
set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
legend('Unrefined','Fine','Linear','Fft','Location','southwest')
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationH1SampleComparison')
%% Plot mean cumulative maximum H1 difference
% meanCumH1Diff = zeros(6,N+1);
% diffPair = [
%     1,2; % Coarse-Fine
%     1,3; % Coarse-Linear
%     1,4; % Coarse-Fft
%     2,3; % Fine-Linear
%     2,4; % Fine-Fft
%     3,4; % Linear-Fft
%     ];
% temp = zeros(4,N+1);
% 
% for j = 1:6
%     for i = 1:4
%         temp(i,:) = cummax(abs(H1{i}(diffPair(j,1),:)-H1{i}(diffPair(j,2),:)));
%     end
%     meanCumH1Diff(j,:) = mean(temp);
% end
%%
% figure(3)
% clf
% LegendInfo = {'Coarse-Fine','Coarse-Linear','Coarse-Fft','Fine-Linear','Fine-Fft','Linear-Fft'};
% lineColor = {'k','g','k','b','k','r'};
% hold on
% for i = 1:6
%     plot(t,meanCumH1Diff(i,:),lineColor{i},'LineWidth',2.5)
% end
% hold off
% 
% ylabel('$| ~ ||\hat{u}_M||_{H^1}^2-||\hat{u}_M||_{H^1}^2 ~ |$','Interpreter','latex')
% xlabel('t')
% set(gca,'FontSize',35)
% set(gca,'yscale','log')
% ylim([10^-17 10^-2]);
% 
% legend(LegendInfo,'Location','southwest')
% set(gcf,'units','normalized','outerposition',[0 0 1 1])