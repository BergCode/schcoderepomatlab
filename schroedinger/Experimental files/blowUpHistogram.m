%% Set initial info and function
initSeed = 4;
batchSize = 100;
% Time and area
L = 5*pi; XInt = [-L,L];
T = 4*10^(-5); TInt = [0,T];

% Numerical precision, number of points
N = 2^19; % Time
M = 2^20; % Space
per = true;
sigma = 4;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
%%
% u0Fun = @(x) 1./(2+sin(x).^2);
u0Fun = @(x) 4.5*exp(-4*x.^2);

u0FunVal = u0Fun(modelInfo.x);
% Norming the initial value
% u0FunVal = u0FunVal/sqrt(L2norm(u0FunVal,modelInfo.dx,per));
u0H1Val = H1norm(fft(u0FunVal),modelInfo.dx,modelInfo.k,per);

numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MEul
% schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Query storage
blowUpTimes = zeros(batchSize,numUsedSchemes);

%% Perform calculations
rng(initSeed,'twister')
test = tic;
parfor m = 1:batchSize
    blowUpTime = zeros(numUsedSchemes,1);
    internalSchemeIndexMat = schemeIndexMat;
    internalModelInfo = modelInfo;
    for j = 1:numUsedSchemes
        currU = fft(u0FunVal);
        currScheme = internalSchemeIndexMat(j,2);
        t = 0;
        H1Val = u0H1Val;

        while H1Val < 1000 && t<T
            dW = randn(2,1)*sqrt(internalModelInfo.h);
            %% Scheme and query calculations
            currU = internalModelInfo.schemes.fun{currScheme}(currU,dW);
            
            H1Val = H1norm(currU,internalModelInfo.dx,internalModelInfo.k,per);

            t = t + modelInfo.h;
            fprintf(['-------'...
                '\n Sample: ', num2str(m), ...
                '.\n Scheme: ', num2str(j), ...
                '.\n Percentage until max time: ', num2str(t/T), ...
                '.\n Current H1 value: ', num2str(H1Val),...
                '.\n-------\n'])
        end
        blowUpTime(j) = t;
    end
    blowUpTimes(m,:) = blowUpTime;
end
%% Vertical histogram plot
% Set y axis info and plot the information in form of histrograms
histWidh = 0.774;
schemePlotSpec = [0.1305,histWidh/numUsedSchemes,0.11,0.815];

yMax = ceil(max(blowUpTimes(:)));
yMin = floor(min(blowUpTimes(:)));

if yMin < -17
    yMin = -17;
end
if yMax > 2
    yMax = 2;
end

% Set the y ticks and labels
yAxisVector = yMin:0.2:yMax;

sideTitle = '$max_{n\in\{1,2,\ldots,N\}}\left(log(| ~ ||u_n||_{L^2}^2-||u_0||_{L^2}^2 ~ |)\right)$';
vertHistPlot(blowUpTimes,modelInfo.schemes.shortNames(schemeIndexMat(:,2)),yAxisVector,sideTitle,schemePlotSpec)
set(gcf, 'Position', get(0, 'Screensize'));
pause(1)
% printToPDF(gcf,'H1Explosion')