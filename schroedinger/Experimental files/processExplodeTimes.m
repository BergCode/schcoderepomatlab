batchSize = 200;

N = 2^17; % Time
M = 2^16; % Space
refM = 2*M; % Space
NVec = [N, 2*N, 4*N, 8*N];
sigmaVec = [3.9, 4, 4.1];
numN = length(NVec);
numSigma = length(sigmaVec);
T = 10^(-6);

% Check what samples are present
numSamples = 0;
for m = 1:batchSize
    if isfile(['Data\blowupTime' num2str(m) '.mat'])
        numSamples = numSamples + 1;
    end
end

blowupTimes = zeros(numSamples,2,4,numN,numSigma);
currIndex = 1;
for m = 1:batchSize
    if isfile(['Data\blowupTime' num2str(m) '.mat'])
        load(['Data\blowupTime' num2str(m) '.mat']);
        blowupTimes(currIndex,:,:,:,:) = variable;
        currIndex = currIndex + 1;
    end
end

%% Check if criteria 1 (H1 ratio) is later than criteria 2 (hard H1 limit)
temp = blowupTimes(:,1,:,:,:) - blowupTimes(:,2,:,:,:);
any(temp(:) < 0)
% For values 1.2 for ratio and 25*H1(u_0) the ratio is always fulfilled
% after

%% Check the maximum difference in time between coarsest and finest
% temp = fine - coarse
numBins = 30;

% Ratio
temp = (squeeze(blowupTimes(:,1,2,end,:) - blowupTimes(:,1,1,1,:)))/T*100;
% Norm by T
max(temp)

figure
set(gcf,'units','normalized','outerposition',[0 0 1/2 1])
set(gca,'FontSize',fontAndMarkerSize('Font','Full'))
for s = 1:numSigma
    subplot(numSigma,1,s)
    histogram(temp(:,s),numBins)
    tempMean = mean(temp(:,s));
    tempMax = max(temp(:,s));
    tempMin = min(temp(:,s));
    title(['$\sigma = ' num2str(sigmaVec(s)) ...
        '$. Mean ' num2str(tempMean) ...
        '. Max ' num2str(tempMax) ...
        '. Min ' num2str(tempMin) '.'],...
        'Interpreter','latex')
end
suptitle('Ratio blowup, coarsest-finest time difference, % of T')
pause(1)
printToPDF(gcf,'BlowupRatioDiff')

% Hard limit
temp = (squeeze(blowupTimes(:,2,2,end,:) - blowupTimes(:,2,1,1,:)))/T*100;
% Norm by T
max(temp)

figure
set(gcf,'units','normalized','outerposition',[0 0 1/2 1])
for s = 1:numSigma
    subplot(numSigma,1,s)
    histogram(temp(:,s),numBins)
    tempMean = mean(temp(:,s));
    tempMax = max(temp(:,s));
    tempMin = min(temp(:,s));
    title(['$\sigma = ' num2str(sigmaVec(s)) ...
        '$. Mean ' num2str(tempMean) ...
        '. Max ' num2str(tempMax) ...
        '. Min ' num2str(tempMin) '.'],...
        'Interpreter','latex')
end
suptitle('Hard limit blowup, coarsest-finest time difference, % of T')
pause(1)
printToPDF(gcf,'BlowupHardDiff')
% set(gca,'FontSize',fontAndMarkerSize('Font','Full'))

% Blowup time differs at most by
% sigma     3.9         4           4.1
% Ratio     1.25%       .65%        .08%
% Hard      2.8*10^-3%  3.5*10^-3%  1.1*10^-3%


%% Check the maximum ratio in blowup time between coarsest and finest
% temp = fine/coarse
numBins = 30;

% Ratio
temp = squeeze(blowupTimes(:,1,2,end,:) ./ blowupTimes(:,1,1,1,:));

figure
set(gcf,'units','normalized','outerposition',[0 0 1/2 1])
for s = 1:numSigma
    subplot(numSigma,1,s)
    histogram(temp(:,s),numBins)
    tempMean = mean(temp(:,s));
    tempMax = max(temp(:,s));
    tempMin = min(temp(:,s));
    title(['$\sigma = ' num2str(sigmaVec(s)) ...
        '$. Mean ' num2str(tempMean) ...
        '. Max ' num2str(tempMax) ...
        '. Min ' num2str(tempMin) '.'],...
        'Interpreter','latex')
end
suptitle('Ratio blowup, coarsest-finest time ratio, fine/coarse')
pause(1)
printToPDF(gcf,'BlowupRatioRatio')

% Hard limit
temp = squeeze(blowupTimes(:,2,2,end,:) ./ blowupTimes(:,2,1,1,:));

figure
set(gcf,'units','normalized','outerposition',[0 0 1/2 1])
for s = 1:numSigma
    subplot(numSigma,1,s)
    histogram(temp(:,s),numBins)
    tempMean = mean(temp(:,s));
    tempMax = max(temp(:,s));
    tempMin = min(temp(:,s));
    title(['$\sigma = ' num2str(sigmaVec(s)) ...
        '$. Mean ' num2str(tempMean) ...
        '. Max ' num2str(tempMax) ...
        '. Min ' num2str(tempMin) '.'],...
        'Interpreter','latex')
end
suptitle('Hard limit blowup, coarsest-finest time ratio, fine/coarse')
pause(1)
printToPDF(gcf,'BlowupHardRatio')


%% Ratio blowup histogram
temp = squeeze(blowupTimes(:,1,:,:,:));
% Swich order
temp = permute(temp,[1,3,4,2]);
% Top is dim 2
% Right is dim 3
% Schemes (in this case refinements) is dim 4
refNames = {'Co' 'Fi' 'Li' 'nF'};
numCols = 20;
timeVector = linspace(0,T,numCols);
topTitles = cell(numN,1);
for n = 1:numN
    topTitles{n} = ['$N = 2^{' num2str(log2(NVec(n))) '}$']; 
end
rightTitles = cell(numSigma,1);
for s = 1:numSigma
    rightTitles{s} = ['$\sigma = ' num2str(sigmaVec(s)) '$']; 
end

vertHistPlotMult(temp,refNames,timeVector,topTitles,rightTitles)
pause(1)
printToPDF(gcf,'BlowupRatioHist')

%% Hard limit blowup histogram
temp = squeeze(blowupTimes(:,2,:,:,:));
% Swich order
temp = permute(temp,[1,3,4,2]);
% Top is dim 2
% Right is dim 3
% Schemes (in this case refinements) is dim 4
refNames = {'Co' 'Fi' 'Li' 'nF'};
numCols = 20;
timeVector = linspace(0,T,numCols);
topTitles = cell(numN,1);
for n = 1:numN
    topTitles{n} = ['$N = 2^{' num2str(log2(NVec(n))) '}$']; 
end
rightTitles = cell(numSigma,1);
for s = 1:numSigma
    rightTitles{s} = ['$\sigma = ' num2str(sigmaVec(s)) '$']; 
end

vertHistPlotMult(temp,refNames,timeVector,topTitles,rightTitles)
pause(1)
printToPDF(gcf,'BlowupHardHist')