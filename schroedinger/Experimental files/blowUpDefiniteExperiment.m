%% Set initial info and function
addpath('Data')
addpath('Functions')
initSeed = 4;
batchSize = 200;
% Time and area
L = 5*pi; XInt = [-L,L];
T = 10^(-6); TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
% N = 2^8; % Time
% M = 2^8; % Space
% refM = 2^10; % Space
N = 2^18; % Time
M = 2^14; % Space
refM = 2^17; % Space
NVec = [N, 2*N, 4*N];
sigmaVec = [3.75, 3.9, 4, 4.1];
per = true;

u0Fun = @(x) 10*exp(-10*x.^2);

% Calculate initial H1 value and 
dx = (XInt(2)-XInt(1))/M;
x = XInt(1) + dx*(0:M-1);
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2-1, 0, -M/2+1:-1];
initH1 = H1norm(fft(u0Fun(x)),dx,k,per);

%% Define the alternative criteria for blowup - Boolean output
blowUpCrit = cell(0,0);

% Ratio comparisons
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.2*H1Old;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.5*H1Old;
% Derivative comparisons
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^2;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^4;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^6;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^8;
% Hard limit comparisons
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 25*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^2*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^3*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^4*initH1;

numBlowUpCrit = length(blowUpCrit);
%% Define the criteria for refining the process
% Lets refine each time H1-norm is 10 times bigger.
% Don't refine more than three times.
% Only refine process 1
refCrit = @(H1New,lastRefH1,numRef,processNo)  (H1New > 10*lastRefH1) & (numRef < 4) & (processNo == 1);

%% Initialize storage
numSigma = length(sigmaVec);
numN = length(NVec);
% Two different refinement levels / methods
% OBS! If this changes, the code within the loop may need to be adapted
numProc = 2;
blowupTimes = zeros(batchSize,numBlowUpCrit,numProc,numN,numSigma);

%% Perform calculations
rng(initSeed,'twister')
parfor m = 1:batchSize
    internalBlowupTimes = zeros(numBlowUpCrit,numProc,numN,numSigma);
    internalBlowUpCrit = blowUpCrit;
    internalRefCrit = refCrit;
    
    maxN = max(NVec);
    dW = randn(2,maxN)*sqrt(T/maxN);
    
    % Vary N and sigma
    for n = 1:length(NVec)
        for j = 1:length(sigmaVec)
            currN = NVec(n);
            sigma = sigmaVec(j);
            % If we are not using the finest time discretization, calculate the
            % coarser Brownian motion increments
            if currN < maxN
                scalingFactor = maxN/currN;
                coarseDW = zeros(2,currN);
                currIndex = 0;
                for i = 1:currN
                    indexList = (currIndex+1):(currIndex+scalingFactor/2);
                    coarseDW(1,i) = sum(sum(dW(:,indexList)));
                    indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
                    coarseDW(2,i) = sum(sum(dW(:,indexList)));
                    currIndex = currIndex + scalingFactor;
                end
            else
                coarseDW = dW;
            end
            modelInfos = cell(2,1);
            modelInfos{1} = initModelInfo(currN,TInt,M,XInt,sigma,per);
            modelInfos{2} = initModelInfo(currN,TInt,refM,XInt,sigma,per);

%             % Load variables into GPU memory
%             currU = gpuArray(fft(u0Fun(modelInfo.x)));
%             currUFine = gpuArray(fft(u0Fun(fineModelInfo.x)));
%             coarseDW = gpuArray(coarseDW);
            currU = cell(numProc,1);
            % Initial value
            for proc = 1:numProc
                currU{proc} = fft(u0Fun(modelInfos{proc}.x));
            end
            % Norm storage
            currH1Val = zeros(numProc,1);
            for proc = 1:numProc
                currH1Val(proc) = H1norm(currU{proc},modelInfos{proc}.dx,modelInfos{proc}.k,per);
            end
            oldH1Val = currH1Val;

            % Not yet refined
            numRef = zeros(numProc,1);
            lastRefVal = currH1Val;
            
            % Two criteria per process
            notExploded = true(numBlowUpCrit,numProc);
                
            i = 1;
            t = 0;
            while any(notExploded(:)) && i <= currN
                t = t + modelInfos{proc}.h;
                currDW = coarseDW(:,i);
                % Loop over each process
                for proc = 1:numProc
                    if(any(notExploded(:,proc)))
                        % Progress
                        currU{proc} = modelInfos{proc}.schemes.fun{7}(currU{proc},currDW);
                        % Calculate norm
                        oldH1Val(proc) = currH1Val(proc);
                        currH1Val(proc) = H1norm(currU{proc},modelInfos{proc}.dx,modelInfos{proc}.k,per);
                        % Check blowup criteria
                        for BUP = 1:numBlowUpCrit
                            notExploded(BUP,proc) = internalBlowUpCrit{BUP}(oldH1Val(proc),currH1Val(proc),modelInfos{proc}.h);
                            % Record time if criteria fulfilled
                            if notExploded(BUP,proc) && (internalBlowupTimes(BUP,proc,n,j) == 0)
                                internalBlowupTimes(BUP,proc,n,j) = t;
                            end
                        end
                        % Refinement if necessary
                        if internalRefCrit(currH1Val(proc),lastRefVal(proc),numRef(proc),proc)
                            numRef(proc) = numRef(proc) + 1;
                            tempCurrU = ifft(currU{proc});
                            currU{proc} = fft(refineU(tempCurrU,per,'nFme'));
                            modelInfos{proc} = initModelInfo(currN,TInt,length(currU{proc}),XInt,sigma,per);
                        end
                    end
                end
                fprintf(['\n-------'...
                    '\n Sample: ', num2str(m), ...
                    '\n N: ', num2str(currN), ...
                    '\n Sigma: ', num2str(sigma), ...
                    '.\n Percentage until max time: ', num2str(i/currN), ...
                    '.\n Current not blown up: ', num2str(sum(notExploded(:)))]);
                fprintf('.\n-------\n');
                i = i+1;
            end
        end
    end
    blowupTimes(m,:,:,:,:) = internalBlowupTimes;
    parforSave(['Data\blowupTime' num2str(m) '.mat'],internalBlowupTimes)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and process text files - H1 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for m = 1:batchSize
%     for n = 1:numN
%         for sigma = 1:numSigma
%             H1samples{m,n,sigma} = importdata(['TEST_H1_sample_' num2str(m) '_N_' num2str(n) '_sigma_' num2str(sigma) '.txt']);
%         end
%     end
% end
% 
% tVec = cell(numN,1);
% for i = 1:numN
%     tVec{i} = linspace(0,T,NVec(i)+1);
% end
% %% Observe what samples blow up at what time
% ratioBlowUpTimes = zeros(batchSize,numN,numSigma,4);
% limitBlowUpTimes = zeros(batchSize,numN,numSigma,4);
% for k = 1:batchSize
%     for n = 1:numN
%         for j = 1:numSigma
%             tempRatio = H1samples{k,n,j}(2:end,:)./H1samples{k,n,j}(1:end-1,:);
%             for i = 1:size(tempRatio,2)
%                 % Ratio blow up criteria
%                 index = find(tempRatio(:,i) > blowUpRatio,1);
%                 if index
%                     ratioBlowUpTimes(k,n,j,i) = tVec{n}(index);
%                 else
%                     % If the blowup hasn't happened before T, just set 2T
%                     % as a placeholder
%                     ratioBlowUpTimes(k,n,j,i) = 2*T;
%                 end
%                 
%                 % Limit blow up critera
%                 index = find(H1samples{k,n,j}(:,i) > blowUpLimit,1);
%                 if index
%                     limitBlowUpTimes(k,n,j,i) = tVec{n}(index);
%                 else
%                     % If the blowup hasn't happened before T, just set 2T
%                     % as a placeholder
%                     limitBlowUpTimes(k,n,j,i) = 2*T;
%                 end
%             end
%         end
%     end
% end
% % The elements which are still zero are the samples where there were no
% % refinement made. Add a placeholder here as well
% ratioBlowUpTimes(ratioBlowUpTimes==0) = 2*T;
% limitBlowUpTimes(limitBlowUpTimes==0) = 2*T;
% 
% numBins = 25;
% timeAxis = linspace(0,T,numBins);
% histID = {'Co','Fi','Lin','Fft'};
% timeID = {'0', 'T/2', 'T'};
% NID = cell(numN,1);
% sigmaID = cell(numSigma,1);
% for n = 1:numN
%     NID{n} = ['$N = 2^{' num2str(log2(NVec(n))) '}$'];
% end
% for j = 1:numSigma
%     sigmaID{j} = ['$\sigma = ' num2str(sigmaVec(j)) '$'];
% end
% 
% %% Plot and print multi-histograms
% vertHistPlotMult(ratioBlowUpTimes,histID,timeAxis,NID,sigmaID)
% printToPDF(gcf,'critExpHistRatio')
% vertHistPlotMult(limitBlowUpTimes,histID,timeAxis,NID,sigmaID)
% printToPDF(gcf,'critExpHistLimit')
% %% Count some processes
% % The number of processes which do not blow up according to the ratio
% % Coarse
% sum(ratioBlowUpTimes(:,:,:,1)==2*T,1)
% % Fine
% sum(ratioBlowUpTimes(:,:,:,2)==2*T,1)
% % Lin
% sum(ratioBlowUpTimes(:,:,:,3)==2*T,1)
% % Fft
% sum(ratioBlowUpTimes(:,:,:,4)==2*T,1)
% % The number of processes which meet the ratio critera before the limit
% sum(ratioBlowUpTimes(:) < limitBlowUpTimes(:))
% % Check how fine vs linear interpolation blow up times vary
% max(abs(ratioBlowUpTimes(:,:,:,2)-ratioBlowUpTimes(:,:,:,3))/T)
% % Check how fine vs Fft interpolation blow up times vary
% max(abs(ratioBlowUpTimes(:,:,:,2)-ratioBlowUpTimes(:,:,:,4))/T)