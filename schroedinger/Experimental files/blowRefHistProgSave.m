%% Set initial info and function
addpath('Data')
initSeed = 4;
batchSize = 4;
% Time and area
L = 5*pi; XInt = [-L,L];
T = 10^(-3); TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^19; % Time
M = 2^18; % Space
% N = 2^9; % Time
% M = 2^8; % Space
refM = 2*M; % Space
per = true;
sigma = 1;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
refModelInfo = initModelInfo(N,TInt,refM,XInt,sigma,per);
%%
% u0Fun = @(x) 1./(2+sin(x).^2);
u0Fun = @(x) exp(-4*x.^2);

% Coarse
u0FunVal = u0Fun(modelInfo.x);
% Norming the initial value
u0L2Val = L2norm(u0FunVal,modelInfo.dx,per);
u0FunVal = u0FunVal/sqrt(u0L2Val);
u0L2Val = L2norm(u0FunVal,modelInfo.dx,per);
% u0H1Val = H1norm(fft(u0FunVal),modelInfo.dx,modelInfo.k,per);

% Fine
fineU0FunVal = u0Fun(refModelInfo.x);
% Norming the initial value
refU0L2Val = L2norm(fineU0FunVal,refModelInfo.dx,per);
fineU0FunVal = fineU0FunVal/sqrt(refU0L2Val);
refU0L2Val = L2norm(fineU0FunVal,refModelInfo.dx,per);
% refU0H1Val = H1norm(fft(fineU0FunVal),refModelInfo.dx,refModelInfo.k,per);


numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

% OBS! Script designed for one scheme at a time
schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MEul
% schemesUsed(4) = true; % CN
% schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];


%% Perform calculations
rng(initSeed,'twister')
test = tic;
parfor m = 1:batchSize
    worker = getCurrentWorker;
    
    internalSchemeIndexMat = schemeIndexMat;
    internalModelInfo = modelInfo;
    internalRefModelInfo = refModelInfo;
    for j = 1:numUsedSchemes
        
        currScheme = internalSchemeIndexMat(j,2);

        W = randn(2,N)*sqrt(internalModelInfo.h);
        
            
        % Import data if it exists containing u_i, i, t and refinement info
        if isfile(['Data/blowRefHist' num2str(m) '_time_progress.txt'])
            % Function info. Due to precision, the data is imported in
            % string format, separated with comma signs
            temp = importdata(['blowRefHist' num2str(m) '_coarseU_progress.txt']);
            coarseU = str2double(strsplit(temp{1},','));
            
            temp = importdata(['blowRefHist' num2str(m) '_fine_or_interpol_progress.txt']);
            fineU = str2double(strsplit(temp{1},','));
            linIntU = str2double(strsplit(temp{2},','));
            splineIntU = str2double(strsplit(temp{3},','));
            fftIntU = str2double(strsplit(temp{4},','));
            
            % Time state
            t = importdata(['blowRefHist' num2str(m) '_time_progress.txt']);
            temp = importdata(['blowRefHist' num2str(m) '_state_progress.txt']);
            i = temp(1);
            if temp(2) == 0
                refined = false;
            else
                refined = true;
            end
            
            % Storage vectors
            temp = importdata(['blowRefHist' num2str(m) '_storage_vectors_progress.txt']);
            L2Vec = temp(1:5,:);
%             H1Vec = temp(6:10,:);
            
            % Clear temp variable
            temp = false;
        else
            % Function info
            coarseU = fft(u0FunVal);
            fineU = fft(fineU0FunVal);
            % Necessary for parfor code reading, values overwritten later
            linIntU = fineU;
            splineIntU = fineU;
            fftIntU = fineU;
            
            % Time state
            refined = false;
            t = 0;
            i = 1;
            
            % Storage vectors
            L2Vec = zeros(5,N+1);
                L2Vec(1,1) = u0L2Val;
                L2Vec(2,1) = refU0L2Val;
                L2Vec(3,1) = u0L2Val;
                L2Vec(4,1) = u0L2Val;
                L2Vec(5,1) = u0L2Val;

%             H1Vec = zeros(5,N+1);
%                 H1Vec(1,1) = u0H1Val;
%                 H1Vec(2,1) = refU0H1Val;
%                 H1Vec(3,1) = u0H1Val;
%                 H1Vec(4,1) = u0H1Val;
%                 H1Vec(5,1) = u0H1Val;
        end
            
        while i < N+1
            dW = W(:,i);
            %% Scheme and query calculations
            if ~refined
                % The fine and coarse
                coarseU = internalModelInfo.schemes.fun{currScheme}(coarseU,dW);
                fineU = internalRefModelInfo.schemes.fun{currScheme}(fineU,dW);
                
                % Store values
                tempCoarse = ifft(coarseU);
                tempFine = ifft(fineU);
                    % L2
                    L2Vec(1,i+1) = L2norm(tempCoarse,internalModelInfo.dx,per);
                    L2Vec(2,i+1) = L2norm(tempFine,internalRefModelInfo.dx,per);
                    L2Vec(3,i+1) = L2Vec(1,i+1);
                    L2Vec(4,i+1) = L2Vec(1,i+1);
                    L2Vec(5,i+1) = L2Vec(1,i+1);
%                     % H1
%                     H1Vec(1,i+1) = H1norm(coarseU,internalModelInfo.dx,internalModelInfo.k,per);
%                     H1Vec(2,i+1) = H1norm(fineU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
%                     H1Vec(3,i+1) = H1Vec(1,i+1);
%                     H1Vec(4,i+1) = H1Vec(1,i+1);
%                     H1Vec(5,i+1) = H1Vec(1,i+1);
            else
                % The fine and coarse
                coarseU = internalModelInfo.schemes.fun{currScheme}(coarseU,dW);
                fineU = internalRefModelInfo.schemes.fun{currScheme}(fineU,dW);
                % The refined
                linIntU = internalRefModelInfo.schemes.fun{currScheme}(linIntU,dW);
                splineIntU = internalRefModelInfo.schemes.fun{currScheme}(splineIntU,dW);
                fftIntU = internalRefModelInfo.schemes.fun{currScheme}(fftIntU,dW);
                
                % Store values
                tempCoarse = ifft(coarseU);
                tempFine = ifft(fineU);
                tempLin = ifft(linIntU);
                tempSpline = ifft(splineIntU);
                tempFft = ifft(fftIntU);
                    % L2
                    L2Vec(1,i+1) = L2norm(tempCoarse,internalModelInfo.dx,per);
                    L2Vec(2,i+1) = L2norm(tempFine,internalRefModelInfo.dx,per);
                    L2Vec(3,i+1) = L2norm(tempLin,internalRefModelInfo.dx,per);
                    L2Vec(4,i+1) = L2norm(tempSpline,internalRefModelInfo.dx,per);
                    L2Vec(5,i+1) = L2norm(tempFft,internalRefModelInfo.dx,per);
%                     % H1
%                     H1Vec(1,i+1) = H1norm(coarseU,internalModelInfo.dx,internalModelInfo.k,per);
%                     H1Vec(2,i+1) = H1norm(fineU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
%                     H1Vec(3,i+1) = H1norm(linIntU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
%                     H1Vec(4,i+1) = H1norm(splineIntU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
%                     H1Vec(5,i+1) = H1norm(fftIntU,internalRefModelInfo.dx,internalRefModelInfo.k,per);
            end
            
            % Refine using linear and custom interpolation
            if i == N/2 && ~refined
                tempLin = refineU(tempCoarse,per,'Linear',internalModelInfo.x,internalRefModelInfo.x);
                linIntU = fft(tempLin);
                tempSpline = refineU(tempCoarse,per,'Spline',internalModelInfo.x,internalRefModelInfo.x);
                splineIntU = fft(tempSpline);
                tempFft = refineU(tempCoarse,per,'Fft',internalModelInfo.x,internalRefModelInfo.x);
                fftIntU = fft(tempFft);
                
                refined = true;
                
                % Save function values just after interpolation
                printToText(['FunCoarse_' num2str(m) '.txt'],tempCoarse);
                printToText(['FunFine_' num2str(m) '.txt'],tempFine);
                printToText(['FunLinInt_' num2str(m) '.txt'],tempLin);
                printToText(['FunFFtInt_' num2str(m) '.txt'],tempSpline);
                printToText(['FunFFtInt_' num2str(m) '.txt'],tempFft);
            end
            
%             H1Val = H1norm(coarseU,internalModelInfo.dx,internalModelInfo.k,per);

            t = t + modelInfo.h;
            fprintf(['-------'...
                '\n Sample: ', num2str(m), ...
                '.\n Scheme: ', num2str(j), ...
                '.\n Percentage until max time: ', num2str(t/T), ...
                '.\n-------\n'])
%                 '.\n Current H1 value: ',...
%                 '\n', num2str(H1Vec(1,i+1)),...
%                 '\n', num2str(H1Vec(2,i+1)),...
%                 '\n', num2str(H1Vec(3,i+1)),...
%                 '\n', num2str(H1Vec(4,i+1)),...
%                 '\n', num2str(H1Vec(5,i+1)),...
%                 '.\n-------\n'])
            i = i + 1;
            
            % Save progress. If saved results exist, copy to backup in case
            % of crash / power outage
            if mod(i,N/64) == 0
                % Copy file to backup
                if isfile(['Data/blowRefHist' num2str(m) '_time_progress.txt'])
                    fileName = ['blowRefHist' num2str(m) '_coarseU_progress.txt'];
                    copyfile(['Data/' fileName],['Data/Backup/' fileName]);
                    fileName = ['blowRefHist' num2str(m) '_fine_or_interpol_progress.txt'];
                    copyfile(['Data/' fileName],['Data/Backup/' fileName]);
                    fileName = ['blowRefHist' num2str(m) '_time_progress.txt'];
                    copyfile(['Data/' fileName],['Data/Backup/' fileName]);
                    fileName = ['blowRefHist' num2str(m) '_state_progress.txt'];
                    copyfile(['Data/' fileName],['Data/Backup/' fileName]);
                    fileName = ['blowRefHist' num2str(m) '_storage_vectors_progress.txt'];
                    copyfile(['Data/' fileName],['Data/Backup/' fileName]);

                    fprintf('\nData backup complete.\n')
                    pause(0.1)
                end
                
                
                % Function info
                printToText(['blowRefHist' num2str(m) '_coarseU_progress.txt'],coarseU)
                temp = [fineU;linIntU;splineIntU;fftIntU];
                printToText(['blowRefHist' num2str(m) '_fine_or_interpol_progress.txt'],temp)

                % Time state
                printToText(['blowRefHist' num2str(m) '_time_progress.txt'],t)
                temp = zeros(1,2);
                temp(1) = i;
                temp(2) = refined;
                printToText(['blowRefHist' num2str(m) '_state_progress.txt'],temp)

                % Storage vectors
                temp = L2Vec;
%                 temp = [L2Vec;H1Vec];
                printToText(['blowRefHist' num2str(m) '_storage_vectors_progress.txt'],temp)

                % Clear temp variable
                temp = false;
                
                fprintf('\nData save complete.\n')
                pause(0.1)
            end
        end
    end
    printToText(['L2_' num2str(m) '.txt'],L2Vec);
%     printToText(['H1_' num2str(m) '.txt'],H1Vec);
    
    pause(0.1)
    % Remove progress and backup files
    delete(['Data/blowRefHist' num2str(m) '_coarseU_progress.txt'])
    delete(['Data/blowRefHist' num2str(m) '_fine_or_interpol_progress.txt'])
    delete(['Data/blowRefHist' num2str(m) '_time_progress.txt'])
    delete(['Data/blowRefHist' num2str(m) '_state_progress.txt'])
    delete(['Data/blowRefHist' num2str(m) '_storage_vectors_progress.txt'])
    
    delete(['Data/Backup/blowRefHist' num2str(m) '_coarseU_progress.txt'])
    delete(['Data/Backup/blowRefHist' num2str(m) '_fine_or_interpol_progress.txt'])
    delete(['Data/Backup/blowRefHist' num2str(m) '_time_progress.txt'])
    delete(['Data/Backup/blowRefHist' num2str(m) '_state_progress.txt'])
    delete(['Data/Backup/blowRefHist' num2str(m) '_storage_vectors_progress.txt'])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and process text files - H1 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract max H1 value and H1 differences
% H1Max = zeros(batchSize,4);
% H1Diff = zeros(batchSize,6);
% for i = 1:batchSize
%     fileName = ['H1_' num2str(i) '.txt'];
%     temp = importdata(fileName);
%     
%     currDiff = 1;
%     for j = 1:4
%         H1Max(i,j) = max(temp(j,:));
%         for k = 2:4
%             % No need for diagonal or mirrored differences
%             if k > j
%                 H1Diff(i,currDiff) = max(log10(abs(temp(j,:) - temp(k,:))));
%                 
%                 currDiff = currDiff + 1;
%             end
%         end
%     end
% end

%% Vertical histogram plot - Max H1 value
% % Histogram legend
% histLeg = {'Coarse','Fine','Lin. int','Fft int'};
% 
% % Set y axis info and plot the information in form of histrograms
% numHist = 4;
% histWidh = 0.774;
% schemePlotSpec = [0.1305,histWidh/numHist,0.11,0.815];
% 
% yMax = max(H1Max(:));
% yMin = min(H1Max(:));
% 
% % Set the y ticks and labels
% numBins = 30;
% yAxisVector = linspace(yMin,yMax,numBins);
% 
% sideTitle = '$max_{n\in\{1,2,\ldots,N\}}||u_n||_{H^1}^2$';
% vertHistPlot(H1Max,histLeg,yAxisVector,sideTitle,schemePlotSpec)
% set(gcf, 'Position', get(0, 'Screensize'));
% pause(1)

%% Vertical histogram plot - Max H1 difference
% % Histogram legend
% histLeg = {'Co-Fi','Co-Li','Co-Fft','Fi-Li','Fi-Cu','Li-Fft'};
% 
% % Set y axis info and plot the information in form of histrograms
% numHist = 6;
% histWidh = 0.774;
% schemePlotSpec = [0.1305,histWidh/numHist,0.11,0.815];
% 
% yMax = ceil(max(H1Diff(:)));
% yMin = floor(min(H1Diff(:)));
% 
% if yMin < -17
%     yMin = -17;
% end
% if yMax > 2
%     yMax = 2;
% end
% 
% % Set the y ticks and labels
% numBins = 30;
% yAxisVector = linspace(yMin,yMax,numBins);
% 
% sideTitle = '$max_{n\in\{1,2,\ldots,N\}}\left(log(| ~ ||u_n^i||_{H^1}^2-||u_n^j||_{H^1}^2 ~ |)\right)$';
% vertHistPlot(H1Diff,histLeg,yAxisVector,sideTitle,schemePlotSpec)
% set(gcf, 'Position', get(0, 'Screensize'));
% pause(1)
% % printToPDF(gcf,'H1Explosion')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and process text files - L2 norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extract max L2 differences
L2MaxDiff = zeros(batchSize,4);
for i = 1:batchSize
    fileName = ['L2_' num2str(i) '.txt'];
    temp = importdata(fileName);
    
    for j = 1:4
        L2MaxDiff(i,j) = max(log10(abs(temp(j,:)-temp(j,1))));
    end
end

%% Vertical histogram plot - Max L2 drift
% Histogram legend
histLeg = {'Coarse','Fine','Lin. int','Fft int'};

% Set y axis info and plot the information in form of histrograms
numHist = 4;
histWidh = 0.774;
schemePlotSpec = [0.1305,histWidh/numHist,0.11,0.815];

yMax = max(L2MaxDiff(:));
yMin = min(L2MaxDiff(:));

if yMin < -17
    yMin = -17;
end
if yMax > 2
    yMax = 2;
end

% Set the y ticks and labels
numBins = 30;
yAxisVector = linspace(yMin,yMax,numBins);

sideTitle = '$max_{n\in\{1,2,\ldots,N\}}\left(log(| ~ ||u_n||_{L^2}^2-||u_0||_{L^2}^2 ~ |)\right)$';
vertHistPlot(L2MaxDiff,histLeg,yAxisVector,sideTitle,schemePlotSpec)
set(gcf, 'Position', get(0, 'Screensize'));
pause(1)

% printToPDF(gcf,'H1Explosion')