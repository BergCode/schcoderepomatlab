%% Set initial info and function
% Time and area
L = 5*pi; XInt = [-L,L];
% XInt = [0,2*pi];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^2; % Time
M = 2.^(5:20); % Space
storeMPosition = 6;
refM = 2^6*max(M);
per = true;
sigma = 1;

%%
uFun = @(x) 1.4*exp(-3*x.^2);

referenceModelInfo = initModelInfo(N,TInt,refM,XInt,sigma,per); 
referenceSol = uFun(referenceModelInfo.x);
referenceL2 = L2norm(referenceSol,referenceModelInfo.dx,per);
referenceH1 = H1norm(fft(referenceSol),referenceModelInfo.dx,referenceModelInfo.k,per);

refinementNames = {'Linear','Interpft','Pchip', 'Spline', 'nFme'};
noKindsOfRefinement = length(refinementNames);
diffMatrix = zeros(length(M),noKindsOfRefinement+1);
L2Matrix = diffMatrix;
H1Matrix = diffMatrix;
storedRefinedFun = zeros(noKindsOfRefinement,2*M(storeMPosition));

%% Calculating differences
for i = 1:length(M)
    % Calculate reference solution sparse indexes
    spaceFactor = refM/M(i);
    coarseIndex = 1:spaceFactor:refM;
    refIndex = 1:spaceFactor/2:refM;
    
    % Construct the x and k vectors
    modelInfo = initModelInfo(N,TInt,M(i),XInt,sigma,per);
    refinedModelInfo = initModelInfo(N,TInt,2*M(i),XInt,sigma,per);
    % Calculate function, refinement and difference to reference solution
    uVal = uFun(modelInfo.x);
    for k = 1:noKindsOfRefinement
        % Refined solutions
        uRefinedVal = refineU(uVal,per,refinementNames{k},modelInfo.x,refinedModelInfo.x);
        diffMatrix(i,k) = L2norm(uRefinedVal-referenceSol(refIndex),refinedModelInfo.dx,per);
        L2Matrix(i,k) = L2norm(uRefinedVal,refinedModelInfo.dx,per);
        H1Matrix(i,k) = H1norm(fft(uRefinedVal),refinedModelInfo.dx,refinedModelInfo.k,per);
        
        % Store for last plot
        
        if i == storeMPosition
            storedRefinedFun(k,:) = uRefinedVal;
        end
    end
        % Unrefined solution
        diffMatrix(i,end) = L2norm(uVal-referenceSol(coarseIndex),modelInfo.dx,per);
        L2Matrix(i,end) = L2norm(uVal,modelInfo.dx,per);
        H1Matrix(i,end) = H1norm(fft(uVal),modelInfo.dx,modelInfo.k,per);
    
    % Store for last plot
    if i == storeMPosition
        storedFun = uVal;
    end
end

%% Plot differences
refineLine = {'-+','-x','-^','-v','-o'};

% Full page width plot
figure(1)
clf
temp = diffMatrix(:,1:end-1);
hold on
for i = 1:noKindsOfRefinement
    plot(M,temp(:,i),refineLine{i},'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Full'))
end
hold off

ylabel('$||\hat{u}_M-u||_{L^2}^2$','Interpreter','latex')
xlabel('$M$','Interpreter','latex')
legend(refinementNames,'Location','southwest')
set(gca,'FontSize',fontAndMarkerSize('Font','Full'))
set(gca,'xscale','log');
set(gca,'yscale','log');
ylim([min(temp(:))/10 10*max(temp(:))])
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationComparisonL2Error')

% Half page width plot
figure(2)
clf
L2Diff = abs(L2Matrix-referenceL2);
hold on
for i = 1:noKindsOfRefinement
    plot(M,L2Diff(:,i),refineLine{i},'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'))
end
% plot(M,L2Diff(:,end),'LineWidth',2.5)
hold off

ylabel('$| ~ ||\hat{u}_M||_{L^2}^2-||u||_{L^2}^2 ~ |$','Interpreter','latex')
xlabel('$M$','Interpreter','latex')
legend([refinementNames 'Unrefined'],'Location','southwest')
set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
set(gca,'xscale','log');
set(gca,'yscale','log');
ylim([min(L2Diff(:))/10 10*max(L2Diff(:))])
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationComparisonL2Difference')

% Half page width plot
figure(3)
clf
H1Diff = abs(H1Matrix-referenceH1);
hold on
for i = 1:noKindsOfRefinement
    plot(M,H1Diff(:,i),refineLine{i},'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'))
end
plot(M,H1Diff(:,end),'LineWidth',2.5)
hold off

ylabel('$| ~ ||\hat{u}_M||_{H^1}^2-||u||_{H^1}^2 ~ |$','Interpreter','latex')
xlabel('$M$','Interpreter','latex')
legend([refinementNames 'Unrefined'],'Location','southwest')
set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
set(gca,'xscale','log');
set(gca,'yscale','log');
ylim([min(H1Diff(:))/10 10*max(H1Diff(:))])
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationComparisonH1Difference')

%% Plot frequency comparison
kVec = 2*pi/(XInt(2)-XInt(1))*ifftshift((-M(storeMPosition)/2):(M(storeMPosition)/2-1));
kRefVec = 2*pi/(XInt(2)-XInt(1))*ifftshift((-M(storeMPosition)):(M(storeMPosition)-1));

figure(4)
xlimArg = 1.1*[min(kRefVec) max(kRefVec)];
ylimArg = [10^-17 10^2];
axisInfo = [xlimArg ylimArg];
% Refined
for k = 1:noKindsOfRefinement
    temp = fft(storedRefinedFun(k,:));
    % Plot the frequencies
    subplot(3,2,k+1)
    temp = abs(temp);
    plot(kRefVec,temp,'.')
    title(refinementNames{k})
    set(gca,'FontSize',35)
    set(gca,'yscale','log')
    axis(axisInfo)
    set(gca,'YTick', [10^-15 10^0])
end
% Unrefined
subplot(3,2,1)
temp = abs(fft(storedFun));
plot(kVec,temp,'.')
title('Unrefined')
set(gca,'FontSize',fontAndMarkerSize('Font','Full'))
set(gca,'yscale','log')
axis(axisInfo)
set(gca,'YTick', [10^-15 10^0])

figure(4)
set(gcf,'units','normalized','outerposition',[0 0 1 1])
pause(1)
printToPDF(gcf,'InterpolationComparison')