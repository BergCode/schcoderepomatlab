T = 1;

NVec = 2.^(2:13);
numN = length(NVec);
refN = 2^16;

refh = T/refN;
refW = randn(refN,2)*sqrt(refh/2);

scalingFactor = zeros(numN,1);
coarseW = cell(numN,1);
for n = 1:numN
    currN = NVec(n);
    scalingFactor(n) = refN/currN;
    coarseW{n} = zeros(currN,2);
    currIndex = 0;
    for i = 1:currN
        indexList = (currIndex+1):(currIndex+scalingFactor(n)/2);
        coarseW{n}(i,1) = sum(sum(refW(indexList,:)));
        indexList = (currIndex+scalingFactor(n)/2+1):(currIndex+scalingFactor(n));
        coarseW{n}(i,2) = sum(sum(refW(indexList,:)));
        currIndex = currIndex + scalingFactor(n);
    end
end
%% Visualize

close all
figure(1)
hold on
plot(linspace(0,T,refN+1),[0 ; cumsum(sum(refW,2))])
pause(2)
for i = 1:numN
    plot(linspace(0,T,NVec(i)+1),[0 ; cumsum(sum(coarseW{i},2))])
    pause(2)
end
hold off

%% Check mean and variance of coarser dW
batchSize = 10^3;
dWBatch = zeros(batchSize,numN);
for m = 1:batchSize
    refW = randn(refN,2)*sqrt(refh/2);

    for n = 1:numN
        currN = NVec(n);
        scalingFactor = refN/currN;
        coarseW = zeros(currN,2);
        currIndex = 0;
        for i = 1:currN
            indexList = (currIndex+1):(currIndex+scalingFactor/2);
            coarseW(i,1) = sum(sum(refW(indexList,:)));
            indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
            coarseW(i,2) = sum(sum(refW(indexList,:)));
            currIndex = currIndex + scalingFactor;
        end
        dWBatch(m,n) = sum(sum(coarseW));
    end
    m
end
mean(dWBatch)
var(dWBatch)

%% Plot out several paths and compare with a linear time increase
batchSize = 50;
t = linspace(0,T,refN);

figure(1)
plot(t,t)
hold on
for m = 1:batchSize
    refW = randn(refN,2)*sqrt(refh/2);
    plot(t,cumsum(sum(refW,2)))
end
hold off