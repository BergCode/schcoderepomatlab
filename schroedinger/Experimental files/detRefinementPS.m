%% Set initial info and function
% Time and area
L = 20*pi; XInt = [-L,L];
% XInt = [0,2*pi];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^13; % Time
M = 2^14; % Space
% N = 2^29; % Time
% M = 2^21; % Space
per = true;
sigma = 1;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
%%
alpha = 1;
q = 1;
c = 1;
u0Fun = @(x) sqrt(2*alpha/q)*exp(0.5*1i*c*x).*sech(sqrt(alpha)*x);
uExactSol = @(x,t) sqrt(2*alpha/q)*exp(1i*(0.5*c*x-(0.25*c^2-alpha)*t)).*sech(sqrt(alpha)*(x-c*t));

% L2norm = @(currU,dx) trapezoidalIntegral(dx,[absSq(currU), absSq(currU(1))]);
% H1norm = @(currU,k,dx) L2norm(ifft(currU),dx) + L2norm(ifft(1i*k.*currU),dx);
% H1norm(u,dx,k,per)


u0FunVal = u0Fun(modelInfo.x);
u0L2Val = L2norm(u0FunVal,modelInfo.dx,per);
u0H1Val = H1norm(fft(u0FunVal),modelInfo.dx,modelInfo.k,per);
% Norm the initial value
% u0FunVal = u0FunVal / sqrt(u0L2Val);
%plot(u0FunVal)

numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MEul
% schemesUsed(4) = true; % CN
% schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Perform calculations
refinementNames = {'Linear','Fft','Pchip', 'Spline', 'Custom'};
noKindsOfRefinement = length(refinementNames);

% Store time
% firstHalf = 0:modelInfo.h:(T/2);
% secondHalf = (T/2+modelInfo.h/2):(modelInfo.h/2):T;
% timeVec = [firstHalf secondHalf];
timeVec = 0:modelInfo.h:T;
% Store L2 norm, H1 norm and L2 error
diagMat = zeros(noKindsOfRefinement,numUsedSchemes,3,length(timeVec),2);

diagMat(:,:,1,1,:) = u0L2Val*ones(noKindsOfRefinement,numUsedSchemes,2);
diagMat(:,:,2,1,:) = u0H1Val*ones(noKindsOfRefinement,numUsedSchemes,2);
diagMat(:,:,3,1,:) = zeros(noKindsOfRefinement,numUsedSchemes,2);

%%
parfor m = 1:noKindsOfRefinement
    
    internalRefNames = refinementNames;
    
    tempMat = diagMat(m,:,:,:,:);
    internalSchemeIndexMat = schemeIndexMat;
    for j = 1:numUsedSchemes
        coarseModelInfo = modelInfo;
        fineModelInfo = initModelInfo(coarseModelInfo.N*2,TInt,coarseModelInfo.M*2,XInt,sigma,per);
        
        fineCurrU = 0;
        tempFineCurrU = 0;
        tempCurrU = 0;
        currU = fft(u0FunVal);
        currScheme = internalSchemeIndexMat(j,2);
        t = 0;
        notRefined = true;
        i = 2;
        
        coarsedW = ones(2,1)*coarseModelInfo.h/2;
        finedW = ones(2,1)*fineModelInfo.h/2;
        while t < T
%             dW = randn(2,1)*sqrt(internalModelInfo.h);
%             dW = ones(2,1)*fineModelInfo.h/2;
            %% Scheme and query calculations
            
            if notRefined
                currU = coarseModelInfo.schemes.fun{currScheme}(currU,coarsedW);
                
                tempCurrU = ifft(currU);
            else
                currU = coarseModelInfo.schemes.fun{currScheme}(currU,coarsedW);
                
                
                fineCurrU = fineModelInfo.schemes.fun{currScheme}(fineCurrU,finedW);
                fineCurrU = fineModelInfo.schemes.fun{currScheme}(fineCurrU,finedW);
                
                tempCurrU = ifft(currU);
                tempFineCurrU = ifft(fineCurrU);
            end
            t = t + coarseModelInfo.h;
            
            % Perform refinement after fixed time
            
            if t == T/2 && notRefined
                % m determines what refinement is used
                tempFineCurrU = refineU(tempCurrU,per,internalRefNames{m},coarseModelInfo.x,fineModelInfo.x);
                
                fineCurrU = fft(tempFineCurrU);
                
                notRefined = false;
            end

            if notRefined
                % Fine
                tempMat(1,j,1,i,1) = L2norm(tempCurrU,coarseModelInfo.dx,per);
                tempMat(1,j,2,i,1) = H1norm(currU,coarseModelInfo.dx,coarseModelInfo.k,per);
                tempMat(1,j,3,i,1) = L2norm(tempCurrU-uExactSol(coarseModelInfo.x,t),coarseModelInfo.dx,per);
                % Coarse
                tempMat(1,j,1,i,2) = tempMat(1,j,1,i,1);
                tempMat(1,j,2,i,2) = tempMat(1,j,2,i,1);
                tempMat(1,j,3,i,2) = tempMat(1,j,3,i,1);
            else
                % Fine
                tempMat(1,j,1,i,1) = L2norm(tempFineCurrU,fineModelInfo.dx,per);
                tempMat(1,j,2,i,1) = H1norm(fineCurrU,fineModelInfo.dx,fineModelInfo.k,per);
                tempMat(1,j,3,i,1) = L2norm(tempFineCurrU-uExactSol(fineModelInfo.x,t),fineModelInfo.dx,per);
                % Coarse
                tempMat(1,j,1,i,2) = L2norm(tempCurrU,coarseModelInfo.dx,per);
                tempMat(1,j,2,i,2) = H1norm(currU,coarseModelInfo.dx,coarseModelInfo.k,per);
                tempMat(1,j,3,i,2) = L2norm(tempCurrU-uExactSol(coarseModelInfo.x,t),coarseModelInfo.dx,per);
            end
            
            i = i+1;
            disp([m t/T])
            
%             figure(1)
%             plotProfile(internalModelInfo.x,ifft(currU),false)
%             title(["t = " num2str(t)])
%             set(gcf, 'Position', get(0, 'Screensize'));
%             axis([-65 65 -0.8 0.8 ])
%             pause(0.0001)
            
        end
    end
    diagMat(m,:,:,:,:)=tempMat;
end
%%
figure(1)
plot(timeVec,squeeze(diagMat(:,1,1,:,1)),timeVec,squeeze(diagMat(1,1,1,:,2)),'LineWidth',1.5)
title("L2-norm")
legend({'Linear', 'Fft', 'Pchip', 'Spline', 'Custom','unrefined'},'Location','southwest')
set(gcf, 'Position', get(0, 'Screensize'));
set(gca,'FontSize',35)
pause(1)
% printToPDF(gcf,'L2normRefinementDet')

% Zoom in to refinement moment
% horZoom = 0.001;
% verZoom = 0.0001;
% axis([0.5-horZoom 0.5+horZoom (1-verZoom)*diagMat(1,1,1,N/2,2) (1+verZoom)*diagMat(1,1,1,N/2,2)])
% title("L2-norm zoomed")
% pause(1)
% printToPDF(gcf,'L2normRefinementDetZoom')


figure(2)
plot(timeVec,squeeze(diagMat(:,1,2,:,1)),timeVec,squeeze(diagMat(1,1,2,:,2)),'LineWidth',1.5)
title("H1-norm")
legend({'Linear', 'Fft', 'Pchip', 'Spline', 'Custom','unrefined'},'Location','southwest')
set(gcf, 'Position', get(0, 'Screensize'));
set(gca,'FontSize',35)
pause(1)
% printToPDF(gcf,'H1normRefinementDet')

% Zoom in to refinement moment
% horZoom = 0.001;
% verZoom = 0.0000005;
% axis([0.5-horZoom 0.5+horZoom (1-verZoom)*diagMat(1,1,2,N/2,2) (1+verZoom)*diagMat(1,1,2,N/2,2)])
% title("H1-norm zoomed")
% pause(1)
% printToPDF(gcf,'H1normRefinementDetZoom')


figure(3)
plot(timeVec,squeeze(diagMat(:,1,3,:,1)),timeVec,squeeze(diagMat(1,1,3,:,2)),'LineWidth',1.5)
title("L2-error")
legend({'Linear', 'Fft', 'Pchip', 'Spline', 'Custom','unrefined'},'Location','southwest')
set(gca,'yscale','log');
set(gcf, 'Position', get(0, 'Screensize'));
set(gca,'FontSize',35)
pause(1)
printToPDF(gcf,'L2ErrorRefinementDet')

% Zoom in to refinement moment
% horZoom = 0.000005;
% verZoom = 0.00001;
% axis([0.5-T/N-horZoom 0.5-T/N+horZoom (1-verZoom)*diagMat(1,1,3,N/2,2) (1+verZoom)*diagMat(1,1,3,N/2,2)])
% set(gca,'yscale','log');
% title("L2-error zoomed")
% pause(1)
% printToPDF(gcf,'L2ErrorRefinementDetZoom')