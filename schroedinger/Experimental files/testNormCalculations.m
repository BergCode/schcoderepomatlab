% This script checks convergence and time requirements for both the L2, and
% the H1 norm, using a Simpsons and a rectangle approximation and using
% different functions.
% Conclusion: Some initial values yield a hyperconvergence. Further, the 
% rectangle approximation provides with significant reduction in time when
% calculating, making it optimal for the latter experiments.
numNormMethods = 6;
batchSize = 10000;
% Normal convergence, or L2-equivalent?
L2Eq = true;

f = cell(0,0);
g = cell(0,0); % Derivative
f{end+1} = @(x) cos(x);
    g{end+1} = @(x) -sin(x);
f{end+1} = @(x) exp(-x.^2);
    g{end+1} = @(x) -2*x.*exp(-x.^2);
% f{end+1} = @(x) exp(x); % Not periodic (req. for H1)
%     g{end+1} = @(x) exp(x);
f{end+1} = @(x) 1./(x.^4+1);
    g{end+1} = @(x) -4*x.^3./(x.^4+1).^2;
% f{end+1} = @(x) exp(1./(abs(x)+1)); % Function not in H1
%     g{end+1} = @(x) -x.^(abs(x)+1)*sqrt(exp(1))./(abs(x).*(abs(x)+1).^2);
% f{end+1} = @(x) exp(x+1i*x); % Not periodic (req. for H1)
%     g{end+1} = @(x) (1+1i)*exp(x+1i*x);
f{end+1} = @(x) absSq(exp(-x.^2));
    g{end+1} = @(x) -4*exp(-2*x.^2).*x;
f{end+1} = @(x) 1./(2+sin(x).^2);
    g{end+1} = @(x) -2*sin(x).*cos(x)./(2+sin(x).^2).^2;
numFun = length(f);


MVector = 2.^(4:11);
numM = length(MVector);

L = 5;
TInt = [-L,L];

xVecs = cell(numM,1);
diffs = zeros(numNormMethods,numM,numFun);
compTimes = diffs;
for m = 1:numM
    M = MVector(m);
    dx = (TInt(2)-TInt(1))/M;
    x = TInt(1) + dx*(0:M-1);
    k = 2*pi/(TInt(2)-TInt(1))*[0:M/2, -M/2+1:-1];
    kSq = k.^2;
    
    for i = 1:numFun
        y = f{i}(x);
        u = fft(y);
        
        % L2 norm
        t = tic;
        for sample = 1:batchSize
            simpsL2 = L2norm(ifft(u),dx,true);
        end
        compTimes(1,m,i) = toc(t);
        % Alt. L2 norm
        t = tic;
        for sample = 1:batchSize
            altL2 = sum(absSq(u))/M^2*2*L;
        end
        compTimes(2,m,i) = toc(t);
        % Alt. 2 L2 norm
        t = tic;
        for sample = 1:batchSize
            u1 = ifft(u);
            alt2L2 = sum(absSq(u1))/M*2*L;
        end
        compTimes(3,m,i) = toc(t);
        % Ref L2 norm
        refL2 = integral(@(x) absSq(f{i}(x)),TInt(1),TInt(2));
        
        % H1 norm
        t = tic;
        for sample = 1:batchSize
            simpsH1 = H1norm(u,dx,k,true);
        end
        compTimes(4,m,i) = toc(t);
        % Alt. H1 norm
        t = tic;
        for sample = 1:batchSize
            altH1 = sum((1+k.^2).*absSq(u))/M^2*2*L;
        end
        compTimes(5,m,i) = toc(t);
        % Alt. 2 H1 norm
        t = tic;
        for sample = 1:batchSize
            u1 = ifft(u);
            u2 = ifft(-1i*k.*u);
            alt2H1 = (sum(absSq(u1)) + sum(absSq(u2)))/M*2*L;
        end
        compTimes(6,m,i) = toc(t);
        
        % Ref H1 norm
        refH1 = integral(@(x) absSq(f{i}(x)) + absSq(g{i}(x)),TInt(1),TInt(2));
        
        % Simple function or L2
        diffs(1,m,i) = abs(simpsL2-refL2);
        diffs(2,m,i) = abs(altL2-refL2);
        diffs(3,m,i) = abs(alt2L2-refL2);
        % H1 norm
        diffs(4,m,i) = abs(altH1-refH1);
        diffs(5,m,i) = abs(altH1-refH1);
        diffs(6,m,i) = abs(alt2H1-refH1);
    end
end
%% Plot the errors vs spatial disc.
for i = 1:numNormMethods
    figure(i)
    loglog(MVector,squeeze(diffs(i,:,:)))
    legend(num2str((1:numFun)'))
end

%% Plot the differences between the simpson and alternative evaluations
comparisonIndex = [1,2;
    2,1;
    3,1;
    3,2;
    4,5;
    5,4;
    6,4;
    6,5];
comparisonTitles = {'L2 Simpsons method worse'
    'L2 Alt. method worse'
    'L2 Alt. method 2 worse (than Simpson)'
    'L2 Alt. method 2 worse (than Simpson)'
    'H1 Simpsons method worse'
    'H1 Alt. method worse'
    'H1 Alt. method 2 worse (than Simpson)'
    'H1 Alt. method 2 worse (than alt 1)'};
numCompares = size(comparisonIndex,1);
for i = 1:numCompares
    figure(numNormMethods+i)
    comp1 = comparisonIndex(i,1);
    comp2 = comparisonIndex(i,2);
    loglog(MVector,squeeze(diffs(comp1,:,:))-squeeze(diffs(comp2,:,:)))
    legend(num2str((1:numFun)'))
    title(comparisonTitles{i})
end
%% Plot the comp times vs spatial disc.
comparisonIndex = {[1 2 3] ; [4 5 6]};
legArgs = {{'L2 Simpsons', 'L2 alt', 'L2 alt 2'},{'H1 Simpsons', 'H1 alt 1', 'H1 alt 2'}};
% legArgs = {'L2' 'H1'};
numCompares = length(comparisonIndex);
for i = 1:numCompares
    figure%(2*numNormMethods+i)
    compInd = comparisonIndex{i};
    hold on
    for j = 1:length(compInd)
        plot(MVector,sum(squeeze(compTimes(compInd(j),:,:)),2))
    end
    hold off
    legend(legArgs{i})
end
%     temp = sum(squeeze(compTimes(2*i-1,:,:)),2)./sum(squeeze(compTimes(2*i,:,:)),2);
%     plot(MVector,temp)
%     legend(legArgs{i})
%     title('Higher -> Simps worse, in time')