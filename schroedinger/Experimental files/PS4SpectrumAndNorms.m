addpath('Functions')
%% Set initial info and function
initSeed = 1;
% Time and area
L = 30; XInt = [-L,L];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^12; % Time
M = 2^12; % Space
sigma = 1;

u0Fun = @(x) exp(-x.^2);
per = true;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);

u0FunVal = u0Fun(modelInfo.x);
u0FunValFft = fft(u0FunVal);

numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Query storage
% Query fidelity
timeFidelity = 2^7;
spaceVec = abs(modelInfo.k)<70;

if N > timeFidelity
    timeScalingFactor = N / timeFidelity;
    timeVec = 0:timeScalingFactor:N;
else
    timeScalingFactor = 1;
    timeVec = 0:N;
end

numQueries = 4;
queryStorage =  cell(numUsedSchemes,numQueries);
query = cell(numQueries,2); % Function evaluating query and boolean to determine physical or Fourier space
    % L2 norm
query{1,1} = @(u) L2norm(u,modelInfo.dx,per);
query{1,2} = true;
    % H1 norm
query{2,1} = @(u) H1norm(u,modelInfo.dx,modelInfo.k,per);
query{2,2} = false;
    % LInf norm
query{3,1} = @(u) sqrt(max(absSq(u)));
query{3,2} = true;
    % Energy
query{4,1} = @(u) schroedEnergy(u,modelInfo.dx,modelInfo.k,sigma,per);
query{4,2} = false;

% Initialize storage
runStorage = cell(numUsedSchemes,1);
for j = 1:numUsedSchemes
    runStorage{j} = zeros(length(timeVec),sum(spaceVec));
    runStorage{j}(1,:) = u0FunValFft(spaceVec);
    for k = 1:numQueries
        queryStorage{j,k} = zeros(length(timeVec),1);
        if query{k,2}
            queryStorage{j,k}(1) = query{k,1}(u0FunVal);
        else
            queryStorage{j,k}(1) = query{k,1}(u0FunValFft);
        end
    end
end

%% Perform calculations
rng(initSeed,'twister')
W = randn(N,2)*sqrt(modelInfo.h/2);
for j = 1:numUsedSchemes
    currU = u0FunValFft;
    currScheme = schemeIndexMat(j,2);
    queryIndex = 2;
    for i = 1:N
        dW = W(i,:);
        %% Scheme and query calculations
        currU = modelInfo.schemes.fun{currScheme}(currU,dW);
        if mod(i,timeScalingFactor) == 0
            runStorage{j}(queryIndex,:) = currU(spaceVec);
            
            currUIfft = ifft(currU);
            for k = 1:numQueries
                % In physical space or not
                if query{k,2}
                    queryStorage{j,k}(queryIndex) = query{k,1}(currUIfft);
                else
                    queryStorage{j,k}(queryIndex) = query{k,1}(currU);
                end
            end
            queryIndex = queryIndex + 1;
        end
    end
end
%% Waterfall plots
for i = 1:numUsedSchemes
    figure
    waterfall(modelInfo.k(spaceVec),linspace(TInt(1),TInt(2),timeFidelity+1),absSq(runStorage{i}))
    set(gcf, 'Position', get(0, 'Screensize'));
    set(gca,'FontSize',35)
    pause(1)
%     printToPDF(gcf,['PSSpectrum' schemes.shortNames{schemeIndexMat(i,2)}])
end
%% Energy and LInf norm evolution
schemeLegendMarks = {'-^r','-vm','k','k','-db','k'};
fileName = {'L2','H1','LInf','Energy'};
title = {'$||u_n||_{L^2}^2$','$||u_n||_{H_1}^2$','$||u_n||_{L^\infty}^2$','Energy$(u_n)$'};

for k = 1:numQueries
    figure
    plotQueryEvol(modelInfo,queryStorage(:,k),TInt,schemeIndexMat,schemeLegendMarks,title{k})
    pause(1)
    % printToPDF(gcf,fileName{k})
end