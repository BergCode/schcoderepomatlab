%% Refinement in space only
%% Set initial info and function
initSeed = 4;
batchSize = 1;
confLevel = 0.95;
% Time and area
L = 5*pi; XInt = [-L,L];
% XInt = [0,2*pi];
T = 0.125; TInt = [0,T];

% Numerical precision, number of points
N = 2^11; % Time
refN = 2^14;
M = 2^11; % Space
refM = 2^14;
spaceFactor = refM/M;
per = false;
sigma = 1;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
refinedModelInfo = initModelInfo(N,TInt,2*M,XInt,sigma,per);
referenceModelInfo = initModelInfo(refN,TInt,refM,XInt,sigma,per);

%%
u0Fun = @(x) 4.5*exp(-2*x.^2);

u0FunVal = u0Fun(modelInfo.x);
u0RefFunVal = u0Fun(referenceModelInfo.x);

numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MEul
schemesUsed(4) = true; % CN
% schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
% schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Query storage
noKindsOfRefinement = 4;

noQueries = 2;
query = cell(noQueries,1); % Function evaluating query and boolean to determine physical or Fourier space
    % L2 norm
query{1} = @(u,dx,uRef) L2norm(u,dx,per);
% H1norm(fft(u),model.dx,k,per);
    % L2 difference
query{2} = @(u,dx,uRef) L2norm(u-uRef,dx,per);

% Initialize storage
diagMat = zeros(batchSize,N+1,noKindsOfRefinement,numUsedSchemes,noQueries,2);
initSize = size(diagMat(:,1,:,:,1,:));
initMat = ones(initSize);

for k = 1:noQueries
    queryVal = query{k,1}(u0FunVal,modelInfo.dx,u0RefFunVal(1:spaceFactor:end));
    diagMat(:,1,:,:,k,:) = initMat*queryVal;
end

%% Perform calculations
rng(initSeed,'twister')
for m = 1:batchSize
    % Load internal variables
    sampleMat = diagMat(m,:,:,:,:,:);
    internalQueries = query;
    internalSchemeIndexMat = schemeIndexMat;
    
    internalModelInfo = modelInfo;
    internalRefinedModelInfo = refinedModelInfo;
    internalReferenceModelInfo = referenceModelInfo;
    
    u0Val = u0FunVal;
    u0Ref = u0RefFunVal;
    
    for k = 1:noKindsOfRefinement
        for j = 1:numUsedSchemes
            currScheme = internalSchemeIndexMat(j,2);
            
            coarseCurrU = u0Val;
            refinedCurrU = 0;
            refCurrU = u0Ref;
            
            t = 0;
            notRefined = true;
            numStepsForRef = refN/N;
            spaceFactor = refM/M;
            i = 2;

            while t < T
                %% Scheme and query calculations
%                 dW = randn(2,numStepsForRef)*sqrt(internalReferenceModelInfo.h);
                dW = ones(2,numStepsForRef)*(internalReferenceModelInfo.h/2);
                coarsedW = sum(dW,2);
                % Perform reference steps first and then one actual step
                % Lastly, calculate query
                if notRefined
                    for w = 1:numStepsForRef
                        dWStep = dW(:,w);
                        refCurrU = internalReferenceModelInfo.schemes.fun{currScheme}(refCurrU,dWStep);
                    end
                    coarseCurrU = internalModelInfo.schemes.fun{currScheme}(coarseCurrU,coarsedW);
                    
                    % Calculate the queries
                    for q = 1:noQueries
                        coarseQueryVal = internalQueries{q}(coarseCurrU,internalModelInfo.dx,refCurrU(1:spaceFactor:end));
                        sampleMat(1,i,k,j,q,1) = coarseQueryVal;
                        sampleMat(1,i,k,j,q,2) = coarseQueryVal;
                    end
                else
                    for w = 1:numStepsForRef
                        dWStep = dW(:,w);
                        refCurrU = internalReferenceModelInfo.schemes.fun{currScheme}(refCurrU,dWStep);
                    end
                    coarseCurrU = internalModelInfo.schemes.fun{currScheme}(coarseCurrU,coarsedW);
                    refinedCurrU = internalRefinedModelInfo.schemes.fun{currScheme}(refinedCurrU,coarsedW);
                    
                    % Calculate the queries
                    for q = 1:noQueries
                        coarseQueryVal = internalQueries{q}(coarseCurrU,internalModelInfo.dx,refCurrU(1:spaceFactor:end));
                        refinedQueryVal = internalQueries{q}(refinedCurrU,internalRefinedModelInfo.dx,refCurrU(1:spaceFactor/2:end));
                        sampleMat(1,i,k,j,q,1) = coarseQueryVal;
                        sampleMat(1,i,k,j,q,2) = refinedQueryVal;
                    end
                end
                t = t + internalModelInfo.h;
                
                
                % Perform refinement after fixed time
                if t == T/2 && notRefined
                    % Check the different types of refinement
                    switch k
                        case 1
                        % Linear interpolation
                        interpolationType = 'linear';
                        case 2
                        % Fft interpolation
                        interpolationType = 'fft';
                        case 3
                        % Piecewise Cubic Hermite Interpolating Polynomial
                        % Good for interpolation of less oscillatory problems
                        interpolationType = 'pchip';
                        case 4
                        % Cubic spline data interpolation
                        % Good for interpolation of more oscillatory problems
                        interpolationType = 'spline';
                    end
                    refinedCurrU = refineU(coarseCurrU,per,interpolationType,internalModelInfo.x,internalRefinedModelInfo.x);

                    notRefined = false;
                end
                i = i+1;
                
                fprintf(['-------'...
                    '\n Sample: ', num2str(m), ...
                    '.\n Scheme: ', num2str(j), ...
                    '.\n Percentage until max time: ', num2str(t/T), ...
                    '.\n Refinement method: ', num2str(k),...
                    '.\n-------\n'])
            end
        end
    end
    diagMat(m,:,:,:,:,:) = sampleMat;
end
%%
timeVec = linspace(0,T,N+1);
meanMat = mean(diagMat,1);

figure(1)
plot(timeVec,squeeze(diagMat(1,:,:,:,1,2)),timeVec,squeeze(diagMat(1,:,1,1,1,1)))
% plot(timeVec,squeeze(meanMat(:,:,:,1,1)),timeVec,squeeze(meanMat(:,:,1,1,1)))
title("L2-norm")
legend({'linear', 'fft', 'pchip', 'spline','unrefined'})
set(gcf, 'Position', get(0, 'Screensize'));
pause(1)
% printToPDF(gcf,'L2normRefinementFD')
%%
% Zoom in to refinement moment
horZoom = 0.001;
verZoom = 0.0000001;
axis([T/2-horZoom T/2+horZoom (1-verZoom)*diagMat(1,N/2,1,1,1,2) (1+verZoom)*diagMat(1,N/2,1,1,1,2)])
title("L2-norm zoomed")
pause(1)
% printToPDF(gcf,'L2normRefinementZoomFD')

%%
figure(2)
plot(timeVec,squeeze(diagMat(1,:,:,:,2,2)),timeVec,squeeze(diagMat(1,:,1,1,2,1)))
title("L2-error")
legend({'linear', 'fft', 'pchip', 'spline','unrefined'})
set(gca,'yscale','log');
set(gcf, 'Position', get(0, 'Screensize'));
pause(1)
% printToPDF(gcf,'L2ErrorRefinementFD')
%%
% Zoom in to refinement moment
horZoom = 0.001;
verZoom = 0.1;
axis([T/2-horZoom T/2+horZoom (1-verZoom)*diagMat(1,N/2,1,1,2,1) (1+verZoom)*diagMat(1,N/2,1,1,2,1  )])
set(gca,'yscale','log');
title("L2-error zoomed")
pause(1)
% printToPDF(gcf,'L2ErrorRefinementZoomFD')