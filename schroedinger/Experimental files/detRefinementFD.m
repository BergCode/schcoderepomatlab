%% Set initial info and function
initSeed = 4;
% Time and area
L = 20*pi; XInt = [-L,L];
% XInt = [0,2*pi];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^10; % Time
M = 2^10; % Space
% N = 2^29; % Time
% M = 2^21; % Space
per = false;
sigma = 1;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
%%
alpha = 1;
q = 1;
c = 1;
u0Fun = @(x) sqrt(2*alpha/q)*exp(0.5*1i*c*x).*sech(sqrt(alpha)*x);

H1norm = @(currU,dx) L2norm(currU,dx,per) + L2norm((currU(2:end)-currU(1:end-1))/dx,dx,per);

u0FunVal = u0Fun(modelInfo.x);
u0L2Val = L2norm(u0FunVal,modelInfo.dx,per);
u0H1Val = H1norm(u0FunVal,modelInfo.dx);
% Norm the initial value and add the exact values
% u0FunVal = u0FunVal / sqrt(u0L2Val);
% uExactSol = @(x,t) sqrt(2*alpha/q)*exp(1i*(0.5*c*x-(0.25*c^2-alpha)*t)).*sech(sqrt(alpha)*(x-c*t)) / sqrt(u0L2Val);
uExactSol = @(x,t) sqrt(2*alpha/q)*exp(1i*(0.5*c*x-(0.25*c^2-alpha)*t)).*sech(sqrt(alpha)*(x-c*t));
%plot(u0FunVal)

numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MEul
% schemesUsed(4) = true; % CN
% schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Perform calculations
rng(initSeed,'twister')
noKindsOfRefinement = 4;

% Store time
firstHalf = 0:modelInfo.h:(T/2);
secondHalf = (T/2+modelInfo.h/2):(modelInfo.h/2):T;
timeVec = [firstHalf secondHalf];
% Store L2 norm, H1 norm and L2 error
diagMat = zeros(noKindsOfRefinement,numUsedSchemes,3,length(timeVec));

diagMat(:,:,1,1) = u0L2Val*ones(noKindsOfRefinement,numUsedSchemes);
diagMat(:,:,2,1) = u0H1Val*ones(noKindsOfRefinement,numUsedSchemes);
diagMat(:,:,3,1) = zeros(noKindsOfRefinement,numUsedSchemes);

%%
for m = 1:noKindsOfRefinement
    internalSchemeIndexMat = schemeIndexMat;
    for j = 1:numUsedSchemes
        internalModelInfo = modelInfo;
        currU = u0FunVal;
        currScheme = internalSchemeIndexMat(j,2);
        t = 0;
        notRefined = true;
        i = 2;
        
        while t < T
%             dW = randn(2,1)*sqrt(internalModelInfo.h);
            dW = ones(2,1)*internalModelInfo.h/2;
            %% Scheme and query calculations
            currU = internalModelInfo.schemes.fun{currScheme}(currU,dW);
            
            % Perform refinement after fixed time, check the different
            % types of refinement
            switch m
                case 1
                % Linear interpolation
                interpolationType = 'linear';
                case 2
                % Fft interpolation
                interpolationType = 'fft';
                case 3
                % Piecewise Cubic Hermite Interpolating Polynomial
                % Good for interpolation of less oscillatory problems
                interpolationType = 'pchip';
                case 4
                % Cubic spline data interpolation
                % Good for interpolation of more oscillatory problems
                interpolationType = 'spline';
            end
            
            if t == T/2 && notRefined
                internalModelInfo = initModelInfo(internalModelInfo.N*2,TInt,internalModelInfo.M*2,XInt,sigma,per);

                currU = refineU(currU,per,interpolationType,modelInfo.x,internalModelInfo.x);
                
                notRefined = false;
            end

            t = t + internalModelInfo.h;

            diagMat(m,j,1,i) = L2norm(currU,internalModelInfo.dx,per);
            diagMat(m,j,2,i) = H1norm(currU,internalModelInfo.dx);
            diagMat(m,j,3,i) = L2norm(currU-uExactSol(internalModelInfo.x,t),internalModelInfo.dx,per);

            i = i+1;
            disp([m t/T])
            
%             figure(1)
%             plotProfile(internalModelInfo.x,currU,false)
%             title(["t = " num2str(t)])
%             set(gcf, 'Position', get(0, 'Screensize'));
%             axis([-65 65 -0.8 0.8 ])
%             
%             figure(2)
%             plotProfile(internalModelInfo.x,uExactSol(internalModelInfo.x,t),false)
%             title(["t = " num2str(t)])
%             set(gcf, 'Position', get(0, 'Screensize'));
%             axis([-65 65 -0.8 0.8 ])
%             pause(0.0001)
            
        end
    end
end
%%
figure
title("L2-norm")
plot(timeVec,squeeze(diagMat(:,1,1,:)))
legend({'linear', 'fft', 'pchip', 'spline'})
figure
title("H1-norm")
plot(timeVec,squeeze(diagMat(:,1,2,:)))
legend({'linear', 'fft', 'pchip', 'spline'})
figure
title("L2-error")
plot(timeVec,squeeze(diagMat(:,1,3,:)))
legend({'linear', 'fft', 'pchip', 'spline'})

pause(1)
% printToPDF(gcf,'H1Explosion')