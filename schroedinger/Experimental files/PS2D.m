% Time and area
rng(1,'twister')
L1 = 4*pi; 
L2 = 4*pi; 
XInt = [-L1,L1,-L2,L2];
T = 0.5; TInt = [0,T];

% Numerical precision, number of points
N = 2^12; % Time
M1 = 2^7; % Space d1
M2 = 2^7; % Space d2
% 2^6 gives short computational time for implicit schemes
% 2^7 is... painful, but possible
per = true;
sigma = 1;

modelInfo = initModelInfo2D(N,TInt,[M1 M2],XInt,sigma,per);

[x1Grid,x2Grid] = meshgrid(modelInfo.x,modelInfo.y);
u0Fun = @(x,y) exp(-5*(x.^2+y.^2));
% u0Fun = @(x,y) sin(x).*sin(y);
u0FunVal = u0Fun(x1Grid,x2Grid);
axisArgs = [XInt 0 1.1*max(u0FunVal(:))];
% figure(1)
% surf(x1Grid,x2Grid,currU)


dW = randn(2,N)*sqrt(T/N);
numSchemes = length(modelInfo.schemes.fun);
storage = zeros(M1,M2,numSchemes);
j = 7
% for j = 1:numSchemes
    currU = fft2(u0FunVal);
    for i = 1:N
        currDW = dW(:,i);
        currU = modelInfo.schemes.fun{j}(currU,currDW);
        figure(j)
        tempCurrU = ifft2(currU);
        surf(x1Grid,x2Grid,abs(tempCurrU))
        axis(axisArgs)
        pause(0.1)
    [j i/N]
    end
    storage(:,:,j) = currU;
% end
%%
for j = 1:numSchemes
    figure(j)
    tempCurrU = ifft2(storage(:,:,j));
    surf(x1Grid,x2Grid,abs(tempCurrU))
    axis(axisArgs)
    pause(0.1)
end

% h = (TInt(2)-TInt(1))/N;
% dx1 = (XInt(2)-XInt(1))/M1;
% dx2 = (XInt(4)-XInt(3))/M2;
% 
% x1 = XInt(1) + dx1*(0:M1-1);
% x2 = XInt(3) + dx2*(0:M2-1);
% k1 = 2*pi/(XInt(2)-XInt(1))*[0:M1/2-1, 0, -M1/2+1:-1];
% k2 = 2*pi/(XInt(4)-XInt(3))*[0:M2/2-1, 0, -M2/2+1:-1];
% 
% % u0Fun = @(x,y) 4.5*exp(-2*x.^2-2*y.^2);
% u0Fun = @(x,y) sin(x).*sin(y);
% % u0Fun = @(x,y) exp(-x.^2-y.^2);
% % testSol = @(x,y) -2*(x+y).*u0Fun(x,y);
% % testSol = @(x,y) 4*((x.^2+y.^2)-1).*u0Fun(x,y);
% testSol = @(x,y) -2*sin(x).*sin(y);
% 
% [x1Grid,x2Grid] = meshgrid(x1,x2);
% u0FunVal = u0Fun(x1Grid,x2Grid);
% testSolVal = testSol(x1Grid,x2Grid);
% 
% u0FunValFft = fft2(u0FunVal);
% 
% [k1Grid,k2Grid] = meshgrid(k1,k2);
% 
% 
% % testDeriv = 1i*(k1Grid+k2Grid).*u0FunValFft;
% testDeriv = -(k1Grid.^2+k2Grid.^2).*u0FunValFft;
% % testDeriv = (exp(1i*2*pi*k1/M1) - 1).^2.*(exp(1i*2*pi*k2/M2) - 1).^2.*u0FunValFft;
% testDeriv = ifft2(testDeriv);
% 
% figure(1)
% surf(x1Grid,x2Grid,u0FunVal)
% title('Function')
% figure(2)
% surf(x1Grid,x2Grid,real(testDeriv))
% title('Numerical 2nd order derivative')
% figure(3)
% surf(x1Grid,x2Grid,testSolVal)
% title('Theoretical 2nd order derivative')
% figure(4)
% surf(x1Grid,x2Grid,abs(testDeriv-testSolVal))
% title('Derivative difference')