addpath('Functions')
%% Set initial info and function
initSeed = 1;
batchSize = 10^4;
confLevel = 0.95;
% Time and area
L = 10; XInt = [-L,L];
T = 1/4; TInt = [0,T];

% Numerical precision, number of points
N = 2.^(3:11); % Time
numN = length(N);
M = 2^7; % Space
refN = 2^16; % Time (used for true mean)
refM = 2^7; % Space (used for true mean)

u0Fun = @(x) exp(-3*x.^2);
per = true;
C = 50;

sigma = 1;

refModel = initModelInfoScaling(refN,TInt,refM,XInt,sigma,per,C);
% refModel = initModelInfo(refN,TInt,refM,XInt,sigma,per);
refH = refModel.h;
numAvailableSchemes = length(refModel.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MP
schemesUsed(4) = true; % CN
schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];
%% Query definitions
% The different weak errors we will compare, currU and refU will be in Fourier space
weakErrorQuery = cell(0,0);
weakErrorQueryNames = cell(0,0);
weakErrorQueryFileNames = cell(0,0);
% L2-norm of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(real(ifft(currU)),dx,per);
weakErrorQueryNames{end+1} = '$e_1(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrRealPartL2';

% L2-norm of gaussian filter
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(exp(-(ifft(currU)).^2),dx,per);
weakErrorQueryNames{end+1} = '$e_2(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrGaussL2';

% L2-norm of sin of real part
weakErrorQuery{end+1} = @(currU,dx,k) L2norm(sin(real(ifft(currU))),dx,per);
weakErrorQueryNames{end+1} = '$e_3(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrSinRealPartL2';

% Energy
weakErrorQuery{end+1} = @(currU,dx,k) energyShroed(currU,dx,k,per,sigma);
weakErrorQueryNames{end+1} = '$e_4(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrEnergy';

% H1 norm
weakErrorQuery{end+1} = @(currU,dx,k) H1norm(currU,dx,k,per);
weakErrorQueryNames{end+1} = '$e_5(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrH1';

% Odd stuff
weakErrorQuery{end+1} = @(currU,dx,k) abs(cos(abs(currU(5))));
weakErrorQueryNames{end+1} = '$e_6(N)$';
weakErrorQueryFileNames{end+1} = 'weakErrOdd';

numWeaks = length(weakErrorQuery);

%% Query storage
% If query storage exists, load it. Otherwise, create it
if isfile('weakErrorEstimateLINEAR.mat')
    load('weakErrorEstimateLINEAR.mat');
    % If the size is smaller than the batch size, resize storage
    % This assumes that no new N, schemes or queries have been added
    numStoredElements = size(estimateBatch,1);
    if(numStoredElements < batchSize)
        temp = zeros(batchSize,length(N),numUsedSchemes,numWeaks);
        temp(1:numStoredElements,:,:,:) = estimateBatch;
        estimateBatch = temp;
    end
else
    estimateBatch = zeros(batchSize,length(N),numUsedSchemes,numWeaks);
end

%% Perform calculations
rng(initSeed,'twister')
parfor m = 1:batchSize
    % Only perform the simulation no estimate has been made earlier
    temp = estimateBatch(m,:,:,:);
    runSim = all(temp(:) == 0);
    if runSim
        % Load the broadcast variables to internal
        internalSchemeIndexMat = schemeIndexMat;
        internalN = N;
        internalWeakErrorQuery = weakErrorQuery;

        % Reference brownian motion
        refW = randn(refN,2)*sqrt(refH/2);

        tempEstBatch = zeros(length(N),numUsedSchemes,numWeaks);
        for n = 1:length(N)
            % Retrieve information regarding current number of steps
            currN = internalN(n);
            scalingFactor = refN/currN;
            currModel = initModelInfoScaling(currN,TInt,M,XInt,sigma,per,C);
%             currModel = initModelInfo(currN,TInt,M,XInt,sigma,per);

            % Calculate the coarser Brownian motion increments
            coarseDW = zeros(2,currN);
            currIndex = 0;
            for i = 1:currN
                indexList = (currIndex+1):(currIndex+scalingFactor/2);
                coarseDW(1,i) = sum(sum(refW(indexList,:)));
                indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
                coarseDW(2,i) = sum(sum(refW(indexList,:)));
                currIndex = currIndex + scalingFactor;
            end

            for j = 1:numUsedSchemes
                currU = fft(u0Fun(currModel.x));
                currScheme = internalSchemeIndexMat(j,2);
                dW = zeros(2,1);

                for i = 1:currN
                    dW = coarseDW(:,i);
                    %% Scheme and query calculations
                    currU = currModel.schemes.fun{currScheme}(currU,dW);
                end
                for i = 1:numWeaks
                    tempEstBatch(n,j,i) = internalWeakErrorQuery{i}(currU,currModel.dx,currModel.k);
                end

            end
        end
        estimateBatch(m,:,:,:) = tempEstBatch;
        m
    end
end

%% Save the data
save('weakErrorEstimateLINEAR.mat','estimateBatch','-v7.3')

%% Load the data if necessary
load('weakErrorEstimateLINEAR.mat')
load('weakErrorReferenceLINEAR.mat')
%% Square root of L2
% estimateBatch(:,:,:,1) = sqrt(estimateBatch(:,:,:,1));
% refBatch(:,1) = sqrt(refBatch(:,1));

%% Calculate the CI - Monte Carlo estimation
numMeans = 10^2;
perc = abs(norminv((1-confLevel)/2,0,1));

% Weak error mean estimate
numMeansInMean = batchSize/numMeans;
weakMeanMat = zeros(numMeans,numN,numUsedSchemes,numWeaks);
for i = 1:numMeans
    for n = 1:numN
        for scheme = 1:numUsedSchemes
            for j = 1:numWeaks
                weakMeanMat(i,n,scheme,j) = mean(estimateBatch((i-1)*numMeansInMean+(1:numMeansInMean),n,scheme,j));
            end
        end
    end
end

% Reference mean estimate
numRefMeansInMean = size(refBatch,1)/numMeans;
weakRefMat = zeros(numMeans,numWeaks);
for i = 1:numMeans
    for j = 1:numWeaks
        weakRefMat(i,j) = mean(refBatch((i-1)*numRefMeansInMean+(1:numRefMeansInMean),j));
    end
end

weakMeanEst = mean(weakMeanMat);
weakStdEst = std(weakMeanMat);

weakCIEst = zeros(2,numN,numUsedSchemes,numWeaks);
weakCIEst(1,:,:,:) = weakMeanEst - perc*weakStdEst/sqrt(numMeans);
weakCIEst(2,:,:,:) = weakMeanEst + perc*weakStdEst/sqrt(numMeans);

meanEst = mean(weakRefMat);
stdEst = std(weakRefMat);

CIEst = zeros(2,numWeaks);
CIEst(1,:) = meanEst - perc*stdEst/sqrt(numRefMeansInMean);
CIEst(2,:) = meanEst + perc*stdEst/sqrt(numRefMeansInMean);


%% Plot convergence
supportLineSlope = {[-1/2 -1 -2] 
    [-1/2 -1 -1.5]
    [-1/2 -1 -2] 
    [-1/2 -2] 
    [-1/2 -2]
    [-1/2 -2]};
supportLineTranslation = {[10^-3.5 10^-2.5 10^-4]
    [10^-3.75 10^-3.5 10^-3.125]
    [10^-3.5 10^-2.5 10^-4]
    [10^-3.5 10^-4]
    [10^-3.5 10^-4]
    [10^-3.5 10^-4]};



for j = 1:numWeaks
    % Estimate degrees of freedom
    % (difference of two normally distributed random variables)
    degreesOfFreedom = 1./((stdEst(j)^4 + weakStdEst(:,:,:,j).^4)/(numMeans-1)./(stdEst(j)^2+weakStdEst(:,:,:,j).^2).^2);
    perc = squeeze(abs(tinv((1-confLevel)/2,degreesOfFreedom)));

    tempMean = squeeze(abs(weakMeanEst(:,:,:,j) - meanEst(j)));
    tempStd = squeeze(sqrt(weakStdEst(:,:,:,j).^2 + stdEst(j)^2));

    numSuppLines = length(supportLineSlope{j});
    legendInArg = cell(numSuppLines,1);
    logInArg = cell(3*numSuppLines,1);
    for i = 1:numSuppLines
        supportLine = (N.^supportLineSlope{j}(i))/(N(1)^supportLineSlope{j}(i))*supportLineTranslation{j}(i);
        logInArg(((i-1)*3+1):((i-1)*3+3)) = {N,supportLine,'--k'};
        legendInArg{i} = ['CN^{' num2str(supportLineSlope{j}(i)) '}'];
    end

    figure(j)
    clf
    hold on
    for i = 1:numUsedSchemes
        switch schemeIndexMat(i,2)
            case 1
                line = '-^b';
            case 2
                line = '-vm';
            case 3
                line = '-*';
            case 4
                line = '-*';
            case 5
                line = '-pc';
            case 6
                line = '-og';
            otherwise
                line = '-x';
        end
        temp = errorbar(N,tempMean(:,i),perc(:,i).*tempStd(:,i)/sqrt(batchSize),line,'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'));
        temp.CapSize = 15;
    end

    set(gca,'xscale','log');
    set(gca,'yscale','log');
    loglog(logInArg{:})
    legend(refModel.schemes.shortNames{schemesUsed},legendInArg{:},'Location','SouthWest')
    hold off

    xlabel('N')
    ylabel(weakErrorQueryNames{j},'Interpreter','latex')
    set(gca,'FontSize',fontAndMarkerSize('Font','Half'))
    set(gcf,'units','normalized','outerposition',[0 0 1 1])

    pause(1)
%     printToPDF(gcf,weakErrorQueryFileNames{j})
end