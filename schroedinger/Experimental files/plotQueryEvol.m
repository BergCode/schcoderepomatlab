function plotQueryEvol(modelInfo,normStorage,TInt,schemeIndexMat,legendMarks,title)
    timeFidelity = length(normStorage{1,1});
    coarseTVec = linspace(TInt(1),TInt(2),timeFidelity);
    
    numUsedSchemes = length(normStorage);
    inArg = cell(3*numUsedSchemes,1);
    legendInArg = cell(numUsedSchemes,1);

    % Find the maximum drifts up and down
    maxUpwardDrift = 0;
    maxDownwardDrift = 0;
    for i = 1:numUsedSchemes
        inArg(3*i-2:3*i) = {coarseTVec,normStorage{i},legendMarks{i}};
        % Retrieve info of which scheme was ran
        usedScheme = schemeIndexMat(i,2);
        % Retrieve legend info
        legendInArg(i) = modelInfo.schemes.shortNames(usedScheme);
        % Retrieve extreme values
        driftNormVector = normStorage{i}-normStorage{i}(1);
        driftUpwardIndex = driftNormVector >= 0;

        schemeDriftUpward = max(driftNormVector(driftUpwardIndex));
        schemeDriftDownward = min(driftNormVector(~driftUpwardIndex));

        if maxUpwardDrift < schemeDriftUpward
            maxUpwardDrift = schemeDriftUpward;
        end
        if maxDownwardDrift > schemeDriftDownward
            maxDownwardDrift = schemeDriftDownward;
        end
    end

    % Make sure that the plot window won't touch the lines
    if -maxDownwardDrift < 0.1*maxUpwardDrift
        maxDownwardDrift = -0.1*maxUpwardDrift;
    elseif -0.1*maxDownwardDrift > maxUpwardDrift
        maxUpwardDrift = -0.1*maxDownwardDrift;
    end

    plot(inArg{:},'LineWidth',2.5,'MarkerSize',fontAndMarkerSize('Marker','Half'))
    legend(legendInArg{:},'Location','northwest')
    ylabel(title,'Interpreter','latex')
    xlabel('t');
    set(gca,'FontSize',fontAndMarkerSize('Font','Half'))

    axis([TInt normStorage{1}(1)+1.1*maxDownwardDrift normStorage{1}(1)+1.1*maxUpwardDrift])
    set(gcf, 'Position', get(0, 'Screensize'));
end