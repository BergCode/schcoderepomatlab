%% Refinement in space only
%% Set initial info and function
initSeed = 4;
batchSize = 4;
confLevel = 0.95;
% Time and area
L = 5*pi; XInt = [-L,L];
% XInt = [0,2*pi];
T = 1; TInt = [0,T];

% Numerical precision, number of points
N = 2^10; % Time
refN = 2^15;
M = 2^10; % Space
% M = 2^21; % Space
refM = 2^16;
spaceFactor = refM/M;
per = true;
sigma = 4;

modelInfo = initModelInfo(N,TInt,M,XInt,sigma,per);
refinedModelInfo = initModelInfo(N,TInt,2*M,XInt,sigma,per);
referenceModelInfo = initModelInfo(refN,TInt,refM,XInt,sigma,per); 
%%
u0Fun = @(x) exp(-x.^2);

u0FunVal = u0Fun(modelInfo.x);
u0FftFunVal = fft(u0FunVal);
u0RefinedFunVal = u0Fun(refinedModelInfo.x);
u0RefinedFftFunVal = fft(u0RefinedFunVal);
u0RefVal = u0Fun(referenceModelInfo.x);
u0FftRefFunVal = fft(u0RefVal);

numAvailableSchemes = length(modelInfo.schemes.fun); % 9 for this

schemesUsed = false(numAvailableSchemes,1);
% schemesUsed(1) = true; % FEul
% schemesUsed(2) = true; % BEul
% schemesUsed(3) = true; % MEul
% schemesUsed(4) = true; % CN
% schemesUsed(5) = true; % EExp
% schemesUsed(6) = true; % SExp
schemesUsed(7) = true; % LTSpl
% schemesUsed(8) = true; % FSpl
% schemesUsed(9) = true; % SSpl

numUsedSchemes = sum(schemesUsed);
schemeIndexMat = [(1:numUsedSchemes)' , find(schemesUsed)];

%% Query storage
refinementNames = {'Linear','Fft','Pchip', 'Spline', 'Custom'};
noKindsOfRefinement = length(refinementNames);

noQueries = 3;
query = cell(noQueries,2); % Function evaluating query and boolean to determine physical or Fourier space
    % L2 norm
query{1,1} = @(u,model,uRef) L2norm(u,model.dx,per);
query{1,2} = true;
    % H1 norm
query{2,1} = @(u,model) H1norm(u,model.dx,model.k,per);
query{2,2} = false;
    % L2 difference
query{3,1} = @(u,model,uRef) L2norm(u-uRef,model.dx,per);
query{3,2} = true;

% Initialize storage
diagMat = zeros(batchSize,N+1,noKindsOfRefinement,numUsedSchemes,noQueries,2);
initSize = size(diagMat(:,1,:,:,1,:));
initMat = ones(initSize);

% for k = 1:noQueries
%     if query{k,2}
%         queryVal = query{k,1}(u0FunVal,modelInfo,u0RefVal(1:spaceFactor:end));
%     else
%         queryVal = query{k,1}(u0FftFunVal,modelInfo);
%     end
%     diagMat(:,1,:,:,k,:) = initMat*queryVal;
% end

% storedRefinement = cell(noKindsOfRefinement,4);

%% Perform calculations
rng(initSeed,'twister')
parfor m = 1:batchSize
    % Load internal variables
    sampleMat = diagMat(m,:,:,:,:,:);
    internalQueries = query;
    internalSchemeIndexMat = schemeIndexMat;
    
    internalModelInfo = modelInfo;
    internalRefinedModelInfo = refinedModelInfo;
    internalReferenceModelInfo = referenceModelInfo;
    
    internalRefNames = refinementNames;
    
    u0Val = u0FftFunVal;
    u0Ref = u0FftRefFunVal;
    
    for k = 1:noKindsOfRefinement
        for j = 1:numUsedSchemes
            currScheme = internalSchemeIndexMat(j,2);
            
            coarseCurrU = u0Val;
            refinedCurrU = 0;
            tempRefinedCurrU = u0RefinedFunVal;
            refCurrU = u0FftRefFunVal;
            
            t = 0;
            notRefined = true;
            numStepsForRef = refN/N;
            spaceFactor = refM/M;
            i = 2;

            while t < T
                %% Scheme and query calculations
%                 dW = randn(2,numStepsForRef)*sqrt(internalReferenceModelInfo.h);
                dW = ones(2,numStepsForRef)*(internalReferenceModelInfo.h/2);
                coarsedW = sum(dW,2);
                % Perform reference steps first and then one actual step
                % Lastly, calculate query
                if notRefined
                    for w = 1:numStepsForRef
                        dWStep = dW(:,w);
                        refCurrU = internalReferenceModelInfo.schemes.fun{currScheme}(refCurrU,dWStep);
                    end
                    coarseCurrU = internalModelInfo.schemes.fun{currScheme}(coarseCurrU,coarsedW);
                    
                    tempRefCurrU = ifft(refCurrU);
                    tempCoarseCurrU = ifft(coarseCurrU);
                    
                    % Calculate the queries
                    for q = 1:noQueries
                        if internalQueries{q,2}
                            coarseQueryVal = internalQueries{q,1}(tempCoarseCurrU,internalModelInfo,tempRefCurrU(1:spaceFactor:end));
                        else
                            coarseQueryVal = internalQueries{q,1}(coarseCurrU,internalModelInfo);
                        end
                        sampleMat(1,i,k,j,q,1) = coarseQueryVal;
                        sampleMat(1,i,k,j,q,2) = coarseQueryVal;
                    end
                else
                    for w = 1:numStepsForRef
                        dWStep = dW(:,w);
                        refCurrU = internalReferenceModelInfo.schemes.fun{currScheme}(refCurrU,dWStep);
                    end
                    coarseCurrU = internalModelInfo.schemes.fun{currScheme}(coarseCurrU,coarsedW);
                    refinedCurrU = internalRefinedModelInfo.schemes.fun{currScheme}(refinedCurrU,coarsedW);
                    
                    tempRefCurrU = ifft(refCurrU);
                    tempCoarseCurrU = ifft(coarseCurrU);
                    tempRefinedCurrU = ifft(refinedCurrU);
                    
                    % Calculate the queries
                    for q = 1:noQueries
                        if internalQueries{q,2}
                            coarseQueryVal = internalQueries{q,1}(tempCoarseCurrU,internalModelInfo,tempRefCurrU(1:spaceFactor:end));
                            refinedQueryVal = internalQueries{q,1}(tempRefinedCurrU,internalRefinedModelInfo,tempRefCurrU(1:spaceFactor/2:end));
                        else
                            coarseQueryVal = internalQueries{q,1}(coarseCurrU,internalModelInfo);
                            refinedQueryVal = internalQueries{q,1}(refinedCurrU,internalRefinedModelInfo);
                        end
                        sampleMat(1,i,k,j,q,1) = coarseQueryVal;
                        sampleMat(1,i,k,j,q,2) = refinedQueryVal;
                    end
                end
                t = t + internalModelInfo.h;
                
                % Perform refinement after fixed time
                if t == T/2 && notRefined
                    % k determines what refinement is used
                    tempRefinedCurrU = refineU(tempCoarseCurrU,per,internalRefNames{k},internalModelInfo.x,internalRefinedModelInfo.x);

                    refinedCurrU = fft(tempRefinedCurrU);

%                     storedRefinement{k,1} = tempCoarseCurrU;
%                     storedRefinement{k,2} = tempRefinedCurrU;
%                     storedRefinement{k,3} = coarseCurrU;
%                     storedRefinement{k,4} = refinedCurrU;
                    
                    notRefined = false;
                end
                i = i+1;
                
%                 figure(1)
%                 plot(tempCoarseCurrU)
%                 pause(0.01)

                fprintf(['-------'...
                    '\n Sample: ', num2str(m), ...
                    '.\n Scheme: ', num2str(j), ...
                    '.\n Percentage until max time: ', num2str(t/T), ...
                    '.\n Refinement method: ', num2str(k),...
                    '.\n-------\n'])
            end
        end
    end
    diagMat(m,:,:,:,:,:) = sampleMat;
end
%%
timeVec = linspace(0,T,N+1);
meanMat = mean(diagMat,1);

figure(1)
plot(timeVec,squeeze(mean(diagMat(:,:,:,:,1,2))),timeVec,squeeze(mean(diagMat(:,:,1,1,1,1))))
% plot(timeVec,squeeze(meanMat(:,:,:,1,1)),timeVec,squeeze(meanMat(:,:,1,1,1)))
title("L2-norm")
legend([refinementNames 'Unrefined'],'Location','southwest')
set(gcf, 'Position', get(0, 'Screensize'));
set(gca,'FontSize',35)
pause(1)
printToPDF(gcf,'L2normRefinementPS')

% Zoom in to refinement moment
% horZoom = 0.001;
% verZoom = 0.0001;
% axis([0.5-horZoom 0.5+horZoom (1-verZoom)*diagMat(1,1,1,N/2,2) (1+verZoom)*diagMat(1,1,1,N/2,2)])
% title("L2-norm zoomed")
% pause(1)
% printToPDF(gcf,'L2normRefinementZoomPS')


figure(2)
plot(timeVec,squeeze(mean(diagMat(:,:,:,:,2,2))),timeVec,squeeze(mean(diagMat(:,:,1,1,2,1))))
title("H1-norm")
legend([refinementNames 'Unrefined'],'Location','southwest')
set(gcf, 'Position', get(0, 'Screensize'));
set(gca,'FontSize',35)
pause(1)
printToPDF(gcf,'H1normRefinementPS')

% % Zoom in to refinement moment
% horZoom = 0.001;
% verZoom = 0.0000005;
% axis([0.5-horZoom 0.5+horZoom (1-verZoom)*diagMat(1,1,2,N/2,2) (1+verZoom)*diagMat(1,1,2,N/2,2)])
% title("H1-norm zoomed")
% pause(1)
% % printToPDF(gcf,'H1normRefinementZoomPS')


figure(3)
plot(timeVec,squeeze(mean(diagMat(:,:,:,:,3,2))),timeVec,squeeze(mean(diagMat(:,:,1,1,3,1))))
title("L2-error")
legend([refinementNames 'Unrefined'],'Location','southwest')
set(gca,'yscale','log');
set(gcf, 'Position', get(0, 'Screensize'));
set(gca,'FontSize',35)
pause(1)
printToPDF(gcf,'L2ErrorRefinementPS')

% % Zoom in to refinement moment
% horZoom = 0.000005;
% verZoom = 0.00001;
% axis([0.5-T/N-horZoom 0.5-T/N+horZoom (1-verZoom)*diagMat(1,1,3,N/2,2) (1+verZoom)*diagMat(1,1,3,N/2,2)])
% set(gca,'yscale','log');
% title("L2-error zoomed")
% pause(1)
% % printToPDF(gcf,'L2ErrorRefinementZoomPS')

%%
% tempRefinedCurrU = ;
% refinedCurrU = fft(tempRefinedCurrU);
%
figure(4)
set(gcf, 'Position', get(0, 'Screensize'));
% Not refined
subplot(3,2,1)
temp = abs(u0FftFunVal);
plot(modelInfo.k,temp,'.')

axisInfo = 1.1*[min(refinedModelInfo.k) max(refinedModelInfo.k) 0 max(temp)];
% axisInfo = [-250,250,0,200];
axis(axisInfo)
title('Unrefined')
set(gca,'FontSize',35)
set(gca,'yscale','log')

for k = 1:noKindsOfRefinement
    % Choose interpolation
    % Calculate the refinement
    temp = refineU(u0FunVal,per,refinementNames{k},modelInfo.x,refinedModelInfo.x);
    temp = fft(temp);
    % Plot the frequencies
    subplot(3,2,k+1)
    temp = abs(temp);
    plot(refinedModelInfo.k,temp,'.')
    axis(axisInfo)
    title(refinementNames{k})
    set(gca,'FontSize',35)
    set(gca,'yscale','log')
end
figure(4)
% printToPDF(gcf,'PSFrequencyRefinement')