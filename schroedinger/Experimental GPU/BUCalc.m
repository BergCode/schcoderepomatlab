function BUCalc(batchSize,L,uFun,NVec,MVec,TVec,dataName,varargin)
% BUCalc - Tracks H1-norm, L2-norm, and absolute value of edges of 
%          evolution over [-L,L] with the defined parameters N, M, and T. 
%          Initial values defined by user.
%          Saves results and backups in .mat-format.
%          Backups of partial steps (n <= N) also saved in intervals.
% Syntax: BUCalc(batchSize,L,uFun,NVec,MVec,TVec,dataName,varargin)
%
% Input:
% batchSize - Positive integer defining how many samples are sought.
%             Will not run samples already present, as defined by output.
% L         - A positive scalar value defining half interval length.
% uFun      - An initial values in form of a complex valued anonymous function.
% NVec      - An integer vector defining time discretization.
% MVec      - An integer vector defining spatial discretization.
%             Multiples of 2 recommended for speed.
% TVec      - A scalar vector of positive values defining end times.
% dataName  - String containing identifying part of name for sample files.
% varargin  - 1. Boolean input. False will disable stochastics. Default true.
%             2. Noise strength. Default gamma = 1.
%             3. Nonlinearity strength. Default sigma = 1.
%             4. Control over number of max time steps storing norm. Default 2^10.
%             5. Value for ceiling of H1 norm (abort if exceeded). Default Inf.
%             6. Number of backups (within simulation). Needs to be a
%             divisor to all choices of N. Default 0.
%             
%
% Output:
% Completed samples saved as ['Data\' dataname num2str(sampleNo) '.mat']
% (see variable normTracking)

%% Import the varargins
% Set the default values
stoch = true; gamma = 1; sigma = 1; maxNumStore = 2^10; H1Limit = Inf; numBackups = 0;

if nargin > 7
    for i = 1:(nargin-7)
        switch i
            case 1 % Stochastic or deterministic?
                stoch = varargin{i};
            case 2 % Noise strength
                gamma = varargin{i};
            case 3 % Nonlinearity power
                sigma = varargin{i};
            case 4 % Maximum norm storages (limiting memory usage)
                maxNumStore = varargin{i};
            case 5 % Limit for H1 norm (for aborting calculations)
                H1Limit = varargin{i};
            case 6 % Fetch how many backups will be done
                numBackups = varargin{i};
        end
    end
end

%% Generate all possible settings
[temp1,temp2,temp3] = meshgrid(NVec,MVec,TVec);
settings = [temp1(:),temp2(:),temp3(:)];

numSettings = size(settings,1);

%% Perform calculations
parfor m = 1:batchSize
    % Load external variables in order to enable parfor
    settings;
    dataName;
    uFun;
    
    % Check if this sample has been run to completion. Don't run in that
    % case
    completeFileName = ['Data\' dataName num2str(m) '.mat'];
    if ~isfile(completeFileName)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%Backup import%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Check if backup has been saved for current sample, if so: load it
        backUp1FileName = ['Data\' dataName 'BackupPrim' num2str(m) '.mat'];
        % Backup 2 only present in case of crash during saving.
        % MANUAL HANDLING IN THAT CASE
        backUp2FileName = ['Data\' dataName 'BackupSec' num2str(m) '.mat'];
        % Backup during runtime
        backUpRun1 = ['Data\' dataName 'BackupRunPrim' num2str(m) '.mat'];
        % Backup 2 only present in case of crash during saving.
        % MANUAL HANDLING IN THAT CASE
        backUpRun2 = ['Data\' dataName 'BackupRunSec' num2str(m) '.mat'];
        % Set how many seconds the process is pausing when backing up
        backUpPause = 1;
        
        % If the backup is there, load it and run where it is zero
        if isfile(backUp1FileName)
            temp = load(backUp1FileName);
            normTracking = temp.variable;
        else
            % Whether a setting has been ran or not
            normTracking.SettingRan = false(numSettings,1);
            % Initialize norm storage
            normTracking.H1 = cell(numSettings,1);
            normTracking.L2 = cell(numSettings,1);
            % Tracking edge value of each piece
            normTracking.absEdge = cell(numSettings,1);
        end
         
        % Loop over all settings. Skip over settings which have been ran.
        % Note that settings are run in sequence
        numRanSettings = sum(normTracking.SettingRan);
        
        for setting = (numRanSettings+1):numSettings
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%Parameter and model definition%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            N = settings(setting,1);
            M = settings(setting,2);
            T = settings(setting,3);
            
            % Simulate the fine Brownian motion
            rng(m,'twister')
            maxN = max(NVec);
            if stoch
                dWFine = gamma*sqrt(T/maxN)*randn(maxN,1);
            else
                % If deterministic, then set noise to zero
                dWFine = zeros(maxN,1);
            end
            
            % Calculate spatial and time info
            model = initModelInfo(N,[0,T],M,[-L,L],sigma,true);
            scheme = model.schemes.fun{7};
            % Now remove the schemes, since they take up memory for large M
            model = rmfield(model,'schemes');

            % Define which norms we will be interested in
            internalL2norm = @(currU) L2norm(currU,model.dx,true,false,2*L);
            internalH1norm = @(currU) H1norm(currU,model.dx,model.k,true,false,2*L);

            % If we are not using the finest time discretization, calculate the
            % coarser Brownian motion increments
            NRatio = maxN / N;
            if NRatio ~=1
                dWCoarse = zeros(N,3);
                for i = 1:N
                    indexList = (i-1)*NRatio + (1:NRatio);
                    dWCoarse(i,:) = sum(dWFine(indexList,:));
                end
            else
                dWCoarse = dWFine;
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%Initiation%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%
            % Calculate norm storage size
            % In case of too large N, store only every few values
            if N <= maxNumStore
                trackEveryStep = true;
                trackIndex = 1;
                
                normStorageLen = N+1;
            else
                trackRatio = N / maxNumStore;
                trackEveryStep = false;
                trackIndex = 1;
                
                normStorageLen = maxNumStore + 1;
            end
            
            % Calculate the ratio of backups
            if N <= numBackups
                backups = true;
                backupRatio = 1;
            elseif numBackups == 0
                backups = false;
                backupRatio = 1; % Doesn't matter
            else
                backups = true;
                backupRatio = N/numBackups;
            end
            
            % Now that we have loaded the potential done results and we know
            % what setting we are in we want to load potential partial results.
            % If we have a partial result, we have the current:
            % Time step, function (complex vector), norms 
            % (L2, H1, edge value, cumulative max edge)
            if isfile(backUpRun1)
                temp = load(backUpRun1);
                % Check if desync has occurred. Settings must match. If
                % current setting is higher, just throw away the backup.
                if setting > temp.variable.setting
                    delete(backUpRun1)
                end
            end
            if isfile(backUpRun1)
                temp = load(backUpRun1);
                
                startStep = temp.variable.i;
                
                currU = temp.variable.currU;
                
                normTracking.L2{setting} = temp.variable.L2;
                normTracking.H1{setting} = temp.variable.H1;
                normTracking.absEdge{setting} = temp.variable.absEdge;
                cumMaxEdge = temp.variable.cumMaxEdge;
            else
                startStep = 1;

                % Initial value
                currU = fft(uFun(model.x));
                tempReal = ifft(currU);
                
                % Initialize storage and calculate initial norms
                normTracking.H1{setting} = zeros(normStorageLen,1);
                normTracking.L2{setting} = zeros(normStorageLen,1);
                normTracking.H1{setting}(1) = internalH1norm(currU);
                normTracking.L2{setting}(1) = internalL2norm(currU);

                % Absolute value at edge
                normTracking.absEdge{setting} = zeros(normStorageLen,1);
                normTracking.absEdge{setting}(1) = abs(tempReal(1));
                cumMaxEdge = 0;
            end
            
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%Simulation%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%
            for i = startStep:N
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%Scheme stepping%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                currU = scheme(currU,dWCoarse(i));
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%Norm storage %%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Calculate at each step (due to cumilative maximum)
                % L2 edge norms
                tempReal = ifft(currU);
                % Absolute value at edge
                currEdge = abs(tempReal(1));
                cumMaxEdge = max(cumMaxEdge,currEdge);

                % Calculate only certain norms when necessary
                if trackEveryStep || (mod(i,trackRatio) == 0)
                    % Step tracking index
                    trackIndex = trackIndex + 1;
                    
                    % Calculate norms
                    currH1Val = internalH1norm(currU); % Used in print

                    % H1 and L2 norms
                    normTracking.H1{setting}(trackIndex) = currH1Val;
                    normTracking.L2{setting}(trackIndex) = internalL2norm(currU);

                    % Absolute value at edge
                    normTracking.absEdge{setting}(trackIndex) = cumMaxEdge;
                    
                    % If the H1 norm has a cap, check if exceeded
                    if  currH1Val > H1Limit
                        break
                    end
                end
                %%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%Backup %%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%
                % Determine whether we want to backup this step.
                if backups && (mod(i,backupRatio) == 0)
                    % For some reason parfor complains if not in separate
                    % function....
                    backup = storeBackupInRun(i,currU,normTracking.L2{setting},...
                        normTracking.H1{setting},normTracking.absEdge{setting},cumMaxEdge,setting)

                    % Backup the results (as well as the backup itself)
                    if isfile(backUpRun1)
                        temp = load(backUpRun1);
                        parforSave(backUpRun2,temp.variable);
                        pause(backUpPause)
                    end
                    parforSave(backUpRun1,gather(backup));
                    pause(backUpPause)
                    % Remove second backup, now that primary backup worked
                    if isfile(backUpRun2)
                        delete(backUpRun2)
                    end
                    pause(backUpPause)
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%Process tracking%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                fprintf(['\n-------'...
                    '\n Sample: ', num2str(m), ...
                    '. N: 2^{', num2str(log2(N)) '}', ...
                    '. M: 2^{', num2str(log2(M)) '}', ...
                    '. T: ', num2str(T), ...
                    '. Setting: ', num2str(setting), ...
                    ' (of ', num2str(numSettings) ')', ...
                    '\n Current H1 value: ', num2str(currH1Val), ...
                    '\n Percentage until max time: ', num2str(i/N)]);
                fprintf('.\n-------\n');
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%Process saving & backup%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            normTracking.SettingRan(setting) = true;
            % Backup the results (as well as the backup itself)
            if isfile(backUp1FileName)
                temp = load(backUp1FileName);
                parforSave(backUp2FileName,temp.variable);
                pause(backUpPause)
            end
            parforSave(backUp1FileName,gather(normTracking));
            pause(backUpPause)
            % Remove second backup, now that primary backup worked
            if isfile(backUp2FileName)
                delete(backUp2FileName)
            end
            pause(backUpPause)
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%Removing in-process backup%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Remove backups
            if isfile(backUpRun1)
                delete(backUpRun1)
            end
            if isfile(backUpRun2)
                delete(backUpRun2)
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%Sample done & cleanup%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Save complete sample. Pause
        parforSave(completeFileName,gather(normTracking))
        pause(backUpPause)
        % Remove backups
        if isfile(backUp1FileName)
            delete(backUp1FileName)
        end
        if isfile(backUp2FileName)
            delete(backUp2FileName)
        end
    end
end
end

function backup = storeBackupInRun(currStep,currU,currL2Vec,currH1Vec,currAbsEdge,cumMaxEdge,setting)
    backup.i = currStep;
    backup.currU = currU;
    backup.L2 = currL2Vec;
    backup.H1 = currH1Vec;
    backup.absEdge = currAbsEdge;
    backup.cumMaxEdge = cumMaxEdge;
    % Keep track of which setting it is calculated in. If
    % result saving is interrupted, then the backup may be
    % desynced.
    backup.setting = setting;
end