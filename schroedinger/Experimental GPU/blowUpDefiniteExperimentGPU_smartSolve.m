%% Set initial info and function
addpath('Data')
addpath('Functions')
initSeed = 4;
batchSize = 40;
% Time and area
L = 20*pi; XInt = [-L,L];
T = 10^(-6); TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
% Test with small variables to see if it works:
% N = 2^4; % Time
% M = 2^4; % Space
% refM = 2^10; % Space
N = 2^20; % Time
M = 2^14; % Space
refM = 2^19; % Space
NVec = [N, 2*N, 4*N];
sigmaVec = [3, 3.9, 4, 4.1];
per = true;
% simpsonAppr determines H1 norm approximation method
simpsonAppr = false;

u0Fun = @(x) 10*exp(-10*x.^2);

% Calculate initial H1 value and 
dx = (XInt(2)-XInt(1))/M;
x = XInt(1) + dx*(0:M-1);
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2-1, 0, -M/2+1:-1];
initH1 = sqrt(H1norm(fft(u0Fun(x)),dx,k,per,simpsonAppr,2*L));

%% Define the alternative criteria for blowup - Boolean output
blowUpCrit = cell(0,0);
% Toggle which criteria which you deem most important to go by. Those not
% deemed important are not used for simulation abortion
importantCrit = [false, false, true, true, true, true];

% Ratio comparisons
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.2*H1Old;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.5*H1Old;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.7*H1Old;
% Derivative comparisons
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^2;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^4;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^6;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^8;
% Hard limit comparisons
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 25*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^2*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 5*10^2*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^3*initH1;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^4*initH1;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^5*initH1;

numBlowUpCrit = length(blowUpCrit);
%% Define the criteria for refining the process
% Lets refine each time H1-norm is 10 times bigger.
% Don't refine more than three times.
% Only refine process 1
refCrit = @(H1New,lastRefH1,numRef,processNo)  (H1New > 10*lastRefH1) & (numRef < 5) & (processNo == 1);

%% Initialize storage
numSigma = length(sigmaVec);
numN = length(NVec);
% Two different refinement levels / methods
% OBS! If this changes, the code within the loop may need to be adapted
numProc = 2;
blowupTimes = zeros(batchSize,numBlowUpCrit,numProc,numN,numSigma);

placeholderValue = 1;
if T >= placeholderValue
    error('Placeholder value used when blowup has not occurred, it needs to be larger than T.')
end

%% Perform calculations
parfor m = 1:batchSize
    rng(m,'twister')
    % Load external variables in order to enable parfor
    blowUpCrit;
    importantCrit;
    refCrit;
    sigmaVec;
    % Check if this sample has been run to completion. Don't run in that
    % case
    completeFileName = ['Data\blowupTime' num2str(m) '.mat'];
    if ~isfile(completeFileName)
        % Check if backup has been saved for current sample, if so: load it
        backUp1FileName = ['Data\blowupBackupPrim' num2str(m) '.mat'];
        % Backup 2 only present in case of crash during saving.
        % MANUAL HANDLING IN THAT CASE
        backUp2FileName = ['Data\blowupBackupSec' num2str(m) '.mat'];
        
        backUpPause = 1;
        
        % If the backup is there, load it and run where it is zero
        if isfile(backUp1FileName)
            temp = load(backUp1FileName);
            internalBlowupTimes = temp.variable;
        else
            internalBlowupTimes = zeros(numBlowUpCrit,numProc,numN,numSigma);
        end
        
        maxN = max(NVec);
        dW = randn(2,maxN)*sqrt(T/maxN/2);

        % Vary N and sigma
        for n = 1:numN
            for j = 1:numSigma
                % Check if the current spec has been run (e.g. when backup)
                % Only run if no data present (presence of zero elements is
                % not enough due to importantCrit)
                if ~any(any(internalBlowupTimes(:,:,n,j)~=0))
                    currN = NVec(n);
                    sigma = sigmaVec(j);
                    % If we are not using the finest time discretization, calculate the
                    % coarser Brownian motion increments
                    if currN < maxN
                        scalingFactor = maxN/currN;
                        coarseDW = zeros(2,currN);
                        currIndex = 0;
                        for i = 1:currN
                            indexList = (currIndex+1):(currIndex+scalingFactor/2);
                            coarseDW(1,i) = sum(sum(dW(:,indexList)));
                            indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
                            coarseDW(2,i) = sum(sum(dW(:,indexList)));
                            currIndex = currIndex + scalingFactor;
                        end
                    else
                        coarseDW = dW;
                    end

                    % Initialize schemes, time, and spatial information
                    modelInfos = cell(2,1);
                    modelInfos{1} = initModelInfo(currN,TInt,M,XInt,sigma,per);
                    modelInfos{2} = initModelInfo(currN,TInt,refM,XInt,sigma,per);

                    % Load variables into GPU memory
                    currU = cell(numProc,1);
                    % Initial value
                    for proc = 1:numProc
%                         currU{proc} = fft(u0Fun(modelInfos{proc}.x));
                        currU{proc} = gpuArray(fft(u0Fun(modelInfos{proc}.x)));
                    end
                    % Norm storage
                    currH1Val = gpuArray(zeros(numProc,1));
                    for proc = 1:numProc
                        currH1Val(proc) = sqrt(H1norm(currU{proc},modelInfos{proc}.dx,modelInfos{proc}.k,per,simpsonAppr,2*L));
%                         currH1Val(proc) = sqrt(gather(H1norm(currU{proc},modelInfos{proc}.dx,modelInfos{proc}.k,per,simpsonAppr,2*L)));
                    end
                    oldH1Val = currH1Val;

                    % Not yet refined
                    numRef = zeros(numProc,1);
                    lastRefVal = currH1Val;

                    % Two criteria per process
                    exploded = false(numBlowUpCrit,numProc);

                    i = 1;
                    t = 0;
                    while any(any(~exploded(importantCrit,:))) && i <= currN
                        t = t + modelInfos{proc}.h;
                        currDW = coarseDW(:,i);
                        % Loop over each process
                        for proc = 1:numProc
                            if(any(~exploded(importantCrit,proc)))
                                % Progress
                                currU{proc} = modelInfos{proc}.schemes.fun{7}(currU{proc},currDW);
                                % Calculate norm
                                oldH1Val(proc) = currH1Val(proc);
                                currH1Val(proc) = sqrt(H1norm(currU{proc},modelInfos{proc}.dx,modelInfos{proc}.k,per,simpsonAppr,2*L));
%                                 currH1Val(proc) = sqrt(gather(H1norm(currU{proc},modelInfos{proc}.dx,modelInfos{proc}.k,per,simpsonAppr,2*L)));
                                % Check blowup criteria
                                for BUP = 1:numBlowUpCrit
                                    exploded(BUP,proc) = blowUpCrit{BUP}(oldH1Val(proc),currH1Val(proc),modelInfos{proc}.h);
                                    % Record time if criteria fulfilled
                                    if exploded(BUP,proc) && (internalBlowupTimes(BUP,proc,n,j) == 0)
                                        internalBlowupTimes(BUP,proc,n,j) = t;
                                    end
                                end
                                % Refinement if necessary
                                if refCrit(currH1Val(proc),lastRefVal(proc),numRef(proc),proc)
                                    numRef(proc) = numRef(proc) + 1;
                                    tempCurrU = ifft(currU{proc});
                                    currU{proc} = fft(refineU(tempCurrU,per,'nFme'));
                                    modelInfos{proc} = initModelInfo(currN,TInt,length(currU{proc}),XInt,sigma,per);
%                                     modelInfos{proc} = initModelInfo(currN,TInt,gather(length(currU{proc})),XInt,sigma,per);
                                end
                            end
                        end
                        fprintf(['\n-------'...
                            '\n Sample: ', num2str(m), ...
                            '\n N: ', num2str(currN), ...
                            ' ( ' num2str(n) ' of ' num2str(numN) ')', ...
                            '\n Sigma: ', num2str(sigma), ...
                            ' ( ' num2str(j) ' of ' num2str(numSigma) ')', ...
                            '.\n Current H1 value: ', num2str(currH1Val'), ...
                            '.\n Percentage until max time: ', num2str(i/currN), ...
                            '.\n Current blown up, proc 1: ', num2str(exploded(:,1)'), ...
                            '.\n Current blown up, proc 2: ', num2str(exploded(:,2)')]);
                        fprintf('.\n-------\n');
                        i = i+1;
                    end
                    % Insert the placeholder value for the non-exploded
                    % criteria
                    temp = internalBlowupTimes(:,:,n,j);
                    temp(temp == 0) = placeholderValue;
                    internalBlowupTimes(:,:,n,j) = temp;
                    
                    % Backup the results (as well as the backup itself)
                    if isfile(backUp1FileName)
                        temp = load(backUp1FileName);
                        parforSave(backUp2FileName,temp.variable);
                        pause(backUpPause)
                    end
                    parforSave(backUp1FileName,gather(internalBlowupTimes));
                    pause(backUpPause)
                    % Remove second backup, now that primary backup worked
                    if isfile(backUp2FileName)
                        delete(backUp2FileName)
                    end
                    pause(backUpPause)
                end
            end
        end
        % Save complete sample. Pause
        blowupTimes(m,:,:,:,:) = gather(internalBlowupTimes);
        parforSave(completeFileName,gather(internalBlowupTimes))
        pause(backUpPause)
        % Remove backups
        if isfile(backUp1FileName)
            delete(backUp1FileName)
        end
        if isfile(backUp2FileName)
            delete(backUp2FileName)
        end
    end
end