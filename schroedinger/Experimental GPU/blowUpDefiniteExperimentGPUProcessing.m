%% Set initial info and function
addpath('Data')
addpath('Functions')
initSeed = 4;
batchSize = 40;
% Time and area
L = 20*pi; XInt = [-L,L];
T = 10^(-6); TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
% Test with small variables to see if it works:
% N = 2^4; % Time
% M = 2^4; % Space
% refM = 2^10; % Space
N = 2^20; % Time
M = 2^14; % Space
refM = 2^19; % Space
NVec = [N, 2*N, 4*N];
sigmaVec = [3, 3.9, 4, 4.1];
per = true;
% simpsonAppr determines H1 norm approximation method
simpsonAppr = false;

u0Fun = @(x) 10*exp(-10*x.^2);

% Calculate initial H1 value and 
dx = (XInt(2)-XInt(1))/M;
x = XInt(1) + dx*(0:M-1);
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2-1, 0, -M/2+1:-1];
initH1 = sqrt(H1norm(fft(u0Fun(x)),dx,k,per,simpsonAppr,2*L));

%% Define the alternative criteria for blowup - Boolean output
blowUpCrit = cell(0,0);
% Toggle which criteria which you deem most important to go by. Those not
% deemed important are not used for simulation abortion
importantCrit = [false, false, true, true, true, true];

% Ratio comparisons
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.2*H1Old;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.5*H1Old;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 1.7*H1Old;
% Derivative comparisons
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^2;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^4;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^6;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) (H1New - H1Old)/dt > 10^8;
% Hard limit comparisons
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 25*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^2*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 5*10^2*initH1;
blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^3*initH1;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^4*initH1;
% blowUpCrit{end+1,1} = @(H1Old,H1New,dt) H1New > 10^5*initH1;

numBlowUpCrit = length(blowUpCrit);
%% Define the criteria for refining the process
% Lets refine each time H1-norm is 10 times bigger.
% Don't refine more than three times.
% Only refine process 1
refCrit = @(H1New,lastRefH1,numRef,processNo)  (H1New > 10*lastRefH1) & (numRef < 4) & (processNo == 1);

%% Load the samples present
numSigma = length(sigmaVec);
numN = length(NVec);
numProc = 2;
completeFileNameFun = @(m) ['Data\blowupTime' num2str(m) '.mat'];
% completeFileNameFun = @(m) ['blowupTime' num2str(m) '.mat'];

% Check how many samples there are present
numSamples = 0;
for m = 1:batchSize
    completeFileName = completeFileNameFun(m);
    if isfile(completeFileName)
        numSamples = numSamples + 1;
    end
end
% Initialize storage
blowupTimes = zeros(numSamples,numBlowUpCrit,numProc,numN,numSigma);
% Fill storage
currSample = 1;
for m = 1:batchSize
    completeFileName = completeFileNameFun(m);
    if isfile(completeFileName)
        load(completeFileName)
        blowupTimes(currSample,:,:,:,:) = variable;
        currSample = currSample + 1;
    end
end
%% Plot both processes
procNo = 1;
coarseProc = permute(squeeze(blowupTimes(:,:,procNo,:,:)),[1 3 4 2]);
procNo = 2;
fineProc = permute(squeeze(blowupTimes(:,:,procNo,:,:)),[1 3 4 2]);

numBins = 20;
yaxis = linspace(0,T,numBins);

rightTitles = cell(numSigma,1);
for i = 1:numSigma
    rightTitles{i} = ['$\sigma = ' num2str(sigmaVec(i)) '$'];
end

botTitles = cell(numBlowUpCrit,1);
for i = 1:numBlowUpCrit
    botTitles{i} = ['\tau_' num2str(i)];
end

topTitles = cell(numN,1);
for i = 1:numN
    topTitles{i} = ['$N = 2^{' num2str(log2(NVec(i))) '}$'];
end

vertHistPlotMult(coarseProc,botTitles,yaxis,topTitles,rightTitles)
pause(1)
printToPDF(gcf,'blowupCoarseAndRef')
vertHistPlotMult(fineProc,botTitles,yaxis,topTitles,rightTitles)
pause(1)
printToPDF(gcf,'blowupFine')

%% Check how many blowup-criteria which are not met
% Current configuration has the placeholder value 1
placeholderValue = 1;

procNo = 1;
numNotBlowup1 = squeeze(sum(blowupTimes(:,:,procNo,:,:)==placeholderValue))
procNo = 2;
numNotBlowup2 = squeeze(sum(blowupTimes(:,:,procNo,:,:)==placeholderValue))

%% Check time ratio between finest and coarsest time on fine proc.
test = fineProc(:,1,2:4,:) ./ fineProc(:,3,2:4,:);
% Ignore the values with a ratio higher than 100. These only occurred with 
% the ratio criteria
histCeil = max(test(test(:) < 100));
histFloor = min(test(test(:) > 0.01));
numBins = 20;
yaxis = linspace(histFloor,histCeil,numBins);

yTicks = [histFloor 1 histCeil];
yLabels = {num2str(round(histFloor,2)) num2str(1) num2str(round(histCeil,2))};

vertHistPlotMult(test,botTitles,yaxis,{'Ratio:$\tau_i(u_{coarse}) / \tau_i(u_{fine})$'},rightTitles(2:4),yTicks,yLabels)
pause(1)
printToPDF(gcf,'BlowupRatio')
%% Check time difference between finest and coarsest time on fine proc.
test = (fineProc(:,1,2:4,:) - fineProc(:,3,2:4,:))/T*100;
% Ignore the values with a difference higher than 10^6. Those are false
% differences introduced by the placeholder values
histCeil = max(test(test(:) < 10^6));
histFloor = min(test(test(:) > -10^6));
numBins = 20;
yaxis = linspace(histFloor,histCeil,numBins);

% yTicks = [histFloor (histFloor+histCeil)/2 histCeil];
yTicks = [histFloor 0 histCeil];

numTicks = length(yTicks);
yLabels = cell(numTicks,1);
for i = 1:numTicks
%     labStr = '10^{';
%     if yTicks(i) < 0
%         labStr = ['-' labStr  num2str(round(log10(abs(yTicks(i))),2))];
%     else
%         labStr = [labStr num2str(round(log10(yTicks(i)),2))];
%     end
%     labStr = [labStr '}'];
%     yLabels{i} = labStr;
    yLabels{i} = num2str(round(yTicks(i),4));
end

vertHistPlotMult(test,botTitles,yaxis,{'Difference:$(\tau_i(u_{coarse}) - \tau_i(u_{fine}))/T*100$'},rightTitles(2:4),yTicks,yLabels)
pause(1)
printToPDF(gcf,'BlowupDiff')
%% Sign test
test = squeeze(fineProc(:,1,2:4,:) - fineProc(:,3,2:4,:));
signTestRes = zeros(numSigma-1,numBlowUpCrit);
for s = 1:numSigma-1
    for t = 1:numBlowUpCrit
        signTestRes(s,t) = signtest(test(:,s,t));
    end
end

signTestResNeg = zeros(numSigma-1,numBlowUpCrit);
for s = 1:numSigma-1
    for t = 1:numBlowUpCrit
        signTestResNeg(s,t) = signtest(test(:,s,t),0,'Tail','left');
    end
end

signTestResPos = zeros(numSigma-1,numBlowUpCrit);
for s = 1:numSigma-1
    for t = 1:numBlowUpCrit
        signTestResPos(s,t) = signtest(test(:,s,t),0,'Tail','right');
    end
end

% We get that
% signTestRes =
% 
%     0.0064    0.0007    1.0000    0.2682    0.4296    0.8746
%     0.0064    1.0000    1.0000    1.0000    0.0385    0.0022
%     0.0000    0.0064    0.2682    0.2682    0.4296    0.4296
%
% signTestResNeg =
% 
%     0.0032    0.0003    0.5627    0.1341    0.8659    0.4373
%     0.0032    0.5627    0.5627    0.5627    0.9917    0.9997
%     0.0000    0.0032    0.1341    0.1341    0.8659    0.2148
%
% signTestResPos =
% 
%     0.9989    0.9999    0.5627    0.9231    0.2148    0.6821
%     0.9989    0.5627    0.5627    0.5627    0.0192    0.0011
%     1.0000    0.9989    0.9231    0.9231    0.2148    0.8659

% With these 40 samples, we cannot conclude that the median is smaller than
% 0, i.e. that the finer process explode later than the coarse process,
% with a reasonable significance level. However, with a 5% significance
% level, we can reject the null hypothesis that the finer explode before
% the coarse process, for sigma=4 and the two last criteria (which are
% unreliable due to their size, as seen from the plots)













% %% Retrieve the Brownian motions, in order to identify which samples do blow up
% % Simulate all Brownian motions
% dW = zeros(batchSize,2,max(NVec));
% parfor m = 1:batchSize
%         rng(m,'twister')
%         maxN = max(NVec);
%         dW(m,:,:) = randn(2,maxN)*sqrt(T/maxN/2);
% end
% 
% % Remove the non-relevant dW's
% dWDone = zeros(numSamples,2,max(NVec));
% currIndex = 1;
% for m = 1:batchSize
%     completeFileName = completeFileNameFun(m);
%     if isfile(completeFileName)
%         dWDone(currIndex,:,:) = dW(m,:,:);
%         currIndex = currIndex + 1;
%     end
% end
% clear dW
% 
% %% Process and  the Brownian motions
% dWDone = cumsum(squeeze(sum(dWDone,2)),2);
% %% Plot blownup samples in red, otherwise in blue
% % Look at which samples blew up
% [row,col] = find(nonFiltered==0);
% nonBlownup = unique(row);
% blownUp = setdiff(1:numSamples,nonBlownup);
% figure
% hold on
% plot(dWDone(blownUp,:)','r')
% plot(dWDone(nonBlownup,:)','b')
% plot(linspace(0,T,max(NVec)),'k')
% hold off