% This script tests for the critical exponent. Can be modified easily
% Aiming for h = 0.1 dx^2 for at least one setting
% Schrödinger equation uses dx = 10^-4, h = 0.1 dx^2 (Debussche)
% addpath(genpath('../'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%Time and spatial info%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define time and spatial info
L = 20*pi;
TVec = 10^-2;
NVec = 2.^(6:7); % Time % For quick testing of new code
% NVec = 2.^[16:17]; % Time
MVec = 2.^(8:9); % Space % For quick testing of new code
% MVec = 2.^[16:17]; % Space

sigmaVec = [1,2,3,4];
numSigma = length(sigmaVec);

% Set how many time points are tracked at most
maxNumStore = 2^17;

% Set when we abort the calculations
H1Limit = 500;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%Sample definition%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define how many samples we want
desiredBatchSize = 4;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%Initial values%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
u0Fun = @(x) 10*exp(-10*x.^2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%Simulations%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vary sigma
% Vary noise (true false)
% Vary interval length (normal and double)
[temp1,temp2,temp3] = ndgrid(1:numSigma,[1,0],[1,0]);
settings = [temp1(:),temp2(:),temp3(:)];
numSettings = length(settings);

for setting = 1:numSettings
    % Get sigma
    sigmaIndex = settings(setting,1);
    sigma = sigmaVec(sigmaIndex);
    % Get deterministic or not
    stoch = settings(setting,2);
    if stoch == 1
        detOrStoch = 'Stoch';
        stoch = true;
        batchSize = desiredBatchSize;
    else
        detOrStoch = 'Det';
        stoch = false;
        batchSize = 1;
    end
    % Get interval width
    normalWidth = settings(setting,3);
    if normalWidth == 1
        widthText = '';
        normalWidth = true;
        intL = L;
        intMVec = MVec;
    else
        widthText = '2L';
        normalWidth = false;
        intL = 2*L;
        intMVec = 2*MVec;
    end
    fileName = ['Schr' detOrStoch 'TEST' widthText 'Sigma' num2str(sigmaIndex)];
    
    BUCalc(batchSize,L,u0Fun,NVec,MVec,TVec,fileName,stoch,1,sigma,maxNumStore,H1Limit,2^3)
end