% This script is written in order to send experiment results of smaller
% sizes, in the order that they get done.

batchSize = 200;

fileSent = false(batchSize,1);
% If we find the file, and we find that we haven't sent it, send the file.
% Check every 20 min (1200 sec).
pauseLen = 1200;
while true
    for m = 1:batchSize
        fileName = ['Data\blowupTime' num2str(m) '.mat'];
        if isfile(fileName) && ~fileSent(m)
            subjectLine = ['See attached sample nr ' num2str(m)];
            messageBody = 'Hopefully this will be some useful data.';
            matlabmail('andremonall@gmail.com', messageBody, subjectLine, fileName);
            fileSent(m) = true;
        end
    end
    disp(['Checked. Checking again in ' num2str(pauseLen/60) ' minutes'])
    pause(pauseLen)
end