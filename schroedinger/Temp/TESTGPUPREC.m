%% Set initial info and function
clear all
close all 
addpath('Data')
addpath('Functions')
% Time and area
L = 20*pi; XInt = [-L,L];
T = 10^(-6);
TInt = [0,T];
% T = 1; TInt = [0,T];

% Numerical precision, number of points
% Test with small variables to see if it works:
% N = 2^4; % Time
% M = 2^4; % Space
% refM = 2^10; % Space
N = 2^20; % Time
M = 2^14; % Space
refM = 2^19; % Space
NVec = [N, 2*N, 4*N];
maxN = max(NVec)
sigma = 3;
per = true;
% simpsonAppr determines H1 norm approximation method
simpsonAppr = false;

u0Fun = @(x) 10*exp(-10*x.^2);

% Calculate initial H1 value and 
dx = (XInt(2)-XInt(1))/M;
x = XInt(1) + dx*(0:M-1);
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2-1, 0, -M/2+1:-1];
initH1 = sqrt(H1norm(fft(u0Fun(x)),dx,k,per,simpsonAppr,2*L));

model = initModelInfo(maxN,TInt,M,XInt,sigma,per);
clc
fprintf(['Endtime: ' num2str(T) '\n',...
    'maxN: ' num2str(maxN) ' = 2^' num2str(log2(maxN)) '\n',...
    'Stepsize: ' num2str(model.h) '\n'])


% DLap=(2*pi/(XInt(2)-XInt(1)))^2*(-1i).*([0:M/2 -M/2+1:-1].^2);

%% Perform calculations
for m = 1:1
    rng(m,'twister')
    maxN = max(NVec);
%     dW = randn(2,maxN)*sqrt(T/maxN/2);

    currU = gpuArray(fft(u0Fun(model.x)));
    for i = 1:maxN
        dW = randn(2,1)*sqrt(T/maxN/2);
        currU = model.schemes.fun{7}(currU,dW);
        
%         tempo4=exp(DLap*sum(dW)).*currU;tempo5=ifft(tempo4);
%         currU=fft(exp(1i*model.h*real(tempo5.*conj(tempo5)).^sigma).*tempo5);
        
        currH1Val = gather(sqrt(H1norm(currU,model.dx,model.k,per,simpsonAppr,2*L)));
        fprintf(['\n-------'...
            '\n Sample: ', num2str(m), ...
            '\n Time: ', num2str(i*model.h), ...
            '.\n Current H1 value: ', num2str(currH1Val')]);
        fprintf('.\n-------\n');
        if currH1Val > 20.88
           break
        end
    end
end