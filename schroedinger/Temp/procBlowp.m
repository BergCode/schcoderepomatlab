for m = 1:40
    m
    completeFileName = ['Data\blowupTime' num2str(m) '.mat'];
    if isfile(completeFileName)
        load(completeFileName);
        if any(variable(:)==0)
            variable(variable==0) = 1;
            parforSave(completeFileName,variable)
        end
    end
end