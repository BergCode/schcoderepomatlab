%% seems ok
%% conjecture Debussche
%% plot of H1 norm of the numerical solution
%% for a fixed spatial discretisation steps (Pseudospectral)

%% Pseudospectral in space, 
%% Lie-Trotter splitting method in time 
%% for nonlinear random Schrödinger equation 
%% iu_t+sigmaNoise*u_xx\circ W+alphaNL*|u|^(2sigmaNL)u=0   on    (0,2pi)

clear all
close all

% initial position, this is normalised below
%u_0=inline('5*exp(-10.*(x-pi).^2)','x');
u_0=inline('10.0*exp(-1.*(10*x.^2))','x');

sigmaNoise=1.;alphaNL=1.;
sigmaNL=3.9; 

%x_0=0*pi;x_end=2*pi;
x_0=-20*pi;x_end=20*pi;
t_0=0;t_end=0.0000001; 

dt=10^(-14)
Nx=2^20; % Fourier modes 2^24 seems to be the max 
x_ex=(x_0:(x_end-x_0)/Nx:x_end-(x_end-x_0)/Nx)';

%      figure(1)
%      plot(x_ex,abs(u_0(x_ex)).^2);
%      title(strcat('t=',num2str(0*dt)));
%      axis([x_0 x_end 0 1]);
%      drawnow;
%      %pause;

Nt=floor((t_end-t_0)/dt);
h=(x_end-x_0)/Nx;
dt/h^2

%% for fft matlab, cf book by Lord

tstop=Nt*dt;

MS=3; % number of samples

% H1 norm
NormeH1=zeros(MS,Nt+1);

parfor mm = 1:MS
    % Load to internal memory
    u_0;
    
    % Load into GPU
%     DLap = gpuArray((2*pi/(x_end-x_0))^2*(-1i).*([0:Nx/2 -Nx/2+1:-1].^2)'); % size Nx with the 1i / if [a,b] one has (2pi/(b-a))^2[0:Nx/2 -Nx/2+1:-1].^2)
    DLap = (2*pi/(x_end-x_0))^2*(-1i).*([0:Nx/2 -Nx/2+1:-1].^2)'; % size Nx with the 1i / if [a,b] one has (2pi/(b-a))^2[0:Nx/2 -Nx/2+1:-1].^2)
    VH1 = 1+abs(DLap); % derivative
%     internalH1 = gpuArray(zeros(1,Nt+1));
    internalH1 = zeros(1,Nt+1);
    
    % No need to initialize if filling memory immediately
    % uSEX_num=zeros(Nx,1);
%     uSEX_num = gpuArray(fft(u_0(x_ex))); 
    uSEX_num = fft(u_0(x_ex)); 
    internalH1(1) = sqrt(sum(VH1.*(abs(uSEX_num/Nx).^2)));
%     NormeH1(mm,1)=sqrt(sum(VH1.*(abs(uSEX_num/Nx).^2)));

    % Set random state
%     ste=mm; % rdm state
%     randn('state',ste)
    rng(mm,'twister')

    % This takes way too long. Since you're not using the finer dW, I
    % replace it with equivalent. Length of time is due to the memory
    % accessing.
%     dW = gpuArray(sigmaNoise*sqrt(dt)*randn(1,Nt));
    dW = sigmaNoise*sqrt(dt)*randn(1,Nt);
%     dWraff = gpuArray(sigmaNoise*sqrt(dt/2)*randn(1,2*Nt));
%     dW = gpuArray(zeros(1,Nt));
%     for kk = 1:Nt
%         dW(kk) = dWraff(2*kk-1)+dWraff(2*kk);
%     end

    for t = 1:Nt
        %% noise
        Winc = sum(dW(1*(t-1)+1:1*t));
%         Winc1 = sum(dWraff((t-1)*2*1+1:(t-1)*2*1+1));
%         Winc2 = sum(dWraff((t-1)*2*1+1+1:(t-1)*2*1+2*1));

        %% Lie-Trotter
        tempo4 = exp(DLap*Winc).*uSEX_num;tempo5=ifft(tempo4);
        uSEX_num = fft(exp(alphaNL*1i*dt*real(tempo5.*conj(tempo5)).^sigmaNL).*tempo5);

%         NormeH1(mm,t+1)=sqrt(sum(VH1.*(abs(uSEX_num/Nx).^2)));
        internalH1(t+1) = sqrt(sum(VH1.*(abs(uSEX_num/Nx).^2)));        
        t
%      if mod(t,200)==0
%      figure(1)
%      plot(x_ex,abs(ifft(uSEX_num)).^2);
%      title(strcat('t=',num2str(t*dt),' H_1=',num2str(NormeH1(mm,t+1))));
%      axis([x_0 x_end 0 1]);
%      drawnow;
%      end;
     
     % stop if normH1 too big
 %    if NormeH1(mm,t+1) >= 100*NormeH1(mm,1)
 %      disp('ciao');
%       tstop=t*dt
%       break
%     end 
    end
    % Export the numbers outside the parfor, gather due to GPU
    NormeH1(mm,:) = gather(internalH1);
    mm
end

save('u10h1DBLT39dt.mat')

set(0,'DefaultTextFontSize',12)
set(0,'DefaultAxesFontSize',12)
set(0,'DefaultLineLineWidth',2)
set(0,'DefaultLineMarkerSize',10)

TT=(0:dt:tstop); 
%ind=1:5:Nt+1;
%stop
figure(2);
for mm=1:MS
    plot(TT,NormeH1(mm,:))
    hold on;
    xlabel('Time','FontSize',14);
    ylabel('$H^1$-norm','Rotation',0,'HorizontalAlignment','right','Interpreter','latex','FontSize',14);
end
print -depsc2 -r0 u10h1DBLTsigma39dt.eps
